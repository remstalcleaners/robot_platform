use std::thread;
use std::time::Duration;
use crate::imu::{calibrate, IMU, test};
use crate::super_sonic::SuperSonic;

pub mod light_distance;
pub mod super_sonic;
pub mod imu;

pub const KILL: i32 = 42;

pub fn libmain() {
    println!("Start MPU Sensor...");
    calibrate();
    /*
    let mut imu = IMU::new();
    for i in 0..10 {
        let angle = imu.read().unwrap();
        println!("{:?}", angle);
        thread::sleep(Duration::from_secs(1));
    }
     */
    //test();
}




//##################################################################################################
//##################################################################################################
//##################################################################################################
/*
#[cfg(test)]
mod tests {
    use super::*;
    //Test the ultrasound sensors on the test hardware. Must be read by a human, because no value is best.
    const L: usize = 10000;
    #[test]
    fn test_ultrasound() {
        println!("Start Ultrasound Sensors...");
        let mut sensor = SuperSonic::new(vec![15, 23, 25, 7],vec![14, 18, 24, 8],vec!["Front Left".to_string(),"Backside".to_string(),"Front Right".to_string(),"Front Middle".to_string()]);
        let mut index: usize = 0;
        let mut data_0: [f32; L] = [0.0;L];
        let mut data_1: [f32; L] = [0.0;L];
        let mut data_2: [f32; L] = [0.0;L];
        let mut data_3: [f32; L] = [0.0;L];
        loop {
            let res = sensor.read();
            data_0[index] = res[0];
            data_1[index] = res[1];
            data_2[index] = res[2];
            data_3[index] = res[3];
            index += 1;
            if index >= L {
                let x_0 = statistic(&data_0);
                let x_1 = statistic(&data_1);
                let x_2 = statistic(&data_2);
                let x_3 = statistic(&data_3);
                print!("\r{:05} {:05} {:05}", x_0.0, x_0.1, x_0.2);
                print!("; {:05} {:05} {:05}", x_1.0, x_1.1, x_1.2);
                print!("; {:05} {:05} {:05}", x_2.0, x_2.1, x_2.2);
                print!("; {:05} {:05} {:05}", x_3.0, x_3.1, x_3.2);
                index = 0;
            }
        }
    }
    fn statistic(numbers: &[f32; L]) -> (i32, i32, i32) {
        let mut avg = 0.0;
        for x in numbers {
            avg += x;
        }
        let avg = avg/(L as f32);
        let mut sig = 0.0;
        for x in numbers {
            sig += (avg - x)*(avg - x);
        }
        let sig = (sig/(L as f32)).sqrt();
        ((avg/100.0-sig/200.0) as i32, (avg/100.0) as i32, (avg/100.0+sig/200.0) as i32)
    }
}

 */