use rppal::i2c::I2c;
use vl53l0x::VL53L0x;
use rppal::gpio::{Gpio, OutputPin, InputPin, Level};
use rppal::gpio::Trigger;
use std::time::{Duration, Instant};
use std::thread;
use std::sync::{Arc, Mutex, mpsc};
use rppal::gpio;
use std::thread::JoinHandle;
use std::sync::mpsc::{TryRecvError, Sender, RecvTimeoutError};
use rppal::gpio::Mode::Output;
use crate::KILL;

/*
Tested! Tested again for the async version at 15.09.2022.
Fully tested on prototype hardware:
    let mut sensor = SuperSonic::new(vec![15, 23, 25, 7],vec![14, 18, 24, 8],vec!["Front Left".to_string(),"Backside".to_string(),"Front Right".to_string(),"Front Middle".to_string()]);
    Are the correct pins. All 4 ultrasonic sensors were able to measure the distance!
*/

pub struct SuperSonic {
    names: Vec<String>,
    handler: JoinHandle<()>,
    terminator: Sender<i32>,
    data: Arc<Mutex<Vec<f32>>>,
}

impl SuperSonic {
    pub fn new(trigger_pins: Vec<u8>, echo_pins: Vec<u8>, names: Vec<String>) -> Self {
        assert_eq!(trigger_pins.len(),echo_pins.len(), "Super sonic needs same number of pins for trigger and echo.");
        assert_eq!(trigger_pins.len(),names.len(), "Length of names must be same as trigger and echo.");
        let (handler, terminator, data) = run_async_asyncly(trigger_pins, echo_pins);
        Self{
            names,
            handler,
            terminator,
            data,
        }
    }
    pub fn read(&mut self) -> Vec<f32> {
        self.data.lock().unwrap().clone()
    }
    pub fn get_name(&mut self) -> Vec<String> {
        self.names.clone()
    }
}
/*
fn run_async(trigger_pins: Vec<u8>, echo_pins: Vec<u8>) -> (JoinHandle<()>, Sender<i32>, Arc<Mutex<Vec<f32>>>){
    println!("Init ultrasonic sensors ...");
    assert_eq!(trigger_pins.len(),echo_pins.len(),"Super sonic needs same number of pins for trigger and echo.");
    let mut data = Arc::new(Mutex::new(vec![0.0;trigger_pins.len()]));
    let data_for_thread = data.clone();
    let (tx, rx) = mpsc::channel();
    let handle = thread::spawn(move ||
        {
            let mut buffer = vec![0.0; trigger_pins.len()];
            let mut trigger_gpio = Vec::<OutputPin>::with_capacity(trigger_pins.len());
            let mut echo_gpio = Vec::<InputPin>::with_capacity(trigger_pins.len());
            for tpin in trigger_pins {
                trigger_gpio.push(Gpio::new().expect("Drigger Err").get(tpin).expect("XXX 2 Err").into_output());
            }
            for epin in echo_pins {
                echo_gpio.push(Gpio::new().expect("Echo Err").get(epin).expect("Echo 2 Err").into_input());
            }
            println!("Start Ultrasonic sensor loop ... ");
            const SOUND_SPEED_HALF: f32 = 0.0343/2.0;//cm per micro second divided by half
            loop {
                match rx.try_recv() {
                    Ok(x) => {
                        if x == KILL {
                            break;/* If KILL signal is send, end this loop */
                        }
                    }
                    Err(TryRecvError::Disconnected) => {break;}
                    Err(TryRecvError::Empty) => {}
                };
                //Loop though all ultrasonic sensors, only one sensor at a time
                for i in 0..echo_gpio.len() {
                    echo_gpio[i].set_interrupt(Trigger::RisingEdge).unwrap();
                    trigger_gpio[i].set_high();
                    thread::sleep(Duration::from_nanos(10));
                    trigger_gpio[i].set_low();
                    let start_sig = echo_gpio[i].poll_interrupt(true, Some(Duration::from_millis(30))).unwrap();
                    let start = Instant::now();
                    echo_gpio[i].set_interrupt(Trigger::FallingEdge).unwrap();
                    let end_sig = echo_gpio[i].poll_interrupt(true, Some(Duration::from_millis(30))).unwrap();
                    let duration = start.elapsed().subsec_micros() as f32;
                    let distance = duration * SOUND_SPEED_HALF; //distance in cm
                    buffer[i] = distance;
                }
                let mut data_locked = data_for_thread.lock().unwrap();
                for i in 0..echo_gpio.len(){
                    data_locked[i] = buffer[i];
                }
            }
        }
    );
    println!("Thread should run now ...");
    (handle, tx, data)
}
 */
impl Drop for SuperSonic {
    fn drop(&mut self) {
        //Try to kill the thread with the KILL signal.
        //If Result Error, ignore because then I don't know, probably the thread is death already.
        match self.terminator.send(KILL){
            Ok(_) => {}
            Err(e) => {
                println!("Drop Error: {}" ,e);
            }
        }
    }
}




fn run_async_asyncly(trigger_pins: Vec<u8>, echo_pins: Vec<u8>) -> (JoinHandle<()>, Sender<i32>, Arc<Mutex<Vec<f32>>>){
    println!("Init ultrasonic sensors ...");
    assert_eq!(trigger_pins.len(),echo_pins.len(),"Super sonic needs same number of pins for trigger and echo.");
    let mut data = Arc::new(Mutex::new(vec![0.0;trigger_pins.len()]));
    let data_for_thread = data.clone();
    let (tx, rx) = mpsc::channel();
    let handle = thread::spawn(move ||
        {
            let mut buffer = vec![0.0; trigger_pins.len()];
            let mut trigger_gpio = Vec::<OutputPin>::with_capacity(trigger_pins.len());
            let mut echo_gpio = Vec::<InputPin>::with_capacity(trigger_pins.len());
            let trigger_duration = Duration::from_nanos(10);
            let echo_max_duration = Duration::from_millis(50);
            for tpin in trigger_pins {
                trigger_gpio.push(Gpio::new().expect("Trigger Err").get(tpin).expect("XXX 2 Err").into_output());
            }
            for epin in echo_pins {
                echo_gpio.push(Gpio::new().expect("Echo Err").get(epin).expect("Echo 2 Err").into_input());
            }
            let mut channels = Vec::new();
            for i in 0..echo_gpio.len() {
                channels.push(mpsc::channel());
            }
            for i in 0..echo_gpio.len() {
                let stx_local = channels[i].0.clone();
                echo_gpio[i].set_async_interrupt(Trigger::Both, move |l: Level| { stx_local.send((l,i)); });
            }
            println!("Start async Ultrasonic sensor loop ... ");
            const SOUND_SPEED_HALF: f32 = 0.0343/2.0;//cm per micro second divided by half
            'main: loop {
                match rx.try_recv() {
                    Ok(x) => {
                        if x == KILL {
                            break;/* If KILL signal is send, end this loop */
                        }
                    }
                    Err(TryRecvError::Disconnected) => {break;}
                    Err(TryRecvError::Empty) => {}
                };
                //Loop though all ultrasonic sensors, only one sensor at a time
                'sensor: for i in 0..echo_gpio.len() {
                    'receiver_flush: loop {
                        match channels[i].1.try_recv() {
                            Ok(_) => {}
                            Err(TryRecvError::Disconnected) => {break 'main;}
                            Err(TryRecvError::Empty) => {break 'receiver_flush;}
                        }
                    }
                    trigger_gpio[i].set_high();
                    thread::sleep(trigger_duration);
                    trigger_gpio[i].set_low();
                    'receiver: loop {
                        match channels[i].1.recv_timeout(echo_max_duration) {
                            Ok((l, index)) => {
                                match l {
                                    Level::High => {
                                        if index == i {
                                            break 'receiver;
                                        }
                                    }
                                    Level::Low => {}
                                }
                            }
                            Err(RecvTimeoutError::Disconnected) => {break 'main;}
                            Err(RecvTimeoutError::Timeout) => {continue 'sensor;}
                        };
                    }
                    let start = Instant::now();//Now that echo got high, let the clock start to see how long until we see a result.
                    'receiver2: loop {
                        match channels[i].1.recv_timeout(echo_max_duration) {
                            Ok((l, index)) => {
                                match l {
                                    Level::Low => {
                                        if index == i {
                                            break 'receiver2;
                                        }
                                    }
                                    Level::High => {}
                                }
                            }
                            Err(RecvTimeoutError::Disconnected) => {break 'main;}
                            Err(RecvTimeoutError::Timeout) => {continue 'sensor;}
                        };
                    }
                    let duration = start.elapsed().subsec_micros() as f32;
                    let distance = duration * SOUND_SPEED_HALF; //distance in cm
                    buffer[i] = distance;
                }
                let mut data_locked = data_for_thread.lock().unwrap();
                for i in 0..echo_gpio.len(){
                    data_locked[i] = buffer[i];
                }
            }
        }
    );
    println!("Thread should run now ...");
    (handle, tx, data)
}




