use std::fs::{File, OpenOptions};
use std::io::{Read, stdout};
use std::io::Write;
use std::{fs, thread};
use std::time::Duration;
use bno055::{BNO055OperationMode, Bno055, BNO055_CALIB_SIZE, BNO055Calibration};
use linux_embedded_hal::{Delay, I2cdev};
use bno055::mint::{EulerAngles, Quaternion};

pub struct IMU {
    imu: Bno055<I2cdev>,
}
impl IMU {
    /*
    pub fn calibrate() -> bool {
        let dev = I2cdev::new("/dev/i2c-1").expect("no dev...");
        let mut delay = Delay {};
        let mut imu = Bno055::new(dev).with_alternative_address();
        imu.init(&mut delay).expect("An error occurred while building the IMU");
        imu.set_mode(BNO055OperationMode::NDOF, &mut delay)
            .expect("An error occurred while setting the IMU mode");
        let mut status = imu.get_calibration_status().unwrap();
        println!("The IMU's calibration status is: {:?}", status);
        println!("- About to begin BNO055 IMU calibration...");
        // Wait for device to auto-calibrate.
        // Please perform steps necessary for auto-calibration to kick in.
        // Required steps are described in Datasheet section 3.11
        // Page 51, https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bno055-ds000.pdf (As of 2021-07-02)
        let mut index = 0;
        while !imu.is_fully_calibrated().unwrap() {
            status = imu.get_calibration_status().unwrap();
            std::thread::sleep(std::time::Duration::from_millis(1000));
            println!("Calibration status: {:?}", status);
            if index > 10 {break;}
            index += 1;
        }
        let calib = imu.calibration_profile(&mut delay).unwrap();
        let mut file = File::create("imu_calibration").expect("fail to create file");

        file.write_all(calib.as_bytes());
        let mut byte = Vec::new();
        file.read_exact(&mut byte);
        imu.set_calibration_profile(byte, &mut delay);

        println!("Calibration complete!");
        //let profile = imu.calibration_profile(&mut delay).unwrap();
        //imu.set_calibration_profile(profile,&mut delay).unwrap();

        true

        let bno055 = ...;
        // Read saved calibration profile from MCUs NVRAM
        let mut buf = [0u8; BNO055_CALIB_SIZE];
        mcu.nvram_read(BNO055_CALIB_ADDR, &mut buf, BNO055_CALIB_SIZE)?;

        // Apply calibration profile
        let calib = BNO055Calibration::from_buf(buf);
        bno055.set_calibration_profile(calib)?;
    }
     */
    pub fn new(fast: bool) -> Self {
        let dev = I2cdev::new("/dev/i2c-1").expect("no dev...");
        let mut delay = Delay {};
        let mut imu = Bno055::new(dev).with_alternative_address();
        imu.init(&mut delay).expect("An error occurred while building the IMU");
        imu.set_mode(BNO055OperationMode::NDOF, &mut delay)
            .expect("An error occurred while setting the IMU mode");
        /*
        let mut status = imu.get_calibration_status().unwrap();
        println!("The IMU's calibration status is: {:?}", status);
        println!("- About to begin BNO055 IMU calibration...");
        // Wait for device to auto-calibrate.
        // Please perform steps necessary for auto-calibration to kick in.
        // Required steps are described in Datasheet section 3.11
        // Page 51, https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bno055-ds000.pdf (As of 2021-07-02)
        let mut index = 0;
        while !imu.is_fully_calibrated().unwrap() {
            status = imu.get_calibration_status().unwrap();
            std::thread::sleep(std::time::Duration::from_millis(1000));
            println!("Calibration status: {:?}", status);
            if fast && index > 3 {break;}
            if index > 10 {break;}
            index += 1;
        }
        let calib = imu.calibration_profile(&mut delay).unwrap();
        */
        let calib = get_calibration();
        imu.set_calibration_profile(calib, &mut delay).unwrap();
        let mut status = imu.get_calibration_status().unwrap();
        println!("Calibration status: {:?}", status);
        println!("Calibration forced complete!");
        let mut imu = Self { imu: imu };
        imu.read_gyro();
        /*
        thread::sleep(Duration::from_secs(1));
        imu.read_gyro();
        thread::sleep(Duration::from_secs(1));
        imu.read_gyro();
        thread::sleep(Duration::from_secs(1));
        imu.read_acc();
        thread::sleep(Duration::from_secs(1));
        imu.read_acc();
        thread::sleep(Duration::from_secs(1));
        imu.read_acc();
        thread::sleep(Duration::from_secs(1));
        imu.read_gyro();
        thread::sleep(Duration::from_secs(1));
        imu.read_gyro();
        thread::sleep(Duration::from_secs(5));
         */
        println!("IMU Started ... !");
        imu
    }
    pub fn read_gyro(&mut self) -> Result<(f32,f32,f32),()> {
        let mut euler_angles: EulerAngles<f32, ()>; // = EulerAngles::<f32, ()>::from([0.0, 0.0, 0.0]);
        match self.imu.euler_angles() {
            Ok(val) => {
                euler_angles = val;
                Ok((euler_angles.a,euler_angles.b,euler_angles.c))
            }
            Err(e) => {
                eprintln!("{:?}", e);
                Err(())
            }
        }
    }
    pub fn read_gyro_q(&mut self) -> Result<Quaternion<f32>,()> {
        let mut euler_angles: EulerAngles<f32, ()>; // = EulerAngles::<f32, ()>::from([0.0, 0.0, 0.0]);
        match self.imu.quaternion(){
            Ok(val) => {
                Ok(val)
            }
            Err(e) => {
                eprintln!("{:?}", e);
                Err(())
            }
        }
    }
    pub fn read_acc(&mut self) -> Result<(f32,f32,f32),()> {
        match self.imu.linear_acceleration() {
            Ok(val) => {
                Ok((val.x,val.y,val.z))
            }
            Err(e) => {
                eprintln!("{:?}", e);
                Err(())
            }
        }
    }
}

pub fn diff(a: &Quaternion<f32>, b: &Quaternion<f32>) -> f32 {
    //https://math.stackexchange.com/questions/90081/quaternion-distance
    let dot = a.v.x * b.v.x + a.v.y * b.v.y + a.v.z * b.v.z + a.s * b.s;
    let arg = 2.0*dot*dot - 1.0;
    if arg > 1.0 {
        0.0
    } else if arg < -1.0 {
        std::f32::consts::PI
    } else {
        arg.acos()
    }
}


pub fn test() {
    let dev = I2cdev::new("/dev/i2c-1").expect("no dev...");
    let mut delay = Delay {};
    let mut imu = Bno055::new(dev).with_alternative_address();
    imu.init(&mut delay).expect("An error occurred while building the IMU");

    imu.set_mode(BNO055OperationMode::NDOF, &mut delay)
    .expect("An error occurred while setting the IMU mode");

    let mut status = imu.get_calibration_status().unwrap();
    println!("The IMU's calibration status is: {:?}", status);

    println!("- About to begin BNO055 IMU calibration...");
    let mut index = 0;
    while !imu.is_fully_calibrated().unwrap() {
        status = imu.get_calibration_status().unwrap();
        std::thread::sleep(std::time::Duration::from_millis(1000));
        println!("Calibration status: {:?}", status);
        if index > 100 {break;}
        index += 1;
    }
    println!("--------------------------------------------------------------");
    let calib = imu.calibration_profile(&mut delay).unwrap();
    imu.set_calibration_profile(calib, &mut delay).unwrap();
    println!(" - Calibration complete!");
    let mut euler_angles: EulerAngles<f32, ()>; // = EulerAngles::<f32, ()>::from([0.0, 0.0, 0.0]);
    match imu.euler_angles() {
        Ok(val) => {
            euler_angles = val;
            println!("\r{:.2} {:.2} {:.2}", euler_angles.a, euler_angles.b, euler_angles.c);
            thread::sleep(Duration::from_secs(1));
        }
        Err(e) => {
            eprintln!("{:?}", e);
        }
    }
    match imu.euler_angles() {
        Ok(val) => {
            euler_angles = val;
            println!("\r{:.2} {:.2} {:.2}", euler_angles.a, euler_angles.b, euler_angles.c);
            thread::sleep(Duration::from_secs(1));
        }
        Err(e) => {
            eprintln!("{:?}", e);
        }
    }
    match imu.euler_angles() {
        Ok(val) => {
            euler_angles = val;
            println!("\r{:.2} {:.2} {:.2}", euler_angles.a, euler_angles.b, euler_angles.c);
            thread::sleep(Duration::from_secs(1));
        }
        Err(e) => {
            eprintln!("{:?}", e);
        }
    }
    loop {
        match imu.euler_angles() {
            Ok(val) => {
                euler_angles = val;
                print!("\r{:.2} {:.2} {:.2}", euler_angles.a, euler_angles.b, euler_angles.c);
                stdout().flush();
                thread::sleep(Duration::from_secs(1));
            }
            Err(e) => {
                eprintln!("{:?}", e);
            }
        }
    }
}


pub fn calibrate() {
    let dev = I2cdev::new("/dev/i2c-1").expect("no dev...");
    let mut delay = Delay {};
    let mut imu = Bno055::new(dev).with_alternative_address();
    imu.init(&mut delay).expect("An error occurred while building the IMU");
    imu.set_mode(BNO055OperationMode::NDOF, &mut delay)
        .expect("An error occurred while setting the IMU mode");
    let mut status = imu.get_calibration_status().unwrap();
    println!("The IMU's calibration status is: {:?}", status);
    println!("- About to begin BNO055 IMU calibration...");
    // Wait for device to auto-calibrate.
    // Please perform steps necessary for auto-calibration to kick in.
    // Required steps are described in Datasheet section 3.11
    // Page 51, https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bno055-ds000.pdf (As of 2021-07-02)
    let mut index = 0;
    while !imu.is_fully_calibrated().unwrap() {
        status = imu.get_calibration_status().unwrap();
        std::thread::sleep(std::time::Duration::from_millis(1000));
        println!("Calibration status: {:?}", status);
        if index > 1000 {panic!("Calibration failed");}
        index += 1;
    }
    let calib = imu.calibration_profile(&mut delay).unwrap();
    //imu.set_calibration_profile(calib, &mut delay).unwrap();
    println!("Calibration forced complete!");

    let calib = calib.as_bytes();
    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .open("/home/pi/imucalibration").unwrap();
    file.write_all(calib);
}
fn get_calibration() -> BNO055Calibration {
    let mut f = File::open("/home/pi/imucalibration").expect("no file found");
    //let metadata = fs::metadata("imucalibration").expect("unable to read metadata");
    let mut buffer = [0; 22];
    f.read(&mut buffer).expect("buffer overflow");
    let calib = BNO055Calibration::from_buf(&buffer);
    calib
}