use rppal::i2c::I2c;
use vl53l0x::VL53L0x;
use rppal::gpio::{Gpio, OutputPin, InputPin};
use rppal::gpio::Trigger;
use std::time::{Duration, Instant};
use std::thread;
use std::sync::{Arc, Mutex, mpsc};
use rppal::gpio;
use std::thread::JoinHandle;
use std::sync::mpsc::{TryRecvError, Sender};
use crate::KILL;

/* For the VL53L0x light distance sensor (https://www.robotshop.com/de/de/tof-entfernungsmesser-sensor-breakout-platine-mit-spannungsregler-vl53l0x.html)
 used in Puzzibo cleaning robot.
 It uses I2C. For this to work we need, we need to wire all sensors to the following pins of the raspberry pi:
 The sensor has the following pins:
 VIN, GND, SDA, SCL,XSHUT, GPIO1 (unused), (Some versions have VCC, this is a voltage output, unused)
 They are connected to the raspberry pi:
 Sensor to Raspberry Pi
 VIN to any 3V3 on the Raspberry Pi (The energy source of the sensor)
 GND to any GND on the Raspberry Pi (GND ground pin, same ground as Raspberry Pi)
 XSHUT to a GPIO Pin on the Raspberry Pi (Switch sensor on and off)
 GPIO1 is unused
 SDA to GPIO 2 on the Raspberry Pi (I2C communication channel)
 SCL to GPIO 3 on the Raspberry Pi (I2C communication channel)
 SDA and SCL are by default on 2 and 3. This can be changed. I didn't change it.
 I2C needs to be activated in the Raspberry pi config (command line "raspi-config").
 When using multiple sensors of the same kind, we use the same pins on the Raspberry Pi
 for VIN, GND, SDA, and SCL. But use for each sensor a different GPIO on the Raspberry Pi for
 the XSHUT. So we can switch them on one after the other.
*/

pub(crate) struct LightDistanceSensor {
    names: Vec<String>,
    handler: JoinHandle<()>,
    terminator: Sender<i32>,
    data: Arc<Mutex<Vec<f32>>>,
}
impl LightDistanceSensor {
    pub(crate) fn new(shut_pins: Vec<u8>, names: Vec<&str>) -> Self {
        let (handler, terminator, data) = run_async(shut_pins);
        let mut names_strings = Vec::new();
        for name in names {
            names_strings.push(name.to_string());
        }
        Self{
            names: names_strings,
            handler,
            terminator,
            data,
        }
    }
    pub(crate) fn read(&mut self) -> Vec<f32> {
        self.data.lock().unwrap().clone()
    }
    pub(crate) fn get_name(&mut self) -> Vec<String> {
        self.names.clone()
    }
}
fn run_async(shut_pins: Vec<u8>) -> (JoinHandle<()>, Sender<i32>, Arc<Mutex<Vec<f32>>>) {
    let mut data = Arc::new(Mutex::new(vec![0.0;shut_pins.len()]));
    let data_for_thread = data.clone();
    let (tx, rx) = mpsc::channel();
    let mut buffer = vec![0.0;shut_pins.len()];

    let i2c = I2c::new().unwrap();
    let mut tof = VL53L0x::new(i2c).unwrap();
    tof.set_measurement_timing_budget(200000).unwrap();
    tof.start_continuous(0).unwrap();

    let handle = thread::spawn(move ||
        {
            let mut pins = Vec::new();
            for pin in shut_pins {
                pins.push(Gpio::new().unwrap().get(pin).unwrap().into_output())
            }
            for pin in &mut pins {
                pin.set_low();
            }
            loop {
                //First check if a termination signal was received
                match rx.try_recv() {
                    Ok(x) => {
                        if x == KILL {
                            break;
                        }
                    },
                    Err(TryRecvError::Disconnected) => { break; },
                    Err(TryRecvError::Empty) => {},
                };
                //Get measurement
                for i in 0..pins.len() {
                    pins[i].set_high();
                    match tof.read_range_continuous_millimeters_blocking() {
                        Ok(x) => {
                            buffer[i] = x as f32;
                        },
                        Err(e) => {
                            /*If error abort.*/
                            println!("Error in loop: {:?}", e);
                            break;
                        },
                    };
                    pins[i].set_low();
                }
                let mut data_locked = data_for_thread.lock().unwrap();
                for i in 0..pins.len() {
                    data_locked[i] = buffer[i];
                }
            }
        }
    );
    println!("Thread should run now");
    (handle, tx, data)
}

impl Drop for LightDistanceSensor {
    fn drop(&mut self) {
        //Try to kill the thread with the KILL signal.
        //If Result Error, ignore because then I don't know, probably the thread is death already.
        match self.terminator.send(KILL){
            Ok(_) => {}
            Err(e) => {
                println!("Drop Error: {}" ,e);
            }
        }
    }
}
