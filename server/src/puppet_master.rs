use std::borrow::BorrowMut;
use std::fs::File;
//Main thread that is not the server to control the robot
use rppal::i2c::I2c;

use rppal::gpio::{Gpio, OutputPin, InputPin};
use rppal::gpio::Trigger;
use std::time::{Duration, Instant};
use std::{fs, thread};
use std::io::{stdout, Write};
use std::sync::{Arc, mpsc};
use rppal::gpio;
use std::thread::JoinHandle;
use std::sync::mpsc::{TryRecvError, Sender};
use actix_web::{HttpRequest, HttpResponse, Responder, web};
use bno055::mint::{Quaternion, Vector3};
use futures::AsyncReadExt;
use rppal::gpio::Mode::Output;
use sensorlib::imu::IMU;
use opencv::prelude::{Mat, MatTraitConstManual};
use cameranavlib::camera::Camera;
use sensorlib::super_sonic::SuperSonic;
use futures::lock::{Mutex, MutexGuard};
use opencv::core::MatTraitConst;
use serde::{Deserialize, Serialize};
use motorlib::map::{Map};
use actix_web::{get, post, App, HttpServer};

use motorlib::motor_service::{MotorSystem, SCALE};
/*
  Important: I was able to remove smearing with: v4l2-ctl --help
  1. See what is possible for the camera:  v4l2-ctl --all
  2. Disable auto exposure, because this sucks: v4l2-ctl -c exposure_auto=1
  3. Set fixed exposure low (or high?): v4l2-ctl -c exposure_absolute=50
  This way I got pictures at daylight without blurring
*/


#[derive(Clone,Deserialize,Serialize)]
struct CurrentState {
    distances: Vec<f32>,
    pos: (f32,f32,f32),
    puppets_pos: Vec<(f32,f32,f32)>,
    message: String,
}

#[derive(Clone)]
pub struct PuppetMaster {
    //handler: JoinHandle<()>,
    commands: Sender<(i32, f32, f32)>,
    data: Arc<Mutex<CurrentState>>,
    //img_data: Arc<Mutex<(Vec<u8>, i32)>>,
}

impl PuppetMaster {
    pub fn new() -> Self {
        let (th,commands,data) = run_async();

        Self {
            commands,
            data,
        }
    }

}

/**/
fn run_async() -> (
    JoinHandle<()>,
    Sender<(i32, f32, f32)>,
    Arc<Mutex<CurrentState>>,
) {
    let mut data = Arc::new(Mutex::new(CurrentState{
        distances: vec![],
        pos: (0.0, 0.0, 0.0),
        puppets_pos: vec![],
        message: "".to_string(),
    }));
    let data_handler = data.clone();
    let (tx, rx) = mpsc::channel();
    println!("Start the Puppet Master thread...");
    let handle= thread::spawn(move || {
        //Load the plan
        let mut plan = Plan{
            steps: vec![
                Step::RelativeGoTo {
                    x: 100.0,
                    y: 0.0,
                },
                Step::RelativeGoTo {
                    x: 100.0,
                    y: 0.0,
                },
            ]
        };
        //Ultrasound sensors init
        println!("SuperSonic: TRY");
        let mut sensor = SuperSonic::new(
            vec![15, 7, 25, 23],
            vec![14, 8, 24, 18],
            vec![
                "Front Left".to_string(),
                "Front Middle".to_string(),
                "Front Right".to_string(),
                "Backside".to_string()]
        );
        thread::sleep(Duration::from_secs(1));
        println!("SuperSonic: TRY FINISHED");

        //Motor control
        println!("Motor: TRY");
        let mut motor = MotorSystem::new(true);
        let mut map = Map::new("test", motor.get_ticks_blocking().unwrap());
        println!("Motor: TRY FINISHED");
        thread::sleep(Duration::from_secs(60));
        //Camera
        println!("Camera: TRY");
        let mut cam = Camera::new(0).unwrap();
        let mut img = Mat::default();
        let mut gray = Mat::default();
        println!("Camera: TRY FINISHED");
        /**/
        loop {
            //Clean the ticks buffer
            match motor.get_ticks_unblocking().expect("Points Channel got disconnected. Must shut down.") {
                Some(x) => {}
                None => {break;}
            };
        }
        let mut directive = Directive::Stop;
        let mut step = plan.steps.pop().unwrap();
        let mut start_step = true;
        println!("Puppet Master Loop Start ...");
        loop {
            //Control ##############################################################################
            match rx.try_recv() {
                Ok((com, v1, v2)) => {
                    match com {
                        0 => {directive=Directive::Stop; motor.stop();}
                        1 => {directive=Directive::Manuel; motor.go_forward(10.0);}
                        2 => {directive=Directive::Manuel; motor.forward_left(20.0,10.0);}
                        3 => {directive=Directive::Manuel; motor.forward_right(20.0,10.0);}
                        4 => {directive=Directive::Manuel; motor.go_backward(10.0);}
                        5 => {directive=Directive::Manuel; motor.backward_left(20.0,10.0);}
                        6 => {directive=Directive::Manuel; motor.backward_right(20.0,10.0);}
                        7 => {directive=Directive::Auto; }
                        _ => {}
                    }


                }
                Err(TryRecvError::Disconnected) => { println!("ERROR");break; }
                Err(TryRecvError::Empty) => {}
            };
            //Data collection ######################################################################
            cam.take_img(&mut img);
            let distances = sensor.read();
            loop {
                match motor.get_ticks_unblocking().expect("Points Channel got disconnected. Must shut down.") {
                    Some(x) => {
                        map.update_moved_from_step(&x);
                    }
                    None => {break;}
                };
            }
            if let Some(mut data_locked) = data_handler.try_lock() {
                data_locked.distances = distances;
                data_locked.pos = (map.x, map.y, 0.0);
            }

            //Actions ##############################################################################
            match directive {
                Directive::Stop => {motor.stop();}
                Directive::Manuel => {}
                Directive::Auto => {
                    if start_step {
                        match step {
                            Step::RelativeGoTo {x,y} => {motor.to_relative_pos(x,y);}
                            Step::RelativeRotate {angle} => {}
                            Step::PuppetGoTo { .. } => {}
                            Step::PuppetAreaClean { .. } => {}
                            Step::Finish => {}
                        }
                        start_step = false;
                    }
                    match motor.finish_unblocking() {
                        Ok(x) => {
                            match x {
                                Some(x) => {
                                    start_step = true;
                                    match plan.steps.pop() {
                                        Some(s) => {step = s;}
                                        None => {directive = Directive::Stop;}
                                    }
                                }
                                None => {}
                            }
                        }
                        Err(_) => {}
                    };
                }
            }
        }
    });
    println!("Main Thread should run now ...");
    (handle, tx, data)
}


struct Plan {
    steps: Vec<Step>,
}

enum Step {
    RelativeGoTo{x:f32, y:f32},
    RelativeRotate{angle:f32},
    PuppetGoTo{x:f32, y:f32, puppet_id: i32},
    PuppetAreaClean{goal_x: f32, goal_y: f32, puppet_id: i32},
    Finish,
}







enum Directive {
    Stop,
    Manuel,
    Auto,
}


#[get("/puppetmaster/state")]
pub async fn get_current_state(req: HttpRequest, actor: web::Data<PuppetMaster>) -> impl Responder {
    println!("Get current state of puppet master");
    if let Some(data_locked) = actor.data.try_lock(){
        let data = data_locked.clone();
        let data = serde_json::to_string(&data).expect("JSON parsing failed in puppet master get_current_state");
        return HttpResponse::Ok().body(data)
    }
    println!("Failed to get lock on puppet master");
    HttpResponse::Ok().body("")
}
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PuppetReq {
    c_1: i32,
    c_2: i32,
    c_3: i32,
    c_4: i32,
}

#[get("/puppet/update")]
pub async fn puppet_update(req: HttpRequest, actor: web::Data<PuppetMaster>) -> impl Responder {
    println!("Puppet requested get new orders");
    //TODO the main loop must communicate to the puppet what is the current intention of them to move
    let new_req = PuppetReq {
        c_1: 0, //links rückwarts
        c_2: 0, //links vorwärts
        c_3: 0,//Rechts vorwärts
        c_4: 0, //Rechts geht nich todo
    };
    let answer = serde_json::to_string(&new_req).expect("Cannot serilize PuppetReq");
    println!("Send to puppet: {}", &answer);
    HttpResponse::Ok().body(answer)
}
