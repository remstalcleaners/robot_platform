use std::borrow::BorrowMut;
use std::fs::File;
//Main thread that is not the server to control the robot
use rppal::i2c::I2c;

use rppal::gpio::{Gpio, OutputPin, InputPin};
use rppal::gpio::Trigger;
use std::time::{Duration, Instant};
use std::{fs, thread};
use std::io::{stdout, Write};
use std::sync::{Arc, mpsc};
use rppal::gpio;
use std::thread::JoinHandle;
use std::sync::mpsc::{TryRecvError, Sender};
use bno055::mint::{Quaternion, Vector3};
use futures::AsyncReadExt;
use rppal::gpio::Mode::Output;
use sensorlib::imu::IMU;
use opencv::prelude::{Mat, MatTraitConstManual};
use cameranavlib::camera::Camera;
use sensorlib::super_sonic::SuperSonic;
use futures::lock::{Mutex, MutexGuard};
use opencv::core::MatTraitConst;
use serde::{Deserialize, Serialize};
use motorlib::map::{Map};

use motorlib::motor_service::{MotorSystem, SCALE};


/*
  Important: I was able to remove smearing with: v4l2-ctl --help
  1. See what is possible for the camera:  v4l2-ctl --all
  2. Disable auto exposure, because this sucks: v4l2-ctl -c exposure_auto=1
  3. Set fixed exposure low (or high?): v4l2-ctl -c exposure_absolute=50
  This way I got pictures at daylight without blurring
*/

#[derive(Clone)]
pub struct Main {
    //handler: JoinHandle<()>,
    commands: Sender<(i32, f32, f32)>,
    data: Arc<Mutex<Vec<f32>>>,
    img_data: Arc<Mutex<(Vec<u8>, i32)>>,
    map_data: Arc<Mutex<Vec<u8>>>,
}
impl Main {
    pub fn new() -> Self {
        let (handler, commands, data, img_data, map_data) = run_async();
        Self{
            //handler,
            commands: commands,
            data: data,
            img_data: img_data,
            map_data: map_data,
        }
    }
    pub async fn read(&self) -> Vec<f32> {
        self.data.lock().await.to_vec()
    }
    pub async fn read_img(&self) -> Vec<u8> {
        self.img_data.lock().await.0.to_vec()
    }
    pub async fn read_map(&self) -> Vec<u8> {
        self.map_data.lock().await.to_vec()
    }

    pub async fn send(&self, typ: i32, first: f32, second: f32) {
        self.commands.send((typ,first,second));
    }
    pub async fn aut_start(&self) {
        println!("Start Robot autonomous drive ... ");
        self.commands.send((127,0.0,0.0));
    }
}
/**/
fn run_async() -> (
    JoinHandle<()>,
    Sender<(i32, f32, f32)>,
    Arc<Mutex<Vec<f32>>>,
    Arc<Mutex<(Vec<u8>, i32)>>,
    Arc<Mutex<Vec<u8>>>,
) {
    println!("ZTZ Init main ...");
    //Communication infrastructure
    //Output data for sensor data and numbers
    let mut data = Arc::new(Mutex::new(vec![0.0;4]));
    let data_handler = data.clone();
    //Output data for images
    let img_data = Arc::new(Mutex::new((vec![0;10], 0)));
    let img_data_handler = img_data.clone();
    let map_data = Arc::new(Mutex::new(vec![0;10000]));
    let mut map_data_handler = map_data.clone();
    let (tx, rx) = mpsc::channel();

    /**/
    println!("ZTZ Create main thread ...");
    let handle= thread::spawn(move || {
        //Ultrasound sensors init
        println!("SuperSonic: TRY");
        let mut sensor = SuperSonic::new(
            vec![15, 7, 25, 23],
            vec![14, 8, 24, 18],
            vec![
                "Front Left".to_string(),
                "Front Middle".to_string(),
                "Front Right".to_string(),
                "Backside".to_string()]
        );
        thread::sleep(Duration::from_secs(1));
        println!("SuperSonic: TRY FINISHED");

        //Motor control
        println!("Motor: TRY");
        let mut motor = MotorSystem::new(true);
        println!("Motor: TRY FINISHED");
        thread::sleep(Duration::from_secs(60));
        //Camera
        println!("Camera: TRY");
        let mut cam = Camera::new(0).unwrap();
        let mut img = Mat::default();
        let mut gray = Mat::default();
        println!("Camera: TRY FINISHED");
        /**/
        let mut time = Instant::now();
        /**/
        let mut desire = Desire::ExGoal;
        let mut start = true;
        let mut map = Map::new("test", motor.get_ticks_blocking().unwrap());
        let mut to_go_x = 0.0;
        let mut part_right = false;
        //##########################################################################################
        //##########################################################################################
        println!("ZTZ Before wait Loop Start ...");
        thread::sleep(Duration::from_secs(5));
        println!("ZTZ Loop Start ...");
        loop {
            thread::sleep(Duration::from_millis(100));
            println!("\r({}, {})", map.x, map.y);
            stdout().flush().unwrap();


            match rx.try_recv() {
                Ok((x, y, z)) => {
                    match x {
                        42 => {
                            motor.stop();
                            desire = Desire::Idle;
                            start = false;
                            motor.stop();
                            motor.kill();
                            thread::sleep(Duration::from_secs(2));
                            println!("STOP");
                            break;
                            /* if 42 is send end this thread */
                        }
                        1 => {
                            /* 1 = motor go straight for given seconds */
                            println!("Go Straigth");
                            //motor.straight(z, y, false);
                        }
                        2 => {
                            /* 2 = motor go straight backwards for given seconds */
                            println!("Go Straigth back");
                            //motor.straight(z, y, true);
                        }
                        3 => {
                            /* 3 = motor turn clockwise */
                            println!("Go right");
                            //motor.turn(&mut imu, y, false);
                        }
                        4 => {
                            /* 4 = motor turn counter clockwise */
                            println!("Go left");
                            //motor.turn(&mut imu, y, true);
                        }
                        127 => {
                            desire = Desire::ExGoal;
                            start = true;
                        }
                        /* If something random or zero comes ignore it */
                        _ => {println!("Nothing");}
                    }
                }
                Err(TryRecvError::Disconnected) => { println!("ERROR");break; }
                Err(TryRecvError::Empty) => {}
            };
            cam.take_img(&mut img);
            let distances = sensor.read();
            if let Some(mut data_locked) = data_handler.try_lock() {
                data_locked[0] = distances[0];
                data_locked[1] = distances[1];
                data_locked[2] = distances[2];
                data_locked[3] = distances[3];
            }
            loop {
                match motor.get_ticks_unblocking().expect("Points Channel got disconnected. Must shut down.") {
                    Some(x) => {
                        map.update_moved_from_step(&x);
                    }
                    None => {break;}
                };
            }
            /*
            match map_data_handler.try_lock(){
                None => {}
                Some(mut x) => {
                    *x = map.get_map_1d();
                }
            };
             */
            //If just switched to another desire and set start as true
            if start {
                start = false;
                match desire {
                    Desire::Idle => {
                        motor.stop();
                    }
                    Desire::InGoal => {
                        desire = Desire::Idle;
                        start = true; //Make only one round
                    }
                    Desire::ExGoal => {
                        if part_right {
                            motor.backward_right(1500 as f32 * SCALE, 40.0)
                        } else {
                            motor.backward_left(1500 as f32 * SCALE,40.0);
                        }
                    }
                    Desire::AwayX => {
                        motor.stop();
                        //to_go_x = map.get_free_x(part_right).unwrap();
                        println!("Start AwayX to {}", to_go_x);
                        println!();
                    }
                    Desire::AwayY => {
                        time = Instant::now();
                    }
                    Desire::Return => {}
                }
            }
            //In the run
            match desire {
                Desire::Idle => {
                    thread::sleep(Duration::from_secs(1));
                }
                Desire::InGoal => {}
                Desire::ExGoal => {
                    let signal = motor.finish_unblocking().expect("Signal Channel got disconnected. Must shut down.");
                    match signal {
                        Some(x) => {
                            motor.stop();
                            desire = Desire::AwayX;
                            start = true;
                            println!("Set Desire to AwayX");
                        }
                        None => {}
                    }
                }
                Desire::AwayX => {
                    let dx = (to_go_x - map.y).abs();
                    if dx < 21.0 {
                        desire = Desire::AwayY;
                        start = true;
                        println!("Set Desire to AwayY");
                    }
                    if time.elapsed().as_secs() > 10 {
                        map.paint("testop");
                        thread::sleep(Duration::from_secs(5));
                        panic!("Time over");
                    }
                    //motor.to_direction(dx, 0.0);
                }
                Desire::AwayY => {
                    //motor.to_direction(0.0, 1.0);
                    let left = distances[0];
                    let middle = distances[1];
                    let right= distances[2];
                    if left < 80.0 && middle < 60.0 && right < 80.0 {
                        desire = Desire::Return;
                        start = true;
                        println!("Set Desire to Return");
                    }
                }
                Desire::Return => {
                    //motor.to_direction(-map.x, -map.y);
                    if map.x < 10.0 && map.y < 10.0 {
                        desire = Desire::InGoal;
                        start = true;
                        println!("Set Desire to InGoal");
                    }
                }
            }
        }
        //##########################################################################################
        //##########################################################################################
    });
    println!("Main Thread should run now ...");
    (handle, tx, data, img_data, map_data)
}


enum Desire {
    Idle,
    InGoal,
    ExGoal,
    AwayX,
    AwayY,
    Return,
}









