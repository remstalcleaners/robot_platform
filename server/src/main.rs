mod operation;
mod demo_1;
mod simplepath;
mod actor;
mod dummy;
mod puppet_master;


extern crate core;

use std::fs;
use std::io::Bytes;
use std::ops::Deref;
use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder, HttpRequest};
use actix_web::body::BoxBody;
use std::path::PathBuf;
use actix_cors::Cors;
use actix_web::http::StatusCode;
use futures::FutureExt;
use actix_files::Files;
use plannerlib::simple_planner::SimplePlanner;
use crate::actor::Actor;
use crate::demo_1::Main;
use serde::{Deserialize, Serialize};
use crate::dummy::DummyActor;
use structurelib::*;
/*
Important: I was able to remove smearing with: v4l2-ctl --help
1. See what is possible for the camera:  v4l2-ctl --all
2. Disable auto exposure, because this sucks: v4l2-ctl -c exposure_auto=1
3. Set fixed exposure low (or high?): v4l2-ctl -c exposure_absolute=50
#=100 was good too
This way I got pictures at daylight without blurring
*/

pub type JSON = String;
//const ADRESSE: &str = "192.168.1.1:8081";
//const ADRESSE: &str = "192.168.178.45:8081";
const ADRESSE: &str = "192.168.178.47:8081";
//const ADRESSE: &str = "127.0.0.1:8081";
/*
//Get all sensor informations
//Ultrasound sensors and current position on the map
#[get("/robot/state")]
async fn current_state(req: HttpRequest) -> impl Responder {
    let res = req.app_data::<Actor>().expect("Main doesn't exist, but should.");
    let data = res.get_state().await;
    let text = serde_json::to_string(&data).unwrap();
    HttpResponse::Ok().body(text)
}

/*
//Get the current image from the robot camera
#[get("/robot/img")]
async fn images(req: HttpRequest) -> impl Responder {
    let res = req.app_data::<Main>().expect("Main doesn't exist, but should.");
    let data = res.read_img().await;
    HttpResponse::Ok().body(data)
}
 */
//Get map changes from the robot camera
#[get("/robot/map")]
async fn map(req: HttpRequest) -> impl Responder {
    //let res = req.app_data::<Actor>().expect("Main doesn't exist, but should.");
    //let data = res.read_map().await;
    //HttpResponse::Ok().body(data)
    HttpResponse::Ok().body("Not yet")
}

//Motor control
#[derive(serde::Deserialize)]
struct MotorControl {
    typ: i32,
    first: f32,
    second: f32,
}
#[post("/robot/motor")]
async fn motor((req, activity): (HttpRequest, web::Json<MotorControl>)) -> impl Responder {
    let res = req.app_data::<Actor>().expect("Main doesn't exist, but should.");
    let typ = activity.typ;
    let first = activity.first;
    let second = activity.second;
    println!("Motor Signal {} {} {}", typ, first, second);
    res.send(typ, first, second, 0.0, 0.0, 0.0).await;
    HttpResponse::Ok().body("OK")
}


#[post("/robot/shutdown")]
async fn shutdown(req: HttpRequest)  -> impl Responder {
    println!("Shutdown called");
    HttpResponse::Ok().body(OK_HTML)
}

 */

//For testing
#[get("/puppetmaster/test")]
async fn testtest(req: HttpRequest) -> impl Responder {
    println!("Test Called");
    HttpResponse::Ok().body(format!("Robot connection OK"))
}





#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("New Robot Server starting at {}...", ADRESSE);
    /*
    let structure_prototype_1 = Structure {
        camera: Some(0),
        supersonic_sensors: vec![
            (15,14,"Front Left".to_string()),
            (7,8,"Front Middle".to_string()),
            (25,24,"Front Right".to_string()),
            (23,18,"Backside".to_string())
        ],
        odometry: None,
        puppets: vec![0],
        imu: true
    };
    let structure_pc_test = Structure {
        camera: Some(0),
        supersonic_sensors: vec![],
        odometry: None,
        puppets: vec![0],
        imu: false,
    };

    let actor = Actor::new(SimplePlanner::new(), structure_pc_test);

    let main_thread = web::Data::new(DummyActor::new());


     */
    let actor = puppet_master::PuppetMaster::new();

    HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_header()
            .allow_any_method();
        App::new()
            .wrap(cors)
            .app_data(web::Data::new(actor.clone()))
            .service(testtest)
            .service(actix_files::Files::new("", "/home/pi/html/"))
            .service(puppet_master::puppet_update)
            .service(puppet_master::get_current_state)
    })
        .bind(ADRESSE)?
        .run()
        .await
}

//#################################################################################################
//Invalid answer
const INVALID_HTML: &str =
    "<html><head><meta charset=\"utf-8\"><title>INVALID</title></head><body>INVALID</body></html>";
//Ok answer
const OK_HTML: &str =
    "<html><head><meta charset=\"utf-8\"><title>OK</title></head><body>OK</body></html>";

fn send<T: serde::Serialize>(content: &T) -> HttpResponse<BoxBody> {
    return match serde_json::to_string(content) {
        Ok(x) => HttpResponse::Ok().body(x),
        Err(e) => {
            println!("{}", e);
            HttpResponse::Ok().body(INVALID_HTML)
        },
    }
}
async fn sending<T: serde::Serialize,V: std::fmt::Debug>(test: Result<T,V>) -> HttpResponse<BoxBody>   {
    return match test {
        Ok(x) => {
            return send(&x);
        }
        Err(e) => {println!("{:?}", e); HttpResponse::Ok().body(INVALID_HTML)}
    }
}
