use std::borrow::BorrowMut;
use std::fs::File;
//Main thread that is not the server to control the robot
use rppal::i2c::I2c;

use rppal::gpio::{Gpio, OutputPin, InputPin};
use rppal::gpio::Trigger;
use std::time::{Duration, Instant};
use std::{fs, thread};
use std::io::{stdout, Write};
use std::sync::{Arc, mpsc};
use rppal::gpio;
use std::thread::JoinHandle;
use std::sync::mpsc::{TryRecvError, Sender};
use bno055::mint::{Quaternion, Vector3};
use futures::AsyncReadExt;
use rppal::gpio::Mode::Output;
use sensorlib::imu::IMU;
use opencv::prelude::{Mat, MatTraitConstManual};
use cameranavlib::camera::Camera;
use sensorlib::super_sonic::SuperSonic;
use futures::lock::{Mutex, MutexGuard};
use opencv::core::MatTraitConst;
use serde::{Deserialize, Serialize};
use motorlib::map::{Map};

use motorlib::motor_service::{MotorSystem, SCALE};
use sensorlib::super_sonic;


#[derive(Clone)]
pub struct Main {
    //handler: JoinHandle<()>,
    commands: Sender<(i32, f32, f32)>,
    data: Arc<Mutex<Vec<f32>>>,
    map_data: Arc<Mutex<Vec<u8>>>,
}

impl Main {
    pub fn new() -> Self {
        let (handler, commands, data, map_data) = run_async();
        Self {
            //handler,
            commands,
            data,
            map_data,
        }
    }
    pub async fn read(&self) -> Vec<f32> {
        self.data.lock().await.to_vec()
    }
    pub async fn read_map(&self) -> Vec<u8> {
        self.map_data.lock().await.to_vec()
    }

    pub async fn send(&self, typ: i32, first: f32, second: f32) {
        self.commands.send((typ,first,second));
    }
    pub async fn auto_start(&self) {
        println!("Start Robot autonomous drive ... ");
        self.commands.send((127,0.0,0.0));
    }
}
enum State {
    Idle,
    ExGoal,
    ToX,
    Up,
    Turn1,
    Back,
}
fn run_async() -> (
    JoinHandle<()>,
    Sender<(i32, f32, f32)>,
    Arc<Mutex<Vec<f32>>>,
    Arc<Mutex<Vec<u8>>>,
) {
    println!("Start Demo 1 preparations ...");
    //Input
    let (tx, rx) = mpsc::channel();
    //Outputs
    //Sensor data
    let mut data = Arc::new(Mutex::new(vec![0.0;4]));
    let data_handler = data.clone();
    //Map data
    let map_data = Arc::new(Mutex::new(vec![0;10000]));
    let mut map_data_handler = map_data.clone();
    println!("Finished Demo 1 preparations.");
    //#############################################################################################
    let handle= thread::spawn(move || {
        println!("Start Demo 1 Thread ...");
        //Make persistent objects
        //Sensors
        let mut sensor = SuperSonic::new(
            vec![15, 7, 25, 23],
            vec![14, 8, 24, 18],
            vec![
                "Front Left".to_string(),
                "Front Middle".to_string(),
                "Front Right".to_string(),
                "Backside".to_string()]
        );
        //Motor control
        let mut motor = MotorSystem::new(true);
        //Make camera
        let mut cam = Camera::new(0).unwrap();
        let mut img = Mat::default();



        //Make control
        let mut map = Map::new("demo 1", motor.get_ticks_blocking().unwrap());
        let mut x_to = 0.0;
        let mut state = State::Idle;
        let mut start = true;
        let mut over = false;
        let mut index = 0;

        println!("Start Demo 1 Loop ...");
        loop {
            //Data collection
            cam.take_img(&mut img);
            let sensors = sensor.read();
            loop {
                match motor.get_ticks_unblocking().expect("Points Channel got disconnected. Must shut down.") {
                    Some(x) => {
                        map.update_moved_from_step(&x);
                        if sensors[1] < 80.0 {
                            map.update_blocking((0.0, sensors[1]), &x);
                        }
                    }
                    None => {break;}
                };
            }
            {
                if let Some(mut data_locked) = data.try_lock() {
                    data_locked[0] = sensors[0];
                    data_locked[1] = sensors[1];
                    data_locked[2] = sensors[2];
                    data_locked[3] = sensors[3];
                }
            }

            //Command receiver
            match rx.try_recv() {
                Ok((x, y, z)) => {
                    match x {
                        42 => {
                            motor.stop();
                            state = State::Idle;
                            start = false;
                            motor.stop();
                            motor.kill();
                            thread::sleep(Duration::from_secs(2));
                            println!("STOP");
                            break;
                            /* if 42 is send end this thread */
                        }
                        0 => {
                            println!("Stop");
                            state = State::Idle;
                            start = false;
                            motor.stop();
                        }
                        1 => {
                            println!("Go Straigth");
                            state = State::Idle;
                            start = false;
                            motor.go_forward(1000 as f32 * SCALE);
                        }
                        2 => {
                            println!("Go Straigth back");
                            state = State::Idle;
                            start = false;
                            motor.go_backward(1000 as f32 * SCALE);
                        }
                        3 => {
                            println!("Go right");
                            state = State::Idle;
                            start = false;
                            motor.forward_right(500 as f32 * SCALE,40.0);
                        }
                        4 => {
                            println!("Go left");
                            state = State::Idle;
                            start = false;
                            motor.forward_left(500 as f32 * SCALE,40.0);
                        }
                        5 => {
                            println!("Go right backwards");
                            state = State::Idle;
                            start = false;
                            motor.backward_right(500 as f32 * SCALE,40.0);
                        }
                        6 => {
                            println!("Go left backwards");
                            state = State::Idle;
                            start = false;
                            motor.backward_left(500 as f32 * SCALE, 40.0);
                        }

                        127 => {
                            println!("Start autonomous control");
                            state = State::ExGoal;
                            start = true;
                        }
                        /* If something random or zero comes ignore it */
                        _ => {println!("Nothing");}
                    }
                }
                Err(TryRecvError::Disconnected) => { println!("ERROR");break; }
                Err(TryRecvError::Empty) => {}
            };
            //Decision making if autonomous
            //-------------------------------------------------------------------------------------
            //Control for autonomous driving
            //TODO: Auto ...
            if start {
                match state {
                    State::Idle => {motor.stop();}
                    State::ExGoal => {
                        println!("1. Go backward out of goal");
                        motor.backward_left(1200 as f32 * SCALE, 40.0);
                    }
                    State::ToX => {
                        println!("Go to x position: {} {} -> {} {}", map.x, map.y, x_to - map.x, -50.0 - map.y);
                        motor.to_relative_pos(x_to - map.x, -50.0 - map.y);
                    }
                    State::Up => {
                        println!("Go up: {} {} -> {} {}", map.x, map.y, x_to - map.x, -500000.0 - map.y);
                        motor.to_relative_pos(x_to - map.x, -500000.0 - map.y);
                    }
                    State::Turn1 => {
                        println!("Go a bit to center: {} {} -> {} {}", map.x, map.y, x_to - map.x + 40.0, 0.0);
                        motor.forward_right(2500 as f32 * SCALE,40.0);
                    }
                    State::Back => {
                        println!("Go back to goal: {} {} -> {} {}", map.x, map.y, 0.0 - map.x, 0.0 - map.y);
                        motor.to_relative_pos(0.0 - map.x, 0.0 - map.y);
                    }
                }
                start = false;
            } else {
                match state {
                    State::Idle => {}
                    State::ExGoal => {}
                    State::ToX => {
                        if sensors[1] < 50.0 {
                            state = State::Up;
                            start = true;
                            println!("Set over because side wall detected.");
                            over = true;
                        }
                    }
                    State::Up => {
                        if sensors[1] < 70.0 {
                            state = State::Turn1;
                            start = true;
                        }
                    }
                    State::Turn1 => {}
                    State::Back => {}
                }
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                match state {
                                    State::Idle => {motor.stop();}
                                    State::ExGoal => {state=State::ToX; start = true;}
                                    State::ToX => {state=State::Up; start = true;}
                                    State::Up => {state=State::Turn1; start = true;}
                                    State::Turn1 => {state=State::Back; start = true;}
                                    State::Back => {
                                        state=State::ExGoal;
                                        start = true;
                                        x_to += 40.0;
                                        motor.stop();
                                        map.paint(&format!("AR{}", index));
                                        index += 1;
                                        println!("GP: {}: {} ({})", index, x_to, over);
                                        if over {
                                            state=State::Idle;
                                            motor.stop();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
        }
    });
    //#############################################################################################
    (handle, tx, data_handler, map_data_handler)
}

