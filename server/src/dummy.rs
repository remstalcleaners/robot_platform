use std::borrow::BorrowMut;
use std::fs::File;
//Main thread that is not the server to control the robot
use rppal::i2c::I2c;

use rppal::gpio::{Gpio, OutputPin, InputPin};
use rppal::gpio::Trigger;
use std::time::{Duration, Instant};
use std::{fs, thread};
use std::io::{stdout, Write};
use std::rc::Rc;
use std::sync::{Arc, mpsc};
use rppal::gpio;
use std::thread::JoinHandle;
use std::sync::mpsc::{TryRecvError, Sender, Receiver, SendError};
use bno055::mint::{Quaternion, Vector3};
use futures::AsyncReadExt;
use rppal::gpio::Mode::Output;
use sensorlib::imu::IMU;
use opencv::prelude::{Mat, MatTraitConstManual};
use cameranavlib::camera::Camera;
use sensorlib::super_sonic::SuperSonic;
use futures::lock::{Mutex, MutexGuard};
use opencv::core::MatTraitConst;
use serde::{Deserialize, Serialize};
use motorlib::map::{Map};

use motorlib::motor_service::{MotorSystem, SCALE};
use sensorlib::super_sonic;
use plannerlib::planner::Planner;
use crate::actor::StateData;

/* Notes:
  Important: I was able to remove smearing with: v4l2-ctl --help
  1. See what is possible for the camera:  v4l2-ctl --all
  2. Disable auto exposure, because this sucks: v4l2-ctl -c exposure_auto=1
  3. Set fixed exposure low (or high?): v4l2-ctl -c exposure_absolute=50
  This way I got pictures at daylight without blurring

  This is the part of the server that is executing the planner and controls the robot,
  and collects the input data.
*/

#[derive(Clone)]
pub struct DummyActor {
    pub(crate) index: i32,
}
impl DummyActor {
    pub fn new() -> Self {
        Self {
            index: 0,
        }
    }
    pub async fn get_state(&self) -> StateData {
        StateData{
            distance: vec![],
            x: 0.0,
            y: 0.0,
            a: 0.0,
        }
    }
    pub async fn send(&self, key: i32, x: f32, y: f32, a: f32, b: f32, c: f32) {
    }
}

