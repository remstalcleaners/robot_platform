use std::borrow::BorrowMut;
use std::fs::File;
//Main thread that is not the server to control the robot
use rppal::i2c::I2c;

use rppal::gpio::{Gpio, OutputPin, InputPin};
use rppal::gpio::Trigger;
use std::time::{Duration, Instant};
use std::{fs, thread};
use std::io::{stdout, Write};
use std::rc::Rc;
use std::sync::{Arc, mpsc};
use rppal::gpio;
use std::thread::JoinHandle;
use std::sync::mpsc::{TryRecvError, Sender, Receiver, SendError};
use bno055::mint::{Quaternion, Vector3};
use futures::AsyncReadExt;
use rppal::gpio::Mode::Output;
use sensorlib::imu::IMU;
use opencv::prelude::{Mat, MatTraitConstManual};
use cameranavlib::camera::Camera;
use sensorlib::super_sonic::SuperSonic;
use futures::lock::{Mutex, MutexGuard};
use opencv::core::MatTraitConst;
use serde::{Deserialize, Serialize};
use motorlib::map::{Map};

use motorlib::motor_service::{MotorSystem, SCALE};
use sensorlib::super_sonic;
use plannerlib::planner::Planner;
use structurelib::*;

/* Notes:
  Important: I was able to remove smearing with: v4l2-ctl --help
  1. See what is possible for the camera:  v4l2-ctl --all
  2. Disable auto exposure, because this sucks: v4l2-ctl -c exposure_auto=1
  3. Set fixed exposure low (or high?): v4l2-ctl -c exposure_absolute=50
  This way I got pictures at daylight without blurring

  This is the part of the server that is executing the planner and controls the robot,
  and collects the input data.
*/
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct StateData {
    pub(crate) distance: Vec<f32>,
    pub(crate) x: f32,
    pub(crate) y: f32,
    pub(crate) a: f32,
}
#[derive(Clone)]
pub struct Actor {
    commands: Sender<(i32, f32, f32, f32, f32, f32)>,
    data: Arc<Mutex<StateData>>,
}
impl Actor {
    pub fn new<T: Planner + 'static>(planner: T, structure: Structure) -> Self {
        let (handler, tx, state) = run_async(planner /*, datatx*/, structure);
        Self {
            commands: tx,
            data: state
        }
    }
    pub async fn get_state(&self) -> StateData {
        self.data.lock().await.clone()
    }
    pub async fn send(&self, key: i32, x: f32, y: f32, a: f32, b: f32, c: f32) {
        match self.commands.send((key, x, y, a, b, c)) {
            Ok(_) => {}
            Err(e) => {
                println!("Error in send: {:?}", e);
            }
        }
    }
}


fn run_async<T: Planner + 'static>(mut planner: T /*, output: Sender<UpdateData>*/, structure: Structure) -> (
    JoinHandle<()>,
    Sender<(i32, f32, f32, f32, f32, f32)>,
    Arc<Mutex<StateData>>
)
{
    println!("Prepare Actor ...");
    //Init the cross communication tools
    let (tx, rx) = mpsc::channel();
    let mut state_data = Arc::new(Mutex::new(
        StateData {
            distance: vec![0.0; structure.supersonic_sensors.len()],
            x: 0.0,
            y: 0.0,
            a: 0.0,
        }
    ));
    let state_data_handler = state_data.clone();
    let mut trigger_pins = Vec::with_capacity(structure.supersonic_sensors.len());
    let mut echo_pins = Vec::with_capacity(structure.supersonic_sensors.len());
    let mut pin_names = Vec::with_capacity(structure.supersonic_sensors.len());
    for pin_info in &structure.supersonic_sensors {
        trigger_pins.push(pin_info.0);
        echo_pins.push(pin_info.1);
        pin_names.push(pin_info.2.clone());
    }
    println!("Finished actor cross communication setup.");
    //#########################################################################
    //Spawn the main thread of the actor to run in parallel to the server api
    let handle= thread::spawn(move || {
        println!("Start Actor Main Thread ...");
        //Init objects before the loop
        //Supersonic Sensors
        let mut sensor = SuperSonic::new(
            trigger_pins,
            echo_pins,
            pin_names
        );
        //Motor control
        let mut motor = MotorSystem::new(structure.imu);
        //Make camera
        let mut cam = Camera::new(0).unwrap();
        let mut img = Mat::default();
        let mut hsv = Mat::default();
        //Map
        //thread::sleep(Duration::from_secs(120));
        motor.clean_ticks();
        let mut map = Map::new("Actor Map", motor.get_ticks_blocking().unwrap());
        let mut autonome = false;
        println!("Start Actor Loop ...");
        loop
        {
            /*
                Structure
                1. Do camera stuff, take img and process
                2. Take sensor and wheel rotation data and update own map
                3. Put above results into a current state struct
                4. Receive commands
                5. Use above information for planning and action
                6. Start from 1
            */
            //Data collection

            // 1.
            //Camera data
            cam.take_img(&mut img);
            println!("IMG: {:?}", img.size());
            //Camera processing
            //TODO: Process image to use for planners


            // 2.
            //Ultrasound sensors
            let sensors = sensor.read();
            //Wheel Rotations and putting sensors and wheel rotations into the map
            loop {
                match motor.get_ticks_unblocking().expect("Points Channel got disconnected. Must shut down.") {
                    Some(x) => {
                        map.update_moved_from_step(&x);
                        //Only do distance sensors update if robot moved
                        let mut index = 0;
                        for sensor in &sensors {
                            if sensors[index] < 80.0 {
                                map.update_blocking((0.0, *sensor), &x);  //Middle
                            }
                            index += 1;
                        }
                    }
                    None => {break;}
                };
            }
            // 3.
            //Making current state accessible outside the thread
            {
                if let Some(mut data_locked) = state_data.try_lock() {
                    let mut index = 0;
                    for sensor in &sensors {
                        data_locked.distance[index] = *sensor;
                        index += 1;
                    }
                    data_locked.x = map.x;
                    data_locked.y = map.y;
                }
            }
            // 4.
            //Receive and execute commands
            match rx.try_recv() {
                Ok((key, x, y, a, b, c)) => {
                    match key {
                        42 => {
                            motor.stop();
                            thread::sleep(Duration::from_secs(3));
                            motor.kill();
                            thread::sleep(Duration::from_secs(2));
                            println!("STOP");
                            break;
                            /* if 42 is send end this thread */
                        }
                        0 => {
                            autonome = false;
                            motor.stop();
                        }
                        1 => {
                            motor.go_forward(20.0);
                        }
                        2 => {
                            motor.forward_left(35.5, 45.0);
                        }
                        3 => {
                            motor.forward_right(35.5, 45.0);
                        }
                        4 => {
                            motor.go_backward(20.0);
                        }
                        5 => {
                            motor.backward_left(35.5, 45.0);
                        }
                        6 => {
                            motor.backward_right(35.5, 45.0);
                        }

                        127 => {
                            autonome = true;
                        }
                        _ => {println!("Nothing");}
                    }
                }
                Err(TryRecvError::Disconnected) => { println!("ERROR");break; }
                Err(TryRecvError::Empty) => {}
            };

            // 5.
            //Own motion planner
            {
                if autonome {
                    let (gx, gy) = planner.run(&sensors, &map);
                    let (gx, gy) = map.reverse_transform(gx,gy);
                    motor.to_relative_pos(gx,gy);
                } else {
                    if let Ok(Some(x)) = motor.finish_unblocking() {
                        motor.stop();
                    }
                }
                if let Ok(Some(x)) = motor.finish_unblocking() {
                    let (gx, gy) = planner.interrupted(x);
                    let (gx, gy) = map.reverse_transform(gx,gy);
                    motor.to_relative_pos(gx,gy);
                }
                if planner.terminate() {
                    autonome = false;
                    motor.stop();
                }
            }
            //Puppet planner
            {
                //For test it is automatically on



            }
        }
    });
    (handle, tx, state_data_handler)
}




















