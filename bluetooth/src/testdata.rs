use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;
use futures::channel::mpsc;
use futures::channel::mpsc::{Receiver, Sender};
use futures::SinkExt;

pub struct Main {
    pub send: Sender<i32>,
    pub recv: Receiver<i32>,
}

impl Main {
    pub fn new() -> Self {
        let (atx, arx) = mpsc::channel(100);
        let (handler, send) = run_async(atx);
        Self {
            send: send,
            recv: arx
        }
    }
}

fn run_async(mut asend: Sender<i32>) -> (JoinHandle<()>, Sender<i32>) {
    println!("Start Demo 1 preparations ...");
    //Input
    let (tx, mut rx) = mpsc::channel(100);
    println!("Finished Demo 1 preparations.");
    //#############################################################################################
    let handle= thread::spawn(move || {
        println!("Start Demo 1 Thread ...");
        let mut index: i32 = 0;
        loop {
            thread::sleep(Duration::from_secs(1));
            index += 1;
            asend.send(index);
            match rx.try_next() {
                Ok(x) => {
                    match x {
                        Some(x) => {
                            if x == 1 {
                                index = 0;
                            }
                            if x == 2 {
                                break;
                            }
                        }
                        None => {}
                    }
                }
                Err(_) => {}
            }
        }
        println!("Start Demo 1 Loop ...");
    });
    //#############################################################################################
    (handle, tx)
}


