mod testdata;

use std::collections::BTreeMap;
use std::sync::Arc;

use std::thread;
use std::time::Duration;
use bluer::adv::Advertisement;
use bluer::gatt::local::{Application, Characteristic, CharacteristicNotify, CharacteristicNotifyMethod, CharacteristicRead, CharacteristicWrite, CharacteristicWriteMethod, Service};
use futures::FutureExt;
use uuid::{Uuid};
use crate::testdata::Main;
use tokio::{
    io::{AsyncBufReadExt, BufReader},
    sync::Mutex,
    time::sleep,
};

#[tokio::main]
async fn main() {
    println!("Start bluetooth x ");
    test().await;
    sleep(Duration::from_secs(1)).await;
    println!("Finish bluetooth x ");
}


async fn test() {
    println!("Start bluetooth 2");
    let main_uuid =   Uuid::parse_str("652b1bd061f111ed9c9d0800200c9a66").unwrap();
    let main_uuid_2 = Uuid::parse_str("652b1bd361f111ed9c9d0800200c9a66").unwrap();
    let main_uuid_3 = Uuid::parse_str("652b1bd461f111ed9c9d0800200c9a66").unwrap();
    //let main_uuid_4 = Uuid::parse_str("652b1bd361f111ed9c9d0800200c9a66").unwrap();

    let session = bluer::Session::new().await.unwrap();
    let adapter = session.default_adapter().await.unwrap();
    adapter.set_powered(true).await.unwrap();
    println!("Advertising on Bluetooth adapter {} with address {}", adapter.name(), adapter.address().await.unwrap());
    //let mut manufacturer_data = BTreeMap::new();
    //manufacturer_data.insert(MANUFACTURER_ID, vec![0x21, 0x22, 0x23, 0x24]);
    let le_advertisement = Advertisement {
        service_uuids: vec![main_uuid].into_iter().collect(),
        discoverable: Some(true),
        local_name: Some("rtcr_gatt".to_string()),
        ..Default::default()
    };
    let adv_handle = adapter.advertise(le_advertisement).await.unwrap();

    let value = Arc::new(Mutex::new(vec![0x10, 0x01, 0x01, 0x10]));
    let value_read = value.clone();
    let value_write = value.clone();
    let value_notify = value.clone();

    let rc = CharacteristicRead {
        read: true,
        fun: Box::new(move |req| {
                let value = value_read.clone();
                async move {
                    let value = value.lock().await.clone();
                    println!("Read request {:?} with value {:?}", &req, &value);
                    Ok(value)
                }.boxed()
            }
        ),
        ..Default::default()
    };

    let wc = CharacteristicWrite {
        write: true,
        write_without_response: true,
        method: CharacteristicWriteMethod::Fun(Box::new(move |new_value, req| {
            let value = value_write.clone();
            async move {
                println!("Write request {:?} with value {:?}", &req, &new_value);
                let mut value = value.lock().await;
                *value = new_value;
                Ok(())
            }
                .boxed()
        })),
        ..Default::default()
    };


    let nc = CharacteristicNotify {
        notify: true,
        method: CharacteristicNotifyMethod::Fun(Box::new(move |mut notifier| {
            let value = value_notify.clone();
            async move {
                tokio::spawn(async move {
                    println!(
                        "Notification session start with confirming={:?}",
                        notifier.confirming()
                    );
                    loop {
                        {
                            let mut value = value.lock().await;
                            println!("Notifying with value {:?}", &*value);
                            if let Err(err) = notifier.notify(value.to_vec()).await {
                                println!("Notification error: {}", &err);
                                break;
                            }
                            println!("Decrementing each element by one");
                            for v in &mut *value {
                                *v = v.saturating_sub(1);
                            }
                        }
                        sleep(Duration::from_secs(5)).await;
                    }
                    println!("Notification session stop");
                });
            }.boxed()
        })),
        ..Default::default()
    };

    let charr = Characteristic {
        uuid: main_uuid_3,
        read: Some(rc),
        write: Some(wc),
        notify: Some(nc),
        ..Default::default()
    };

    let ser = Service {
        uuid: main_uuid_2,
        primary: true,
        characteristics: vec![charr],
        ..Default::default()
    };

    let app = Application {
        services: vec![ser],
        ..Default::default()
    };
    let app_handle = adapter.serve_gatt_application(app).await.unwrap();
    sleep(Duration::from_secs(520)).await;
}











/*
"652b1bd0-61f1-11ed-9c9d-0800200c9a66"
"652b1bd1-61f1-11ed-9c9d-0800200c9a66"
"652b1bd2-61f1-11ed-9c9d-0800200c9a66"
"652b1bd3-61f1-11ed-9c9d-0800200c9a66"
"652b1bd4-61f1-11ed-9c9d-0800200c9a66"
"652b1bd5-61f1-11ed-9c9d-0800200c9a66"
"652b1bd6-61f1-11ed-9c9d-0800200c9a66"
"652b1bd7-61f1-11ed-9c9d-0800200c9a66"
"652b1bd8-61f1-11ed-9c9d-0800200c9a66"
"652b1bd9-61f1-11ed-9c9d-0800200c9a66"
"652b1bda-61f1-11ed-9c9d-0800200c9a66"
"652b1bdb-61f1-11ed-9c9d-0800200c9a66"
"652b1bdc-61f1-11ed-9c9d-0800200c9a66"
"652b1bdd-61f1-11ed-9c9d-0800200c9a66"
"652b1bde-61f1-11ed-9c9d-0800200c9a66"
"652b1bdf-61f1-11ed-9c9d-0800200c9a66"
"652b1be0-61f1-11ed-9c9d-0800200c9a66"
"652b1be1-61f1-11ed-9c9d-0800200c9a66"
"652b1be2-61f1-11ed-9c9d-0800200c9a66"
"652b1be3-61f1-11ed-9c9d-0800200c9a66"
 */
