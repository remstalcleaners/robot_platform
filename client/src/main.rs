use yew::prelude::*;
use std::fs::read_to_string;
use yew::macros::html_nested;
use web_sys::{Event, HtmlInputElement};
use std::collections::HashMap;
use yew::virtual_dom::VChild;
use wasm_bindgen_futures::spawn_local;
use gloo_file::callbacks::FileReader;
use gloo_file::File;
use reqwest::{Client, Response};
use chrono::Datelike;
use std::path::PathBuf;
use serde::{Serialize,Deserialize};
use serde::de::DeserializeOwned;
use gloo_render::{request_animation_frame, AnimationFrame};
use web_sys::{HtmlCanvasElement, WebGlRenderingContext as GL};
use wasm_bindgen::JsCast;
use yew::html::Scope;
use yew::{html, Component, Context, Html, NodeRef};


enum Msg {
    PullImg,
    Image(Vec<u8>),
    PullSensors,
    Sensors(Vec<f32>),
    Failed,
    Render(f64),
}

struct Model {
    img: Vec<Vec<u8>>,
    sensors: Vec<f32>,

    gl: Option<GL>,
    node_ref: NodeRef,
    _render_loop: Option<AnimationFrame>,
}
impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        Self {
            img: vec![],
            sensors: vec![0.0,0.0,0.0,0.0,0.0,0.0,0.0],
            gl: None,
            node_ref: Default::default(),
            _render_loop: None
        }
    }
    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::PullImg => {
                ctx.link().send_future(async move {
                    match get_img().await {
                        Ok(response) => {
                            Msg::Image(response)
                        }
                        Err(e) => {Msg::Failed}
                    }
                });
                false
            }
            Msg::Image(img) => {
                /*
                if img.len() < 10 { panic!("incomming img too short: {}", img.len()); }
                //let x = img[0] as usize;
                //let y = img[1] as usize;
                let x = 720;
                let y = 1280;
                self.img = vec![vec![0;y];x];
                if img.len() != x*y { panic!("incomming img has wrong length: L {} to x {} and y {}", img.len(), x, y); }
                if self.img.len() != x || self.img[0].len() != y {
                    println!("need to resize own array x {} y {} xs {} ys {}", x,y, self.img.len(), self.img[0].len());
                    self.img = vec![vec![0;y];x];
                }
                for i in 0..x {
                    for j in 0..y {
                        self.img[i][j] = img[j * (i+1)];
                    }
                }
                 */
                self.render_gl(img, ctx.link());
                true
            }
            Msg::Render(x) => {
                //self.render_gl(x, ctx.link());
                false
            }
            Msg::PullSensors => {
                ctx.link().send_future(async move {
                    match get_sens().await {
                        Ok(response) => {
                            Msg::Sensors(response)
                        }
                        Err(e) => {Msg::Failed}
                    }
                });
                false
            }
            Msg::Sensors(x) => {
                if x.len() == 5 {
                    self.sensors = x;
                }
                true
            }
            Msg::Failed => {true}
        }
    }
    fn view(&self, ctx: &Context<Self>) -> Html {
        let img_text = if self.img.len() > 0 {
            format!("IMG: {} {}", self.img.len(), self.img[0].len())
        }else{
            format!("IMG too short!")
        };
        let sens_text = format!("Sensors: {}", self.sensors.len());
        ;;;;
        html! {
            <div>
              <h1> {"Robostatus"} </h1>
              {img_text}
            <br/>
              {sens_text}
            <br/>
            <button class="button" onclick={ctx.link().callback(move |_| Msg::PullImg)}>
            {"Image"}
            </button>
            <br/>
            <br/>
            <h3>{"Camera"}</h3>
            <canvas ref={self.node_ref.clone()} />
            </div>
        }
    }
    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        // Once rendered, store references for the canvas and GL context. These can be used for
        // resizing the rendering area when the window or canvas element are resized, as well as
        // for making GL calls.

        let canvas = self.node_ref.cast::<HtmlCanvasElement>().unwrap();

        let gl: GL = canvas
            .get_context("webgl")
            .unwrap()
            .unwrap()
            .dyn_into()
            .unwrap();

        self.gl = Some(gl);

        // In a more complex use-case, there will be additional WebGL initialization that should be
        // done here, such as enabling or disabling depth testing, depth functions, face
        // culling etc.

        if first_render {
            // The callback to request animation frame is passed a time value which can be used for
            // rendering motion independent of the framerate which may vary.
            let handle = {
                let link = ctx.link().clone();
                request_animation_frame(move |time| link.send_message(Msg::Render(time)))
            };

            // A reference to the handle must be stored, otherwise it is dropped and the render won't
            // occur.
            self._render_loop = Some(handle);
        }
    }
}
fn main() {
    yew::start_app::<Model>();
}

async fn get_img() -> reqwest::Result<Vec<u8>> {
    let client = reqwest::Client::new();
    let res = client.get("http://192.168.178.45:8081/robot/img")
        .send()
        .await?;
    let res = res.bytes().await?;
    reqwest::Result::Ok(res.to_vec())
}
async fn get_sens() -> reqwest::Result<Vec<f32>> {
    let client = reqwest::Client::new();
    let res = client.get("http://192.168.178.45:8081/robot/sensor")
        .send()
        .await?;
    let res: Vec<f32> = res.json().await?;
    reqwest::Result::Ok(res)
}



impl Model {
    fn render_gl(&mut self, img: Vec<u8>, link: &Scope<Self>) {
        let gl = self.gl.as_ref().expect("GL Context not initialized!");
        //gl.draw_arrays(img, 0, 6);

        /*
        let vert_code = include_str!("./basic.vert");
        let frag_code = include_str!("./basic.frag");

        // This list of vertices will draw two triangles to cover the entire canvas.
        let vertices: Vec<f32> = vec![
            -1.0, -1.0, 1.0, -1.0, -1.0, 1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0,
        ];
        let vertex_buffer = gl.create_buffer().unwrap();
        let verts = js_sys::Float32Array::from(vertices.as_slice());

        gl.bind_buffer(GL::ARRAY_BUFFER, Some(&vertex_buffer));
        gl.buffer_data_with_array_buffer_view(GL::ARRAY_BUFFER, &verts, GL::STATIC_DRAW);

        let vert_shader = gl.create_shader(GL::VERTEX_SHADER).unwrap();
        gl.shader_source(&vert_shader, vert_code);
        gl.compile_shader(&vert_shader);

        let frag_shader = gl.create_shader(GL::FRAGMENT_SHADER).unwrap();
        gl.shader_source(&frag_shader, frag_code);
        gl.compile_shader(&frag_shader);

        let shader_program = gl.create_program().unwrap();
        gl.attach_shader(&shader_program, &vert_shader);
        gl.attach_shader(&shader_program, &frag_shader);
        gl.link_program(&shader_program);

        gl.use_program(Some(&shader_program));

        // Attach the position vector as an attribute for the GL context.
        let position = gl.get_attrib_location(&shader_program, "a_position") as u32;
        gl.vertex_attrib_pointer_with_i32(position, 2, GL::FLOAT, false, 0, 0);
        gl.enable_vertex_attrib_array(position);

        // Attach the time as a uniform for the GL context.
        let time = gl.get_uniform_location(&shader_program, "u_time");
        gl.uniform1f(time.as_ref(), timestamp as f32);

        gl.draw_arrays(GL::TRIANGLES, 0, 6);

                let handle = {
            let link = link.clone();
            request_animation_frame(move |time| link.send_message(Msg::Render(time)))
        };
        // A reference to the new handle must be retained for the next render to run.
        self._render_loop = Some(handle);
         */




    }
}







