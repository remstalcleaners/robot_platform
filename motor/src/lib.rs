use std::thread;
use std::time::{Duration, Instant};
use sensorlib::imu::{diff, IMU, test};
use crate::i2c_motor::MotorI2C;
use crate::map::{Map};
use crate::pwm_motor::PwmMotor;
use crate::motor::Motor;
use crate::motor_service::{MotorSystem, SCALE};
use crate::servo::Servo;
use bno055::mint::{Quaternion, Vector3};
use sensorlib::super_sonic;
use sensorlib::super_sonic::SuperSonic;

mod pwm_motor;
mod i2c_motor;
mod motor;
mod wheel_encoder;
mod servo;

pub mod motor_service;
pub mod map;

//The main function for this system
pub fn libmain() {
    println!("Test");
    simple_test();
    /*
    let mut motor = MotorSystem::new();
    motor.stop();
    //thread::sleep(Duration::from_secs(60));
    println!("Start angle");
    println!("Start loop main");

    for i in 0..1000 {
        thread::sleep(Duration::from_secs(1));
        let p = motor.get_ticks_blocking().unwrap();
        println!("{:?}", p);
    }
    motor.stop();
    println!("Stop angle");
    //test_ai();
     */



    /*
    let mut motor = MotorSystem::new();
    motor.stop();
    thread::sleep(Duration::from_secs(60));
    let mut index = 31;
    let mut maxima = 0;
    println!("Start angle");

    let mut map = Map::new(Quaternion{ v: Vector3 { x: 0.0, y: 0.0, z: 0.0 }, s: 0.0 });
    println!("Start loop main");
    loop {
        if maxima > 1000 {index = 100;println!("Maxima time out stop");}
        thread::sleep(Duration::from_millis(40));
        match motor.get_ticks_unblocking().unwrap() {
            Some(p) => {
                map.update_moved_from_step(&p);
            }
            None => {}
        }
        match index {
            31 => {
                println!("Start to relative pos");
                motor.to_relative_pos(-100.0,50.0,(0.0,1.0));
                index = 32;
            }
            32 => {
                maxima += 1;
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 100;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            22 => {
                motor.go_forward(2000);
                index = 23;
            }
            23 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 100;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            0 => {
                motor.forward_left(1200, 40.0);
                index = 1;
            }
            1 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 2;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            2 => {
                motor.forward_right(3800, 40.0);
                index = 3;
            }
            3 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 4;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            4 => {
                motor.go_forward(1300);
                index = 5;
            }
            5 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 100;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            100 => {
                println!("Terminate test...");
                motor.stop();
                map.paint();
                thread::sleep(Duration::from_secs(1));
                motor.stop();
                thread::sleep(Duration::from_secs(1));
                motor.kill();
                thread::sleep(Duration::from_secs(30));
                println!("Finished terminating test...");
                break;
            }
            _=> {
                println!("Terminate test...");
                motor.stop();
                map.paint();
                thread::sleep(Duration::from_secs(1));
                motor.stop();
                thread::sleep(Duration::from_secs(1));
                motor.kill();
                thread::sleep(Duration::from_secs(30));
                println!("Finished terminating test...");
                break;
            }
        }
    }


     */

/*
    motor.done.recv();
    motor.stop();

    motor.forward_right(1200, 40.0);
    motor.done.recv();
    motor.stop();

    motor.forward_right(1300, 40.0);
    motor.done.recv();
    motor.stop();

    motor.forward_right(1300, 40.0);
    motor.done.recv();
    motor.stop();

    motor.go_forward(1300);
    motor.done.recv();
    motor.stop();

    motor.stop();
    thread::sleep(Duration::from_secs(10));
    motor.kill();
    thread::sleep(Duration::from_secs(60));


 */

    /*
    let mut motor = MotorSystem::new();
    motor.stop();
    motor.stop();
    motor.stop();
    thread::sleep(Duration::from_secs(2));
    motor.stop();
    thread::sleep(Duration::from_secs(2));
    motor.kill();

     */
    /*
    let servo = Servo::new();
    loop {
        let mut line = String::new();
        let b1 = std::io::stdin().read_line(&mut line).unwrap();
        let x: u64 = match line.parse() {
            Ok(x)=>x,
            Err(_)=> {
                servo.change(0);
                break;
            }
        };
        servo.change(x);
    }
     */

    //test3();
    /*
    let mut motor = MotorSystem::new();
    motor.stop();
    thread::sleep(Duration::from_secs(30));
    motor.forward_left(1000, 40.0);


    motor.done.recv();
    motor.stop();

    motor.forward_right(1200, 40.0);
    motor.done.recv();
    motor.stop();

    motor.forward_right(1300, 40.0);
    motor.done.recv();
    motor.stop();

    motor.forward_right(1300, 40.0);
    motor.done.recv();
    motor.stop();

    motor.go_forward(1300);
    motor.done.recv();
    motor.stop();

    motor.stop();
    thread::sleep(Duration::from_secs(10));
    motor.kill();
    thread::sleep(Duration::from_secs(60));



     */

    //let mut imu = IMU::new(false);
    //let mut motor = MotorInternal::new_i2c(false, true);
    //motor.change(1.0,1.0);
    //thread::sleep(Duration::from_secs(5));
    //motor.change(0.0,0.0);
    //motor.turn(&mut imu, 90.0, true);
    /*
    let mut motor = MotorSystem::new();
    thread::sleep(Duration::from_millis(2000));
    motor.stop();
    thread::sleep(Duration::from_millis(2000));
    motor.go_forward();
    thread::sleep(Duration::from_millis(2000));
    motor.stop();
    thread::sleep(Duration::from_millis(2000));
    motor.left_turn();
    thread::sleep(Duration::from_millis(10000));
    motor.stop();
    thread::sleep(Duration::from_millis(2000));
    motor.go_forward();
    thread::sleep(Duration::from_millis(2000));
    motor.stop();
    thread::sleep(Duration::from_millis(2000));
    motor.turn_right();
    thread::sleep(Duration::from_millis(10000));
    motor.stop();
    thread::sleep(Duration::from_millis(2000));
    motor.go_backward();
    thread::sleep(Duration::from_millis(2000));
    motor.stop();
    thread::sleep(Duration::from_millis(2000));
    motor.stop();
    thread::sleep(Duration::from_millis(5000));
    motor.stop();

    /*
    motor.go_forward();
    thread::sleep(Duration::from_millis(2000));
    motor.stop();
    motor.go_backward();
    thread::sleep(Duration::from_millis(2500));
    motor.stop();
    motor.go_forward();
    */

    thread::sleep(Duration::from_millis(2500));
    motor.kill();
    thread::sleep(Duration::from_millis(10000));
    //let mut motor = Motor::new_i2c(false, false);
    //motor.change(0.0,0.0);

     */
/*
    let mut motor = MotorSystem::new();
    thread::sleep(Duration::from_millis(2000));
    motor.stop();
    thread::sleep(Duration::from_millis(2000));
    for i in 0..10 {
        motor.turn_right();
        thread::sleep(Duration::from_millis(5000));
        motor.stop();
        thread::sleep(Duration::from_millis(1000));
    }
    thread::sleep(Duration::from_millis(2000));
    motor.stop();
    thread::sleep(Duration::from_millis(10000));
    motor.kill();
    thread::sleep(Duration::from_millis(20000));

 */
    /*
    let mut imu = IMU::new(true);
    let test = imu.read_gyro_q().unwrap();
    thread::sleep(Duration::from_millis(2000));
    let start = imu.read_gyro_q().unwrap();;
    let mut motor = Motor::new_i2c(true, false);
    thread::sleep(Duration::from_millis(1000));
    motor.change(-1.0,1.0);
    for i in 0..10000 {
        thread::sleep(Duration::from_millis(10));
        let angle = imu.read_gyro_q().unwrap();
        let diff = diff(&start, &angle);
        //println!("{} : {} {:?}",diff, angle.s, angle.v);
        println!("{}",diff);
    }
    for i in 0..1000 {
        thread::sleep(Duration::from_millis(10));
        let angle = imu.read_gyro_q().unwrap();
        let diff = diff(&start, &angle);
        //println!("{} : {} {:?}",diff, angle.s, angle.v);
        println!("{}",diff);
        if diff < 0.01 {
            println!("FIN");
            break;
        }
    }

    motor.change(0.0,0.0);
    motor.change(0.0,0.0);
    thread::sleep(Duration::from_millis(2000));
    motor.change(0.0,0.0);
    motor.change(0.0,0.0);
    thread::sleep(Duration::from_millis(2000));
    motor.change(0.0,0.0);
    motor.change(0.0,0.0);
    */
    println!("Fin");
}

fn simple_test() {
    println!("Simple Test");
    let mut motor = MotorSystem::new(true);
    motor.stop();
    thread::sleep(Duration::from_secs(60));
    let mut index = 0;
    thread::sleep(Duration::from_secs(120));
    motor.clean_ticks();
    //Sensors
    let mut sensor = SuperSonic::new(
        vec![15, 7, 25, 23],
        vec![14, 8, 24, 18],
        vec![
            "Front Left".to_string(),
            "Front Middle".to_string(),
            "Front Right".to_string(),
            "Backside".to_string()]
    );
    let mut map = Map::new("test basic", motor.get_ticks_blocking().unwrap());
    println!("Start loop main");
    loop {
        thread::sleep(Duration::from_millis(40));
        let sensors = sensor.read();
        match motor.get_ticks_unblocking().unwrap() {
            Some(x) => {
                map.update_moved_from_step(&x);
                if sensors[1] < 180.0 && sensors[1] > 2.0 {
                    map.update_blocking((sensors[1],0.0), &x);//Middle
                }
                if sensors[0] < 180.0 && sensors[0] > 2.0 {
                    map.update_blocking((sensors[0], -20.0), &x); //Left
                }
                if sensors[2] < 180.0 && sensors[2] > 2.0 {
                    map.update_blocking((sensors[2], 20.0), &x); //Right
                }
                if sensors[3] < 180.0 && sensors[3] > 2.0 {
                    map.update_blocking((-sensors[3], 0.0), &x); //Behind
                }
            }
            None => {}
        }
        match index {
            0 => {
                println!("0");
                motor.go_forward(40.0);
                index = 1;
            }
            1 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 2;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            2 => {
                println!("2");
                motor.forward_left(40.0,40.0);
                index = 3;
            }
            3 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 4;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            4 => {
                println!("4");
                motor.forward_right(120.0, 40.0);
                index = 5;
            }
            5 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 100;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            _=> {
                println!("Terminate test...");
                motor.stop();
                map.paint("SimpleMap");
                thread::sleep(Duration::from_secs(1));
                motor.stop();
                thread::sleep(Duration::from_secs(1));
                motor.kill();
                thread::sleep(Duration::from_secs(30));
                println!("Finished terminating test...");
                break;
            }
        }
    }
}


enum State {
    ExGoal,
    ToX,
    Up,
    Turn1,
    Turn2,
    Back,
}
fn test_ai() {
    println!("Test");
    let mut motor = MotorSystem::new(true);
    motor.stop();
    thread::sleep(Duration::from_secs(60));
    println!("Start angle");
    let p = motor.get_ticks_blocking().unwrap();
    let mut map = Map::new("", p);
    let mut x_to = 0.0;
    let mut state = State::ExGoal;
    let mut start = true;
    let mut over = false;
    let mut index = 0;
    let mut sensor = super_sonic::SuperSonic::new(
        vec![15, 7, 25, 23],
        vec![14, 8, 24, 18],
        vec![
            "Front Left".to_string(),
            "Front Middle".to_string(),
            "Front Right".to_string(),
            "Backside".to_string()]
    );
    println!("Start loop main");
    loop {
        thread::sleep(Duration::from_millis(40));
        let sensors = sensor.read();
        match motor.get_ticks_unblocking().unwrap() {
            Some(p) => {
                map.update_moved_from_step(&p);
                if sensors[1] < 80.0 {
                    map.update_blocking((0.0, sensors[1]), &p);
                }
            }
            None => {}
        }
        if start {
            match state {
                State::ExGoal => {
                    println!("1. Go backward out of goal");
                    motor.backward_left(1200 as f32 * SCALE, 40.0);
                }
                State::ToX => {
                    println!("Go to x position: {} {} -> {} {}", map.x, map.y, x_to - map.x, -50.0 - map.y);
                    motor.to_relative_pos(x_to - map.x, -50.0 - map.y);
                }
                State::Up => {
                    println!("Go up: {} {} -> {} {}", map.x, map.y, x_to - map.x, -500000.0 - map.y);
                    motor.to_relative_pos(x_to - map.x, -500000.0 - map.y);
                }
                State::Turn1 => {
                    println!("Go a bit to center: {} {} -> {} {}", map.x, map.y, x_to - map.x + 40.0, 0.0);
                    motor.to_relative_pos(x_to - map.y - 30.0, 0.0);
                }
                State::Turn2 => {
                    println!("Go a bit back: {} {} -> {} {}", map.x, map.y, x_to - map.x + 40.0, 10.0);
                    motor.to_relative_pos(x_to - map.x - 60.0, 30.0);
                }
                State::Back => {
                    println!("Go back to goal: {} {} -> {} {}", map.x, map.y, 0.0 - map.x, 0.0 - map.y);
                    motor.to_relative_pos(0.0 - map.x, 0.0 - map.y);
                }
            }
            start = false;
        } else {
            match state {
                State::ExGoal => {}
                State::ToX => {
                    if sensors[1] < 50.0 {
                        state = State::Up;
                        start = true;
                        println!("Set over because side wall detected.");
                        over = true;
                    }
                }
                State::Up => {
                    if sensors[1] < 70.0 {
                        state = State::Turn1;
                        start = true;
                    }
                }
                State::Turn1 => {}
                State::Turn2 => {}
                State::Back => {}
            }
            match motor.finish_unblocking() {
                Ok(x) => {
                    match x {
                        None => {}
                        Some(x) => {
                            match state {
                                State::ExGoal => {state=State::ToX; start = true;}
                                State::ToX => {state=State::Up; start = true;}
                                State::Up => {state=State::Turn1; start = true;}
                                State::Turn1 => {state=State::Turn2; start = true;}
                                State::Turn2 => {state=State::Back; start = true;}
                                State::Back => {
                                    state=State::ExGoal;
                                    start = true;
                                    x_to += 40.0;
                                    motor.stop();
                                    map.paint(&format!("AR{}", index));
                                    index += 1;
                                    println!("GP: {}: {} ({})", index, x_to, over);
                                    if over {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                Err(_) => {}
            };
        }
    }
}






/*
let mut motor = Motor::new_i2c(true, false);
        self.change(1.0,-1.0);
        counter clock wise
=> let mut motor = Motor::new_i2c(true, false); is straigt backwards

*/

//##################################################################################################
//##################################################################################################
//##################################################################################################
/*
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_pwm_motor() {
        let mut motor = PwmMotor::new(0,0);
        motor.change(1.0,1.0);
        thread::sleep(Duration::from_secs(2));
    }
    #[test]
    fn test_i2c_motor() {
        let mut motor = MotorI2C::new(false,true);
        let mut test_pin_1 = TestPin::new(vec![10,9,11,29]);
        motor.change(1.0,0.2);
        for i in 0..10 {
            thread::sleep(Duration::from_secs(1));
            let r1 = test_pin_1.read();
            if r1 != 0 {println!("Something!");}
            //println!("{}  ||  {} || {} || {}", r1, r2, r3, r4);
        }
        test_pin_1.kill();
        motor.change(0.0,0.0);
    }
    #[test]
    fn test_pins() {
        let mut motor = MotorI2C::new(false,false);
        let mut test_pin_1 = TestPin::new(vec![10,9,11,29]);
        motor.change(1.0,0.2);
        for i in 0..10 {
            thread::sleep(Duration::from_secs(1));
            let r1 = test_pin_1.read();
            if r1 != 0 {println!("Something!");}
            //println!("{}  ||  {} || {} || {}", r1, r2, r3, r4);
        }
        test_pin_1.kill();
        motor.change(0.0,0.0);
    }

}

 */


/*

Point { time: 5, angle: 0.0, quart: Quaternion { v: Vector3 { x: 0.99884033, y: 0.009033203, z: 0.047302246 }, s: 0.0 }, left: 0, right: 0, state: Rest }
Point { time: 12695, angle: 0.11222975, quart: Quaternion { v: Vector3 { x: 0.9972534, y: 0.0018310547, z: 0.04888916 }, s: 0.055786133 }, left: 105, right: 37, state: ForwardLeft(1000, 40.0) }
Point { time: 13000, angle: 0.21652733, quart: Quaternion { v: Vector3 { x: 0.99298096, y: 0.0006713867, z: 0.048828125 }, s: 0.107543945 }, left: 111, right: 39, state: ForwardLeft(1000, 40.0) }
Point { time: 13306, angle: 0.31486842, quart: Quaternion { v: Vector3 { x: 0.9864502, y: -0.00036621094, z: 0.04925537 }, s: 0.15655518 }, left: 110, right: 40, state: ForwardLeft(1000, 40.0) }
Point { time: 13612, angle: 0.4196526, quart: Quaternion { v: Vector3 { x: 0.9768677, y: -0.0029296875, z: 0.049865723 }, s: 0.2078247 }, left: 112, right: 39, state: ForwardLeft(1000, 40.0) }
Point { time: 13920, angle: 0.5200295, quart: Quaternion { v: Vector3 { x: 0.96520996, y: -0.004760742, z: 0.049438477 }, s: 0.25665283 }, left: 111, right: 40, state: ForwardLeft(1000, 40.0) }
Point { time: 14226, angle: 0.6220732, quart: Quaternion { v: Vector3 { x: 0.9508667, y: -0.0059814453, z: 0.04876709 }, s: 0.3057251 }, left: 111, right: 40, state: ForwardLeft(1000, 40.0) }
Point { time: 14532, angle: 0.7241139, quart: Quaternion { v: Vector3 { x: 0.934021, y: -0.008239746, z: 0.04876709 }, s: 0.3538208 }, left: 111, right: 39, state: ForwardLeft(1000, 40.0) }
Point { time: 14837, angle: 0.82311636, quart: Quaternion { v: Vector3 { x: 0.9154053, y: -0.012023926, z: 0.047851563 }, s: 0.3994751 }, left: 110, right: 40, state: ForwardLeft(1000, 40.0) }
Point { time: 15142, angle: 0.9236177, quart: Quaternion { v: Vector3 { x: 0.89416504, y: -0.013977051, z: 0.04748535 }, s: 0.44500732 }, left: 112, right: 39, state: ForwardLeft(1000, 40.0) }
Point { time: 15447, angle: 1.0203137, quart: Quaternion { v: Vector3 { x: 0.87158203, y: -0.014953613, z: 0.047180176 }, s: 0.48773193 }, left: 111, right: 40, state: ForwardLeft(1000, 40.0) }
Point { time: 15752, angle: 1.1204147, quart: Quaternion { v: Vector3 { x: 0.84606934, y: -0.017456055, z: 0.046813965 }, s: 0.5307617 }, left: 111, right: 40, state: ForwardLeft(1000, 40.0) }
Point { time: 16058, angle: 1.2196262, quart: Quaternion { v: Vector3 { x: 0.8187256, y: -0.020629883, z: 0.045776367 }, s: 0.5720215 }, left: 111, right: 40, state: ForwardLeft(1000, 40.0) }
Point { time: 16363, angle: 1.3211473, quart: Quaternion { v: Vector3 { x: 0.78863525, y: -0.022399902, z: 0.04486084 }, s: 0.612854 }, left: 111, right: 40, state: ForwardLeft(1000, 40.0) }
Point { time: 16671, angle: 1.4219537, quart: Quaternion { v: Vector3 { x: 0.75671387, y: -0.023620605, z: 0.044433594 }, s: 0.65179443 }, left: 111, right: 40, state: ForwardLeft(1000, 40.0) }






*/
/*
fn test_simple_loop() {
    println!("Test");

    let mut motor = MotorSystem::new();
    motor.stop();
    thread::sleep(Duration::from_secs(60));
    let mut index = 0;
    println!("Start angle");

    let mut map = Map::new(Quaternion{ v: Vector3 { x: 0.0, y: 0.0, z: 0.0 }, s: 0.0 });
    println!("Start loop main");
    loop {
        thread::sleep(Duration::from_millis(40));
        match motor.get_ticks_unblocking().unwrap() {
            Some(p) => {
                map.update_moved_from_step(&p);
            }
            None => {}
        }
        match index {
            22 => {
                motor.go_forward(2000);
                index = 23;
            }
            23 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 100;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            0 => {
                motor.forward_left(1200, 40.0);
                index = 1;
            }
            1 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 2;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            2 => {
                motor.forward_right(3800, 40.0);
                index = 3;
            }
            3 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 4;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            4 => {
                motor.go_forward(1300);
                index = 5;
            }
            5 => {
                match motor.finish_unblocking() {
                    Ok(x) => {
                        match x {
                            None => {}
                            Some(x) => {
                                motor.stop();
                                index = 100;
                            }
                        }
                    }
                    Err(_) => {}
                };
            }
            100 => {
                println!("Terminate test...");
                motor.stop();
                map.paint();
                thread::sleep(Duration::from_secs(2));
                motor.stop();
                thread::sleep(Duration::from_secs(3));
                motor.kill();
                thread::sleep(Duration::from_secs(30));
                println!("Finished terminating test...");
                break;
            }
            _=> {
                println!("Terminate test...");
                motor.stop();
                map.paint();
                thread::sleep(Duration::from_secs(2));
                motor.stop();
                thread::sleep(Duration::from_secs(3));
                motor.kill();
                thread::sleep(Duration::from_secs(30));
                println!("Finished terminating test...");
                break;
            }
        }
    }
}
 */