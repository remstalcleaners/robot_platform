use rppal::i2c::I2c;


//Source: Code is a rewrite of https://github.com/Seeed-Studio/Grove_Motor_Driver_TB6612FNG
//Key values used to communicate to the I2C
type ByteI2C = u8;
const GROVE_MOTOR_DRIVER_DEFAULT_I2C_ADDR  : ByteI2C =        0x14;

//const GROVE_MOTOR_DRIVER_DEFAULT_I2C_ADDR         0x14
//const GROVE_MOTOR_DRIVER_I2C_CMD_BRAKE            0x00
const GROVE_MOTOR_DRIVER_I2C_CMD_STOP: ByteI2C = 0x01;
const GROVE_MOTOR_DRIVER_I2C_CMD_CW: ByteI2C = 0x02;
const GROVE_MOTOR_DRIVER_I2C_CMD_CCW: ByteI2C = 0x03;
//const GROVE_MOTOR_DRIVER_I2C_CMD_STANDBY          0x04
//const GROVE_MOTOR_DRIVER_I2C_CMD_NOT_STANDBY      0x05
//const GROVE_MOTOR_DRIVER_I2C_CMD_STEPPER_RUN      0x06
//const GROVE_MOTOR_DRIVER_I2C_CMD_STEPPER_STOP     0x07
//const GROVE_MOTOR_DRIVER_I2C_CMD_STEPPER_KEEP_RUN 0x08
//const GROVE_MOTOR_DRIVER_I2C_CMD_SET_ADDR         0x11

pub struct MotorI2C {
    left_reverse: bool,
    right_reverse: bool,
}
impl MotorI2C {
    pub(crate) fn new(left_reverse: bool, right_reverse: bool) -> Self {
        Self { left_reverse, right_reverse}
    }
    pub(crate) fn change(&mut self, mut left_speed: f64, mut right_speed: f64) {
        if left_speed  > 1.0  { left_speed =  1.0; }
        if right_speed > 1.0  { right_speed=  1.0; }
        if left_speed  < -1.0 { left_speed = -1.0; }
        if right_speed < -1.0 { right_speed= -1.0; }
        dc_motor_run(MotorChannelType::MOTORCHB, (left_speed*255.0) as i32 * if self.right_reverse {-1}else{1});
        dc_motor_run(MotorChannelType::MOTORCHA, (right_speed*255.0) as i32 * if self.left_reverse {-1}else{1});
    }
}
//Changing the motor speed, low level part
enum MotorChannelType {
    MOTORCHA = 0,
    MOTORCHB = 1,
}
impl MotorChannelType {
    fn get(&self) -> ByteI2C {
        match self {
            MotorChannelType::MOTORCHA => 0,
            MotorChannelType::MOTORCHB => 1,
        }
    }
}
fn dc_motor_run(chl: MotorChannelType, speed: i32) {
    //println!("Run DC Motor... {} {}", chl.get(), speed);
    //Cut off too large numbers (in case the type will not protect it)
    let speed =
        if speed > 255 {
            255
        } else if speed < -255 {
            -255
        } else {
            speed
        };
    //Data is send to the I2C as array
    let mut buffer: [ByteI2C;2] = [0, 0];
    //Write the key value for forward rotation CW or opposite rotation CCW to data array
    let reg_addr = if speed >= 0 {
        GROVE_MOTOR_DRIVER_I2C_CMD_CW
    } else {
        GROVE_MOTOR_DRIVER_I2C_CMD_CCW
    };
    buffer[0] = chl.get();
    buffer[1] = if speed >= 0 {
        speed
    } else {
        (-speed)
    } as ByteI2C;

    write_bytes(GROVE_MOTOR_DRIVER_DEFAULT_I2C_ADDR, reg_addr, buffer);
}
fn write_bytes(devAddr: ByteI2C, regAddr: ByteI2C, data: [ByteI2C; 2]) {
    let mut i2c = I2c::new().expect("new not working");
    //println!("{:?}", i2c);
    i2c.set_slave_address(devAddr as u16).expect("set slave not working");
    //let res = i2c.write(&data).expect("Send not working");
    //println!(">> {}", res);
    i2c.block_write(
        regAddr,
        &data,
    ).expect("Send 2 not working");;
    //Wire.beginTransmission(devAddr);
    //Wire.send((uint8_t) regAddr); // send address
    //i2c.block_write(data[0], &[data[1]]);
}
