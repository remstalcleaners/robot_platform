use crate::motor_service::Step;


const SQUARE_SIZE: f32 = 10.0;//(10 cm)^2 = 1 square on map
const X_SIZE: usize = 100;
const Y_SIZE: usize = 100;
const GOAL_X: usize = 50;
const GOAL_Y: usize = 10;
const GOAL_X_F: f32 = GOAL_X as f32;
const GOAL_Y_F: f32 = GOAL_Y as f32;


pub struct Map {
    map: Vec<Vec<u8>>,
    pub x: f32,
    pub y: f32,
    pub basis: (f32,f32),
    pub name: String,
    points: Vec<(f32,f32)>,
    blocks: Vec<(f32,f32)>,
}
impl Map {
    pub fn new(name: &str, base_vec: Step) -> Self {
        //println!("New map {:?}", base_vec);
        let base_length = (base_vec.uvec.0 * base_vec.uvec.0 + base_vec.uvec.1 * base_vec.uvec.1).sqrt();
        let bx = base_vec.uvec.0/base_length;
        let by = base_vec.uvec.1/base_length;
        // The origin is (0,1) so robot looking up
        // cosT = (x,y) * (0,1) = x*0 + y*1 = y
        // sinT = (x,y) X (0,1) = x*1 - y*0 = x
        Self {
            map: vec![vec![0;Y_SIZE];X_SIZE],
            x: 0.0,
            y: 0.0,
            basis: (bx, by),
            name: name.to_string(),
            points: vec![(0.0, 0.0)],
            blocks: vec![],
        }
    }
    pub fn reset(&mut self, base_vec: Step) {
        let base_length = (base_vec.uvec.0 * base_vec.uvec.0 + base_vec.uvec.1 * base_vec.uvec.1).sqrt();
        let bx = base_vec.uvec.0/base_length;
        let by = base_vec.uvec.1/base_length;
        self.basis = (bx, by);
    }

    pub fn update_moved_from_step(&mut self, step: &Step) {
        let x = step.uvec.0 * step.length;
        let y = step.uvec.1 * step.length;
        let (x,y) = transform(self.basis.0, self.basis.1, x, y, false);
        //println!("Map Update ({},{}) -> {:?}", x, y, step);
        self.x += x;
        self.y += y;
        self.points.push((self.x, self.y));
    }
    pub fn update_blocking(&mut self, dvec: (f32, f32), step: &Step) {
        let x = step.uvec.0;
        let y = step.uvec.1;
        let (x, y) = transform(self.basis.0, self.basis.1, x, y, false);
        //print!("{:?} -> {:?} ", dvec, (x,y));
        let (x, y) = transform(x, y, dvec.0, dvec.1, true);
        //print!(" {:?} ", (x,y));
        let x = x + self.x;
        let y = y + self.y;
        //println!(" : {:?} ", (x,y));
        self.blocks.push((x, y));
    }
    pub fn reverse_transform(&self, x: f32, y: f32) -> (f32, f32) {
        transform(self.basis.0, self.basis.1, x, y, true)
    }

    pub fn paint(&self, name: &str) {
        plot_2d(&self.points, &self.blocks, name.to_string());
    }
}



//Plot 2d graph
fn plot_2d(path: &Vec<(f32, f32)>, blocks: &Vec<(f32, f32)>, name: String) {
    use plotters::prelude::*;
    println!("Start plotting 2D points ...");
    let mut size = 0.0;
    for (x,y) in path {
        if x.abs() > size {size = x.abs();}
        if y.abs() > size {size = y.abs();}
    }
    let path_name = &format!("{}.png", name);
    let root = BitMapBackend::new(path_name, (800, 800)).into_drawing_area();
    root.fill(&WHITE).unwrap();
    let mut chart = ChartBuilder::on(&root)
        .caption("map", ("sans-serif", 50).into_font())
        .margin(30)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(-size..size, -size..size).unwrap();
    chart.configure_mesh().draw().unwrap();
    chart.draw_series(PointSeries::of_element(
        path.clone().into_iter(),
        5,
        &BLUE,
        &|c, s, st| {
            return EmptyElement::at(c)    // We want to construct a composed element on-the-fly
                + Circle::new((0,0),s,st.filled()) // At this point, the new pixel coordinate is established
                + Text::new(format!("({}|{})", c.0 as i32, c.1 as i32), (10, 0), ("sans-serif", 10).into_font());
        },
    )).unwrap();
    chart.draw_series(PointSeries::of_element(
        blocks.clone().into_iter(),
        5,
        &RED,
        &|c, s, st| {
            return EmptyElement::at(c)    // We want to construct a composed element on-the-fly
                + Circle::new((0,0),s,st.filled()) // At this point, the new pixel coordinate is established
                + Text::new(format!("({}|{})", c.0 as i32, c.1 as i32), (10, 0), ("sans-serif", 10).into_font());
        },
    )).unwrap();
    chart.configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw().unwrap();
    root.present().unwrap();
    println!("Saved map");
}

fn transform(bx: f32, by: f32, x: f32, y:f32, reverse: bool) -> (f32, f32) {
    let r: f32 = if reverse {-1.0} else {1.0};
    // The origin is (0,1) so robot looking up
    // cosT = (x,y) * (0,1) = x*0 + y*1 = y here called by
    // sinT = (x,y) X (0,1) = x*1 - y*0 = x here called bx
    let xt =  x * bx     + y * by * r;
    let yt = -x * by * r + y * bx;
    (xt,yt)
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn transform_test() {
        //Test rotation to itself to origin position
        let bx = 1.0;
        let by = 0.0;
        let nv = transform(bx, by, bx, by, false);
        assert_eq!(nv, (1.0,0.0));
        let bx = -1.0;
        let by = 0.0;
        let nv = transform(bx, by, bx, by, false);
        assert_eq!(nv, (1.0,0.0));
        let bx = 0.0;
        let by = 1.0;
        let nv = transform(bx, by, bx, by, false);
        assert_eq!(nv, (1.0,0.0));
        let bx = 0.0;
        let by = -1.0;
        let nv = transform(bx, by, bx, by, false);
        assert_eq!(nv, (1.0,0.0));
        let bx = 0.5/(0.5*0.5+0.5*0.5 as f32).sqrt();
        let by = 0.5/(0.5*0.5+0.5*0.5 as f32).sqrt();
        let bb = (bx*bx+by*by).sqrt();
        let bx = bx/bb;
        let by = by/bb;
        let nv = transform(bx, by, bx, by, false);
        assert!((nv.0 - 1.0).abs() < 0.0001 && nv.1.abs() < 0.0001);

        //Test mirroring
        let nv = transform(0.0, 1.0, 0.0, -1.0, false);
        assert_eq!(nv, (-1.0,0.0));
        let nv = transform(1.0, 0.0, -1.0, 0.0, false);
        assert_eq!(nv, (-1.0,0.0));
        let nv = transform(0.0, -1.0, 0.0, 1.0, false);
        assert_eq!(nv, (-1.0,0.0));
        let nv = transform(-1.0, 0.0, 1.0, 0.0, false);
        assert_eq!(nv, (-1.0,0.0));


        //Test no rotation consistency
        for i in 0..1000 {
            let i = i as f32;
            let nv = transform(1.0, 0.0, i/1000.0, i/1000.0, false);
            assert_eq!(nv, (i/1000.0, i/1000.0));
        }
        for i in 0..1000 {
            let i = i as f32;
            let nv = transform(1.0, 0.0, i/1000.0, -i/1000.0, false);
            assert_eq!(nv, (i/1000.0, -i/1000.0));
        }
        for i in 0..1000 {
            let i = i as f32;
            let nv = transform(1.0, 0.0, -i/1000.0, -i/1000.0, false);
            assert_eq!(nv, (-i/1000.0, -i/1000.0));
        }
        for i in 0..1000 {
            let i = i as f32;
            let nv = transform(1.0, 0.0, -i/1000.0, i/1000.0, false);
            assert_eq!(nv, (-i/1000.0, i/1000.0));
        }
        let nv = transform(0.0, 1.0, 1.0, 0.0, false);
        assert_eq!(nv, (0.0,-1.0));
        let nv = transform(0.0, 1.0, -1.0, 0.0, false);
        assert_eq!(nv, (0.0,1.0));
        let nv = transform(0.0, -1.0, 1.0, 0.0, false);
        assert_eq!(nv, (0.0,1.0));
        let nv = transform(0.0, -1.0, -1.0, 0.0, false);
        assert_eq!(nv, (0.0,-1.0));
        let nv = transform(0.0, 1.0, 1.0, 1.0, false);
        assert_eq!(nv, (1.0,-1.0));
        let nv = transform(0.0, -1.0, 1.0, 1.0, false);
        assert_eq!(nv, (-1.0,1.0));

        for i in 0..100 {
            let i = i as f32;
            for j in 0..100 {
                let j = j as f32;
                let nv = transform(1.0, 0.0, i/100.0, j/100.0, false);
                let rnv = transform(1.0, 0.0, nv.0, nv.1, true);
                assert!((rnv.0 - i/100.0).abs() < 0.0001);
                assert!((rnv.1 - j/100.0).abs() < 0.0001);
            }
        }

    }


    #[test]
    fn block_test() {
        let mut map = Map::new("testtest", Step {
            uvec: (1.0, 0.0),
            length: 0.0,
            time: 0
        });
        map.update_blocking((100.0, 20.0), &Step {
            uvec: (1.0, 0.0),
            length: 0.0,
            time: 0
        });
        map.update_blocking((100.0, 20.0), &Step {
            uvec: (0.0, 1.0),
            length: 0.0,
            time: 0
        });
        map.update_blocking((100.0, 0.0), &Step {
            uvec: (1.0, 1.0),
            length: 0.0,
            time: 0
        });
        map.update_blocking((100.0, 0.0), &Step {
            uvec: (1.0, -1.0),
            length: 0.0,
            time: 0
        });


        assert!((map.blocks[0].0 - 100.0).abs() < 0.001 );
        assert!((map.blocks[0].1 - 20.0).abs() < 0.001);

        assert!((map.blocks[1].0 + 20.0).abs() < 0.001 );
        assert!((map.blocks[1].1 - 100.0).abs() < 0.001);

        assert!((map.blocks[2].0 - 100.0).abs() < 0.001 );
        assert!((map.blocks[2].1 - 100.0).abs() < 0.001);

        assert!((map.blocks[3].0 - 100.0).abs() < 0.001 );
        assert!((map.blocks[3].1 + 100.0).abs() < 0.001);


    }


}







/*
pub fn get_free_x(&self, right_side: bool) -> Option<f32> {
    /* Go to X */
    let mut selected_x = GOAL_X;
    if right_side {
        for x in GOAL_X..X_SIZE {
            let mut sum = 0;
            for y in GOAL_Y..Y_SIZE-1 {
                let v = self.map[x][y];
                sum += if v < 100 {v}else{0};
            }
            if sum < 2 {
                selected_x = x;
                break;
            }
        }
    } else {
        for x in (0..GOAL_X).rev() {
            let mut sum = 0;
            for y in GOAL_Y..Y_SIZE-1 {
                sum += self.map[x][y];
            }
            if sum < 2 {
                selected_x = x;
                break;
            }
        }
    }
    return Some((selected_x as f32 - GOAL_X_F) *  SQUARE_SIZE);
}
pub fn get_map_1d(&self) -> Vec<u8> {
    let data: Vec<u8> = self.map
        .iter()
        .flat_map(|array| array.iter())
        .cloned()
        .collect();
    data
}


 */

/*
pub fn update_moved_from_ticks(&mut self, point: &Point) {
    let length = (point.left + point.right) as f32 / 2.0;
    let r = rotate(&point.quart);
    let r = (
        r.0 * length,
        r.1 * length,
        r.2 * length
    );
    //Scale from ticks to cm
    self.x += r.0 * SCALE;
    self.y += r.1 * SCALE;
    self.z += r.2 * SCALE;
    /*
    let mx = (self.y / SQUARE_SIZE + GOAL_X_F) as usize;
    let my = (self.z / SQUARE_SIZE + GOAL_Y_F) as usize;
    if self.map[mx][my] <= 10 {
        self.map[mx][my] += 1;
    }
     */
    self.points.push((self.y,self.z));
}
 */

/*

//Turn quaternion into an unit direction vector
fn rotate(angle: &Quaternion<f32>) -> (f32,f32,f32) {
    //P' = RPR'
    let a = hamilton(
        angle.s, -angle.v.x,-angle.v.y,-angle.v.z,
        0.0,0.0,0.0,-1.0
    );
    let b = hamilton(
        a.0,a.1,a.2,a.3,
        angle.s, angle.v.x,angle.v.y,angle.v.z,
    );
    (b.1, b.2, b.3)
}
//Hamilton product
fn hamilton(a1: f32, b1: f32, c1: f32, d1: f32, a2: f32, b2: f32, c2: f32, d2: f32) -> (f32,f32,f32,f32) {
    let s = a1*a2 - b1*b2 - c1*c2 - d1*d2;
    let x = a1*b2 + b1*a2 + c1*d2 - d1*c2;
    let y = a1*c2 - b1*d2 + c1*a2 + d1*b2;
    let z = a1*d2 + b1*c2 - c1*b2 + d1*a2;
    (s,x,y,z)
}



 */



/*
fn relative_angle(x0: f32, y0: f32, xt: f32, yt: f32) -> f32 {
    let a = xt*y0-yt*x0;
    let b = yt*y0-xt*x0;
    let tan_angle = (a/b).atan();
    if tan_angle.is_nan() {
        return 0.0;
    }
    if x0*xt + y0*yt >= 0.0 {
        //The vectors point approximately in the same direction, tan_angle is true
        tan_angle
    } else {
        ////The vectors point approximately in the opposite direction, tan_angle is too small
        tan_angle.signum()*(2.0* std::f32::consts::PI - tan_angle.abs())
    }
}
 */




/*
//Take relative translation vector (xt,yt) and current absolute angle of the robot and return motor instruction for motor.change(left, right)
pub fn run_towards(xt: f32, yt: f32, a0: &Quaternion<f32>) -> (f64, f64) {
    let (_,ry,rz) = rotate(a0);
    let a = relative_angle(ry, rz, xt, yt);
    let r = if a.abs() > std::f32::consts::FRAC_PI_4 {
        45.0
    }else if a.abs() < 0.1 {
        1000000000.0
    } else {
        45.0*std::f32::consts::FRAC_PI_4/a
    };
    let inside = (r-SIZE/2.0)/(r+SIZE/2.0);
    if a > 0.0 {
        (inside as f64, 1.0)
    } else {
        (1.0, inside as f64)
    }
}


 */

/* helper function for run_towards
    vt X R v0 = (xt,yt) X (x0*cosT-y0*sinT, x0*sinT+y0*cosT) = 0
    (xt,yt) X (x0*cosT-y0*sinT, x0*sinT+y0*cosT)
    xt*x0*sinT+xt*y0*cosT - yt*x0*cosT-yt*y0*sinT = 0
    (xt*y0-yt*x0)*cosT - (yt*y0-xt*x0)*sinT = 0
    (xt*y0-yt*x0) = (yt*y0-xt*x0)*sinT/cosT  | sinT/cosT = tanT
    (xt*y0-yt*x0)/(yt*y0-xt*x0) = tanT
    T = atan((xt*y0-yt*x0)/(yt*y0-xt*x0))
*/