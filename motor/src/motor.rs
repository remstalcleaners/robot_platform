use std::thread;
use std::time::{Duration, Instant};
use crate::{MotorI2C, PwmMotor};
use sensorlib::imu::IMU;

use std::sync::{Arc, mpsc};
use std::sync::mpsc::{TryRecvError, Sender};
use futures::lock::Mutex;
use std::thread::JoinHandle;


pub enum Motor {
    I2C(MotorI2C),
    PWM(PwmMotor),
}
impl Motor {
    pub fn new_i2c(left_reverse: bool, right_reverse: bool) -> Self {
        Self::I2C(MotorI2C::new(left_reverse, right_reverse))
    }
    pub fn new_pwm(left_direction_pin: u8, right_direction_pin: u8) -> Self {
        Self::PWM(PwmMotor::new(left_direction_pin, right_direction_pin))
    }
    pub fn change(&mut self, mut left_speed: f64, mut right_speed: f64) {
        match self {
            Self::I2C(i2c_motor) => {i2c_motor.change(left_speed, right_speed)}
            Self::PWM(pwm_motor) => {pwm_motor.change(left_speed, right_speed)}
        };
    }
    /*
    fn straight(&mut self, speed: f32, time: f32, reverse: bool) {
        self.change(speed as f64,speed as f64);
        thread::sleep(Duration::from_secs((time/1000.0) as u64));
        self.change(0.0,0.0);
    }
    fn turn(&mut self, imu: &mut IMU, angle: f32, reverse: bool) {
        let (g1, g2, a) = imu.read().unwrap();
        println!("<< {:.2} {:.2} {:.2}", g1, g2, a);
        thread::sleep(Duration::from_secs(1));
        let (g1, g2, a) = imu.read().unwrap();
        println!("<< {:.2} {:.2} {:.2}", g1, g2, a);
        self.change(1.0,-1.0);
        let (g1, g2, a) = imu.read().unwrap();
        println!("<< {:.2} {:.2} {:.2}", g1, g2, a);
        for i in 0..100 {
            thread::sleep(Duration::from_millis(50));
            let (g1, g2, na) = imu.read().unwrap();
            println!(">> {:.2} {:.2} {:.2}", g1, g2, na);
        }
        self.change(0.0,0.0);
    }
     */
}
