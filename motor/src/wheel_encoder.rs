use std::ops::{Deref};
use std::thread;
use std::time::{Duration, Instant};
use std::sync::{Arc, mpsc, Mutex};
use std::sync::mpsc::{TryRecvError, Sender, Receiver, RecvTimeoutError};
use std::thread::JoinHandle;
use nalgebra::Quaternion;
use rppal::gpio::{Gpio, InputPin, Level, Trigger};


pub struct WheelEncoder {
    handle: JoinHandle<()>,
    sender: Sender<i32>,
    getter: Receiver<(u128,u32,u32)>,
}
impl WheelEncoder {
    pub fn new() -> Self {
        let (tx_ex, rx_ex) = mpsc::channel();
        let (handle, sender) = run_async(tx_ex);
        Self { handle, sender, getter: rx_ex }
    }
    pub fn pop_path(&self) -> Option<(u128, u32, u32)> {
        let res = self.getter.try_recv();
        match res {
            Ok(x) => {Some(x)}
            Err(e) => {None}
        }
    }
    pub fn finish(&self) {
        self.sender.send(42);
    }
}
fn run_async(tx_ex: Sender<(u128, u32, u32)>)-> (JoinHandle<()>, Sender<i32>) {
    println!("Init odo test ...");
    let (tx, rx) = mpsc::channel();
    let tx_u = tx.clone();
    println!("Create main thread ...");
    let handle= thread::spawn(move || {
        let tx_1 = tx.clone();
        let tx_2 = tx.clone();
        let mut ticks_1: u32 = 0;
        let mut ticks_2: u32 = 0;

        let mut f_pin_1 = move |x: Level| {
            tx_1.send(1);
        };
        let mut f_pin_2 = move |x: Level| {
            tx_2.send(2);
        };

        let mut pin_1 = Gpio::new().expect("Error Odo Pin 1").get(10).expect("Echo 1 Err").into_input();
        let mut pin_2 = Gpio::new().expect("Error Odo Pin 2").get(11).expect("Echo 2 Err").into_input();

        pin_1.set_async_interrupt(Trigger::RisingEdge,f_pin_1);
        pin_2.set_async_interrupt(Trigger::RisingEdge,f_pin_2);

        let mut time = Instant::now();
        let delay = Duration::from_millis(100);
        println!("Start loop");
        loop {
            match rx.recv_timeout(delay) {
                Ok(x) => {
                    match x {
                        42 => {break;}
                        1 => { ticks_1 += 1; }
                        2 => { ticks_2 += 1; }
                        _ => {}
                    }
                }
                Err(_) => {}
            }
            if time.elapsed().as_millis() > 100 {
                time = Instant::now();
                let res = (time.elapsed().as_millis(), ticks_1, ticks_2);
                tx_ex.send(res);
                ticks_1 = 0;
                ticks_2 = 0;
            }
        }
        println!("Ticks 1: {}", ticks_1);
        println!("Ticks 2: {}", ticks_2);
    });
    println!("Odo test should run now ...");
    (handle, tx_u)
}











/*
let mut pin_1 = Gpio::new().expect("Error Odo Pin 1").get(10).expect("Echo 1 Err").into_input();
let mut pin_2 = Gpio::new().expect("Error Odo Pin 2").get(9).expect("Echo 2 Err").into_input();
let mut pin_3 = Gpio::new().expect("Error Odo Pin 3").get(11).expect("Echo 3 Err").into_input();
let mut pin_4 = Gpio::new().expect("Error Odo Pin 4").get(5).expect("Echo 4 Err").into_input();
*/






