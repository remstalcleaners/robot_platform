use std::ops::Deref;
use std::thread;
use std::time::{Duration, Instant};
use std::sync::{Arc, mpsc, Mutex, TryLockResult};
use std::sync::mpsc::{TryRecvError, Sender, Receiver};
use std::thread::JoinHandle;

use sensorlib::imu::{diff, IMU};
use bno055::mint::{Quaternion, Vector3};
use crate::motor::Motor;
use crate::{MotorI2C, PwmMotor};
use crate::wheel_encoder::WheelEncoder;





/*
 The Motor System knows all about the motor and the wheels and the geometry of this system.
 It knows about the internal things, like quaternion angle in 3D.
 It knows what actions it is doing.

 It gives its changes in movement to the other systems using Steps.
 Other systems should not know about 3D angles etc.
 This is a 2D system.
 It tells other systems what it is doing and gives signals when it is finished, both block and non blocking.
*/
//The constants of the geometry
pub const SCALE: f32 = 0.048;//Scale ticks to cm
pub const SIZE: f32 = 34.0;//Width of the robot, approximated as round










/*
 The system that controls everything about the motor for the outside world.
*/
pub struct MotorSystem {
    handle: JoinHandle<()>,
    commands: Sender<(i32, u32, f32, f32)>,
    done: Receiver<i32>,
    points: Receiver<Step>
}
impl MotorSystem {
    pub fn new(real_mode: bool) -> MotorSystem {
        let (tx_done, rx_done) = mpsc::channel();
        let (tx_point, rx_point) = mpsc::channel::<Step>();
        let (handle, sender) = if real_mode {
            run_async(tx_done, tx_point)
        } else {
            dummy_mode_run_async(tx_done,tx_point)
        };

        let mut motor = Self {
            handle: handle,
            commands: sender,
            done: rx_done,
            points: rx_point,
        };
        println!("Init Stop...");
        motor.stop();
        thread::sleep(Duration::from_secs(1));
        motor
    }

    pub fn stop(&mut self) {
        println!("stop function");
        self.commands.send((1,0,0.0,0.0));
    }
    pub fn kill(self) {
        println!("kill function");
        self.commands.send((42,0,0.0,0.0));
    }

    pub fn go_forward(&mut self, distance: f32) {
        println!("forward function");
        self.commands.send((2,(distance/SCALE) as u32,0.0,0.0));
    }
    pub fn go_backward(&mut self, distance: f32) {
        println!("backward function");
        self.commands.send((3,(distance/SCALE) as u32,0.0,0.0));
    }
    pub fn forward_left(&mut self, distance: f32, radius: f32) {
        println!("forward left function");
        self.commands.send((4,(distance/SCALE) as u32,radius,0.0));
    }
    pub fn forward_right(&mut self, distance: f32, radius: f32) {
        println!("forward right function");
        self.commands.send((5,(distance/SCALE) as u32,radius,0.0));
    }
    pub fn backward_left(&mut self, distance: f32, radius: f32) {
        println!("back left function");
        self.commands.send((6,(distance/SCALE) as u32,radius,0.0));
    }
    pub fn backward_right(&mut self, distance: f32, radius: f32) {
        println!("back right function");
        self.commands.send((7,(distance/SCALE) as u32,radius,0.0));
    }
    //Relative motion to a coordinate then stop
    //arg (relative x, relative y, the original/start coordinate system)
    pub fn to_relative_pos(&self, x: f32, y: f32) {
        self.commands.send((9,0,x,y));
    }

    pub fn finish_blocking(&self) -> Result<i32, ()> {
        match self.done.recv() {
            Ok(x) => Ok(x),
            Err(_) => Err(()),
        }
    }
    pub fn finish_unblocking(&self) -> Result<Option<i32>, ()> {
        match self.done.try_recv() {
            Ok(x) => Ok(Some(x)),
            Err(TryRecvError::Empty) => Ok(None),
            Err(TryRecvError::Disconnected) => Err(()),
        }
    }
    pub fn get_ticks_blocking(&self) -> Result<Step, ()> {
        match self.points.recv() {
            Ok(x) => Ok(x),
            Err(_) => Err(()),
        }
    }
    pub fn get_ticks_unblocking(&self) -> Result<Option<Step>, ()> {
        match self.points.try_recv() {
            Ok(x) => Ok(Some(x)),
            Err(TryRecvError::Empty) => Ok(None),
            Err(TryRecvError::Disconnected) => Err(()),
        }
    }
    pub fn clean_ticks(&self) {
        loop {
            match self.points.try_recv() {
                Ok(x) => {}
                Err(TryRecvError::Empty) => break,
                Err(TryRecvError::Disconnected) => break,
            };
        }
    }
}
#[derive(Debug, Clone)]
enum State {
    Rest,
    Forward(u32),
    ForwardLeft(u32,f32),
    ForwardRight(u32,f32),
    Backward(u32),
    BackwardLeft(u32,f32),
    BackwardRight(u32,f32),
    ToPoint(f32,f32),
}

fn run_async(done: Sender<i32>, point: Sender<Step>) -> (
    JoinHandle<()>,
    Sender<(i32, u32, f32, f32)>
) {
    println!("Init motor system ...");
    let (tx, rx) = mpsc::channel();
    let mut imu = IMU::new(true);
    println!("Create motor system thread ...");
    let handle= thread::spawn(move || {
        let mut motor = Motor::new_i2c(true, false);
        motor.change(0.0,0.0);
        let odometry = WheelEncoder::new();
        loop {/*Clean the wheel encoder buffer*/
            match odometry.pop_path() {
                Some(x) => x,
                None => break,
            };
        }
        let loop_duration = Duration::from_millis(100);
        let time = Instant::now();
        let start_angle = imu.read_gyro_q().unwrap();
        let mut angle = imu.read_gyro_q().unwrap();
        let mut state = State::Rest;
        let mut start = true;
        let mut left: u32 = 0;
        let mut right: u32 = 0;
        let mut left_s = 0;
        let mut right_s = 0;
        let mut idle_index = 0;
        let mut forward = true;
        point.send(Step::from_raw_ticks(time.elapsed().as_millis(), &angle, left as i32, right as i32));
        println!("Full Start Angle: {} {} {} {}", angle.s, angle.v.x, angle.v.y, angle.v.z);
        println!("Full Start Angle: {} {} {} {}", start_angle.s, start_angle.v.x, start_angle.v.y, start_angle.v.z);
        let mut go_to_x= 0.0;
        let mut go_to_y= 0.0;
        println!("Loop Start ...");
        loop {
            {/* Data collection for odometry part */
                loop {
                    let next = match odometry.pop_path() {
                        Some(x) => x,
                        None => break,
                    };
                    left += next.2;
                    right += next.1;
                }
                angle = imu.read_gyro_q().unwrap();
                if left > 100 || right > 100 {
                    match state {
                        State::Forward(_) | State::ForwardLeft(_,_) | State::ForwardRight(_,_) => {
                            forward = true;
                        }
                        State::Backward(_) | State::BackwardLeft(_,_) | State::BackwardRight(_,_) => {
                            forward= false;
                        }
                        State::ToPoint(_,_) => {
                            forward = true;
                        }
                        State::Rest => {}
                    }
                    let step = if forward {
                        Step::from_raw_ticks(time.elapsed().as_millis(), &angle, left as i32, right as i32)
                    }else{
                        Step::from_raw_ticks(time.elapsed().as_millis(), &angle, -(left as i32), -(right as i32))
                    };
                    go_to_x -= step.uvec.0 * step.length;
                    go_to_y -= step.uvec.1 * step.length;
                    point.send(step);
                    if go_to_x < -10000000.0 || go_to_x > 10000000.0 {go_to_x = 0.0;}
                    if go_to_y < -10000000.0 || go_to_y > 10000000.0 {go_to_y = 0.0;}

                    left_s += left;
                    right_s += right;

                    left = 0;
                    right = 0;
                    idle_index = 0;
                } else {
                    if idle_index > 10 {
                        point.send(Step::from_raw_ticks(time.elapsed().as_millis(), &angle, 0, 0));
                        idle_index = 0;
                    }
                    idle_index += 1;
                }
            }
            {/*Movement control part*/
                /*Receiving signals at any time*/
                match rx.try_recv() {
                    Ok((x,y,z,a)) => {
                        println!("Signal {} {}", x,y);
                        match x {
                            42 => {
                                println!("signal kill");
                                state = State::Rest;
                                start = false;
                                motor.change(0.0,0.0);
                                println!("motor stopped");
                                println!("FINISH!");
                                break;
                            }
                            1 => {
                                println!("signal stop");
                                state = State::Rest;
                                start = true;
                            }
                            2 => {
                                println!("signal forward");
                                state = State::Forward(y as u32);
                                start = true;
                            }
                            3 => {
                                println!("signal backward");
                                state = State::Backward(y);
                                start = true;
                            }
                            4 => {
                                println!("signal left forward");
                                state = State::ForwardLeft(y,z);
                                start = true;
                            }
                            5 => {
                                println!("signal right forward");
                                state = State::ForwardRight(y,z);
                                start = true;
                            }
                            6 => {
                                println!("signal left backward");
                                state = State::BackwardLeft(y,z);
                                start = true;
                            }
                            7 => {
                                println!("signal right backward");
                                state = State::BackwardRight(y,z);
                                start = true;
                            }
                            9 => {
                                println!("0 signal to point, {} {}", z, a);
                                state = State::ToPoint(z,a);
                                start = true;
                            }
                            _ => {}
                        }
                    }
                    Err(TryRecvError::Disconnected) => {break;}
                    Err(TryRecvError::Empty) => {}
                };
                /* Start funcions */
                if start {
                    match state {
                        State::Rest => {
                            motor.change(0.0,0.0);
                        }
                        State::Forward(s) => {
                            motor.change(1.0,1.0);
                        }
                        State::Backward(s) => {
                            motor.change(-1.0,-1.0);
                        }
                        State::ForwardLeft(s, r) => {
                            let inside = (r-SIZE/2.0)/(r+SIZE/2.0);
                            motor.change(inside as f64, 1.0);
                        }
                        State::ForwardRight(s, r) => {
                            let inside = (r-SIZE/2.0)/(r+SIZE/2.0);
                            motor.change(1.0, inside as f64);
                        }
                        State::BackwardLeft(s, r) => {
                            let inside = -(r-SIZE/2.0)/(r+SIZE/2.0);
                            motor.change(inside as f64, -1.0);
                        }
                        State::BackwardRight(s, r) => {
                            let inside = -(r-SIZE/2.0)/(r+SIZE/2.0);
                            motor.change(-1.0, inside as f64);
                        }
                        State::ToPoint(x,y) => {
                            println!("1 Start to point::: {}, {}", x, y);
                            go_to_x = x;
                            go_to_y = y;
                        }
                    }
                    start = false;
                    left_s = 0;
                    right_s = 0;
                }
                /* End or later while running functions */
                else {
                    match state {
                        State::Rest => {}
                        State::Forward(s) => {
                            if left_s + right_s > 2 * s {
                                state = State::Rest;
                                motor.change(0.0, 0.0);
                                start = true;
                                done.send(2);
                            }
                        }
                        State::Backward(s) => {
                            if left_s + right_s > 2 * s {
                                state = State::Rest;
                                motor.change(0.0, 0.0);
                                start = true;
                                done.send(3);
                            }
                        }
                        State::ForwardLeft(s, _) => {
                            if left_s + right_s > 2 * s {
                                state = State::Rest;
                                motor.change(0.0, 0.0);
                                start = true;
                                done.send(4);
                            }
                        }
                        State::ForwardRight(s, _) => {
                            if left_s + right_s > 2 * s {
                                state = State::Rest;
                                motor.change(0.0, 0.0);
                                start = true;
                                done.send(5);
                            }
                        }
                        State::BackwardLeft(s, _) => {
                            if left_s + right_s > 2 * s {
                                state = State::Rest;
                                motor.change(0.0, 0.0);
                                start = true;
                                done.send(6);
                            }
                        }
                        State::BackwardRight(s, _) => {
                            if left_s + right_s > 2 * s {
                                state = State::Rest;
                                motor.change(0.0, 0.0);
                                start = true;
                                done.send(7);
                            }
                        }
                        State::ToPoint(_, _) => {
                            if go_to_x.abs() < 30.0 && go_to_y.abs() < 30.0 {
                                println!("2 To point finished with: {}, {}", go_to_x, go_to_y);
                                state = State::Rest;
                                start = true;
                                done.send(9);
                                continue
                            }
                            let speeds = go_to_point(go_to_x, go_to_y, &angle);
                            motor.change(speeds.0 as f64, speeds.1 as f64);
                        }
                    }
                }
                thread::sleep(loop_duration);
            }
        }
    });
    println!("Motor System should run now ...");
    (handle, tx)
}

fn dummy_mode_run_async(done: Sender<i32>, point: Sender<Step>) -> (
    JoinHandle<()>,
    Sender<(i32, u32, f32, f32)>
) {
    println!("Init motor system ...");
    let (tx, rx) = mpsc::channel();
    println!("Create motor dummy system thread ...");
    let handle= thread::spawn(move || {
        let loop_duration = Duration::from_millis(500);
        let quarternion: Quaternion<f32> = Quaternion{ v: Vector3 {
            x: 0.1,
            y: 0.0,
            z: 0.0,
        }, s: 0.1 };
        loop {
            point.send(Step::from_raw_ticks(0, &quarternion, 1, 1));
            match rx.try_recv() {
                Ok((x,y,z,a)) => {
                    println!("Signal {} {}", x,y);
                    match x {
                        42 => {
                            println!("signal kill");
                            println!("dummy motor stopped");
                            println!("FINISH!");
                            break;
                        }
                        _ => {}
                    }
                }
                Err(TryRecvError::Disconnected) => {break;}
                Err(TryRecvError::Empty) => {}
            };
            thread::sleep(loop_duration);
        }
    });
    println!("Motor System should run now ...");
    (handle, tx)
}
impl Drop for MotorSystem {
    fn drop(&mut self) {
        println!("Drop function called on motorsystem");
        self.commands.send((42,0,0.0,0.0));
    }
}




#[derive(Debug, Clone)]
pub struct Step {
    pub uvec: (f32, f32),
    pub length: f32,
    pub time: u128,
}
impl Step {
    pub fn from_raw_ticks(time: u128, angle: &Quaternion<f32>, left: i32, right: i32) -> Self {
        let length = ((left + right) as f32 / 2.0)* SCALE;
        let r = rotate(angle);
        let r = (
            r.0,
            r.1,
            r.2
        );
        Self {
            uvec: (r.1, r.2),
            length ,
            time
        }
    }
}

//Helper functions to transform for Step
//Turn quaternion into an unit direction vector
fn rotate(angle: &Quaternion<f32>) -> (f32,f32,f32) {
    //P' = RPR'
    let a = hamilton(
        angle.s, -angle.v.x,-angle.v.y,-angle.v.z,
        0.0,0.0,0.0,-1.0
    );
    let b = hamilton(
        a.0,a.1,a.2,a.3,
        angle.s, angle.v.x,angle.v.y,angle.v.z,
    );
    (b.1, b.2, b.3)
}
//Hamilton product
fn hamilton(a1: f32, b1: f32, c1: f32, d1: f32, a2: f32, b2: f32, c2: f32, d2: f32) -> (f32,f32,f32,f32) {
    let s = a1*a2 - b1*b2 - c1*c2 - d1*d2;
    let x = a1*b2 + b1*a2 + c1*d2 - d1*c2;
    let y = a1*c2 - b1*d2 + c1*a2 + d1*b2;
    let z = a1*d2 + b1*c2 - c1*b2 + d1*a2;
    (s,x,y,z)
}
pub fn go_to_point(x: f32, y: f32, angle: &Quaternion<f32>) -> (f32, f32) {
    let (_, a, b) = rotate(angle);
    let a0 = to_angle(a,b);
    let a1 = to_angle(x,y);
    let mut da = a1-a0;
    if da > std::f32::consts::PI {
        da -= std::f32::consts::PI*2.0;
    }
    if da <= -std::f32::consts::PI {
        da += std::f32::consts::PI*2.0;
    }
    //0.7853985 == 45 degree
    let mut speed = 1.002 - da.abs();
    if speed < 0.5 {
        speed = 0.5;
    }
    if speed > 1.0 {
        speed = 1.0;
    }
    if da > 0.0 {
        return (1.0, speed);
    }
    if da < 0.0 {
        return (speed, 1.0);
    }
    (0.0, 0.0)
}
fn to_angle(x: f32, y: f32) -> f32 {
    let a = if y.abs() < 0.000001 {
        if x >= 0.0 {
            std::f32::consts::FRAC_PI_2
        }else{
            6.0*std::f32::consts::FRAC_PI_4
        }
    }
    else if y >= 0.0 {
        if x >= 0.0 {
            (x/y).atan()
        }else{
            std::f32::consts::PI*2.0 + (x/y).atan()
        }
    } else {
        std::f32::consts::PI - (-x/y).atan()
    };
    //println!("{}|{} :  {}", x,y,a);
    a
}







/*

use plotters::prelude::*;
use crate::wheel_encoder::WheelEncoder;
use linreg::{linear_regression, linear_regression_of};
use crate::map::Point;

struct RawCurve {
    parts: Vec<(u32,u32,Quaternion<f32>)>,
    state: State,
}
struct AbsCurve {
    state: State,
    start_angle: f32,
    end_angle: f32,
    end_x: f32,
    end_y: f32,
    r_x: f32,
    r_y: f32,
    points: Vec<(f32,f32)>,
}
fn to_raw_curves(points: &Vec<Point>) -> Vec<RawCurve> {
    let mut state = State::Rest;
    let mut chain = Vec::new();
    let mut curve = Vec::new();
    for p in points {
        if compare_state(&p.state, &state) {
            curve.push((p.left,p.right,p.quart));
        } else {
            chain.push(RawCurve {
                parts: curve,
                state: state.clone()
            });
            curve = Vec::new();
            curve.push((0,0,p.quart));
            state = p.state.clone();
        }
    }
    chain
}
fn to_abs_curves(c: &RawCurve, zero: &Quaternion<f32>, zero_is_positive: bool) -> Result<AbsCurve,()> {
    let mut l = Vec::with_capacity(c.parts.len()); /*length left*/
    let mut r = Vec::with_capacity(c.parts.len()); /*length right*/
    let mut a = Vec::with_capacity(c.parts.len()); /*angle*/
    let mut tot_l = 0.0;
    let mut tot_r = 0.0;

    let start_angle = c.parts[0].2.clone();
    for par in &c.parts {
        tot_l += par.0 as f32;
        tot_r += par.1 as f32;
        let a_local = diff(&par.2, &start_angle);
        l.push(tot_l);
        r.push(tot_r);
        a.push(a_local);
    }
    let c_l: (f32,f32) = match linear_regression(&l, &a){
        Ok(x) => {x}
        Err(_) => {return Err(())}
    };
    let c_r: (f32,f32) = match linear_regression(&r, &a){
        Ok(x) => {x}
        Err(_) => {return Err(())}
    };
    let a_l = c_l.0 * tot_l + c_l.1;
    let a_r = c_r.0 * tot_r + c_r.1;
    let a_0 = (c_l.1 + c_r.1)/2.0;
    let r_l = tot_l / a_l;
    let r_r = tot_r / a_r;
    let a_m = (a_l + a_r)/2.0;
    let r_m = (r_l+r_r)/2.0;

    //Get the absolute state
    let abs_delta = diff(&start_angle, &zero);
    let (new_angle_is_positive, new_angle) = match c.state {
        State::Rest => {(zero_is_positive, abs_delta)}
        State::Forward(_) => {(zero_is_positive, abs_delta)}
        State::ForwardLeft(_, _) => {
            if zero_is_positive {
                if abs_delta - a_m < 0.0 {
                    (false, a_m - abs_delta)
                }else{
                    (true, abs_delta - a_m)
                }
            } else {
                if abs_delta + a_m > std::f32::consts::PI {
                    (true, 2.0* std::f32::consts::PI - (abs_delta + a_m))
                }else{
                    (false, abs_delta + a_m)
                }
            }
        }
        State::ForwardRight(_, _) => {
            if zero_is_positive {
                if abs_delta + a_m > std::f32::consts::PI {
                    (false, 2.0* std::f32::consts::PI - (abs_delta + a_m))
                }else{
                    (true, abs_delta + a_m)
                }
            } else {
                if abs_delta - a_m < 0.0 {
                    (true, a_m - abs_delta)
                }else{
                    (false, abs_delta - a_m)
                }
            }
        }
        State::Backward(_) => {(zero_is_positive, abs_delta)}
        State::BackwardLeft(_, _) => {
            if zero_is_positive {
                if abs_delta + a_m > std::f32::consts::PI {
                    (false, 2.0* std::f32::consts::PI - (abs_delta + a_m))
                }else{
                    (true, abs_delta + a_m)
                }
            } else {
                if abs_delta - a_m < 0.0 {
                    (true, a_m - abs_delta)
                }else{
                    (false, abs_delta - a_m)
                }
            }
        }
        State::BackwardRight(_, _) => {
            if zero_is_positive {
                if abs_delta - a_m < 0.0 {
                    (false, a_m - abs_delta)
                }else{
                    (true, abs_delta - a_m)
                }
            } else {
                if abs_delta + a_m > std::f32::consts::PI {
                    (true, 2.0* std::f32::consts::PI - (abs_delta + a_m))
                }else{
                    (false, abs_delta + a_m)
                }
            }
        }
        _ => {(false,0.0)}
    };

    Ok(AbsCurve {
        state: c.state.clone(),
        start_angle: if zero_is_positive {abs_delta}else{-abs_delta},
        end_angle: if new_angle_is_positive {new_angle} else {-new_angle},
        end_x: 0.0,
        end_y: 0.0,
        r_x: 0.0,
        r_y: 0.0,
        points: vec![]
    })
}

fn to_points(points: &Vec<Point>, zero: Quaternion<f32>) -> Vec<(f32,f32)> {
    let chain = to_raw_curves(points);
    //Use chain for the rest. Now turn the chain's curves into 2D points
    let mut path = Vec::with_capacity(chain.len()+10);
    path.push((0.0, 0.0));
    let mut x_state: f32 = 0.0;
    let mut y_state: f32 = 0.0;
    let mut a_state: f32 = 0.0;
    for curve in chain {
        let sign: f32 = match curve.state {
            State::Rest => {1.0}
            State::Forward(_) => {1.0}
            State::ForwardLeft(_, _) => {1.0}
            State::ForwardRight(_, _) => {-1.0}
            State::Backward(_) => {1.0}
            State::BackwardLeft(_, _) => {1.0}
            State::BackwardRight(_, _) => {-1.0}
            State::ToPoint => {0.0}
        };
        let mut l = Vec::with_capacity(curve.parts.len()); /*length left*/
        let mut r = Vec::with_capacity(curve.parts.len()); /*length right*/
        let mut a = Vec::with_capacity(curve.parts.len()); /*angle*/
        let mut tot_l = 0.0;
        let mut tot_r = 0.0;

        let tot = curve.parts[0].2.clone();
        for par in &curve.parts {
            tot_l += par.0 as f32;
            tot_r += par.1 as f32;
            let a_local = diff(&par.2, &tot);
            println!("GG {}  {}  {}", tot_l, tot_r, a_local);
            l.push(tot_l);
            r.push(tot_r);
            a.push(a_local);
        }
        let c_l: (f32,f32) = match linear_regression(&l, &a){
            Ok(x) => {x}
            Err(_) => {continue}
        };
        let c_r: (f32,f32) = match linear_regression(&r, &a){
            Ok(x) => {x}
            Err(_) => {continue}
        };
        println!(">> {:?} {:?}", c_l, c_r);
        let a_l = c_l.0 * tot_l + c_l.1;
        let a_r = c_r.0 * tot_r + c_r.1;
        let r_l = tot_l / a_l;
        let r_r = tot_r / a_r;
        let a_m = (a_l + a_r)/2.0;
        let r_m = (r_l+r_r)/2.0;
        let x_r = -r_m * a_state.cos()*sign;
        let y_r = r_m * a_state.sin();
        println!(">> {} {}; {} {}; {} {}", a_l, a_r, r_l, r_r, tot_l, tot_r);
        path.push((x_r+x_state, y_r+y_state));
        for i in 0..11 {
            let x_c = x_r + r_m * (a_state - i as f32 * a_m/10.0).cos()*sign;
            let y_c = y_r - r_m * (a_state - i as f32 * a_m/10.0).sin();
            path.push((x_c+x_state, y_c+y_state));
        }
        x_state += x_r + r_m * (a_state -  a_m).cos();
        y_state += y_r - r_m * (a_state -  a_m).sin();
        a_state += a_m;
    }
    path
}

fn compare_state(a: &State, b: &State) -> bool {
    match a {
        State::Rest => {
            match b {
                State::Rest => true,
                _ => false,
            }
        }
        State::Forward(x) => {
            match b {
                State::Forward(y) => x == y,
                _ => false,
            }
        }
        State::ForwardLeft(x, y) => {
            match b {
                State::ForwardLeft(z, w) => x == z && xeq(*y, *w),
                _ => false,
            }
        }
        State::ForwardRight(x, y) => {
            match b {
                State::ForwardRight(z, w) => x == z && xeq(*y, *w),
                _ => false,
            }
        }
        State::Backward(x) => {
            match b {
                State::Backward(y) => return x == y,
                _ => false,
            }
        }
        State::BackwardLeft(x, y) => {
            match b {
                State::BackwardLeft(z, w) => x == z && xeq(*y, *w),
                _ => false,
            }
        }
        State::BackwardRight(x, y) => {
            match b {
                State::BackwardRight(z, w) => x == z && xeq(*y, *w),
                _ => false,
            }
        }
        State::ToPoint => {
            match b {
                State::ToPoint => true,
                _ => false
            }
        }
    }
}
//Approximatly equal
fn xeq(a: f32, b: f32) -> bool {
    a < (b + 0.0001) && b < (a + 0.0001)
}


fn rotate(angle: &Quaternion<f32>) -> (f32,f32,f32) {
    //P' = RPR'
    let a = hamilton(
        angle.s, -angle.v.x,-angle.v.y,-angle.v.z,
        0.0,0.0,0.0,-1.0
    );
    let b = hamilton(
        a.0,a.1,a.2,a.3,
        angle.s, angle.v.x,angle.v.y,angle.v.z,
    );
    (b.1, b.2, b.3)
}
fn hamilton(a1: f32, b1: f32, c1: f32, d1: f32, a2: f32, b2: f32, c2: f32, d2: f32) -> (f32,f32,f32,f32) {
    let s = a1*a2 - b1*b2 - c1*c2 - d1*d2;
    let x = a1*b2 + b1*a2 + c1*d2 - d1*c2;
    let y = a1*c2 - b1*d2 + c1*a2 + d1*b2;
    let z = a1*d2 + b1*c2 - c1*b2 + d1*a2;
    (s,x,y,z)
}

fn to_points2(points: &Vec<Point>) {
    let mut x = 0.0;
    let mut y = 0.0;
    let mut z = 0.0;
    let mut pathxy = Vec::with_capacity(points.len()+2);
    let mut pathxz = Vec::with_capacity(points.len()+2);
    let mut pathyz = Vec::with_capacity(points.len()+2);
    pathxy.push((0.0,0.0));
    pathxz.push((0.0,0.0));
    pathyz.push((0.0,0.0));
    for point in points {
        let length = (point.left + point.right) as f32 / 2.0;
        let r = rotate(&point.quart);
        println!("( {} | {} | {} )", r.0*length, r.1*length, r.2*length);
        let r = (r.0*length+x, r.1*length+y, r.2*length+z);
        x = r.0;
        y = r.1;
        z = r.2;
        pathxy.push((r.0*TICK_TO_CM,r.1*TICK_TO_CM));
        pathxz.push((r.0*TICK_TO_CM,r.2*TICK_TO_CM));
        pathyz.push((r.1*TICK_TO_CM,r.2*TICK_TO_CM));
    }
    plot_2d(&pathxy, "xy".to_string());
    plot_2d(&pathxz, "xz".to_string());
    plot_2d(&pathyz, "yz".to_string());
}





fn plot_2d(path: &Vec<(f32, f32)>, name: String) {
    println!("Start plotting 2D points ...");
    let mut size = 0.0;
    for (x,y) in path {
        if x.abs() > size {size = x.abs();}
        if y.abs() > size {size = y.abs();}
    }
    let path_name = &format!("{}.png", name);
    let root = BitMapBackend::new(path_name, (800, 800)).into_drawing_area();
    root.fill(&WHITE).unwrap();
    let mut chart = ChartBuilder::on(&root)
        .caption("map", ("sans-serif", 50).into_font())
        .margin(30)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(-size..size, -size..size).unwrap();
    chart.configure_mesh().draw().unwrap();
    chart.draw_series(PointSeries::of_element(
        path.clone().into_iter(),
        5,
        &RED,
        &|c, s, st| {
            return EmptyElement::at(c)    // We want to construct a composed element on-the-fly
                + Circle::new((0,0),s,st.filled()) // At this point, the new pixel coordinate is established
                + Text::new(format!("({}|{})", c.0 as i32, c.1 as i32), (10, 0), ("sans-serif", 10).into_font());
        },
    )).unwrap();
    chart.configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw().unwrap();
    root.present().unwrap();
    println!("Saved map");
}


//y to z is the 2d map


const X_SIZE: usize = 100;
const Y_SIZE: usize = 100;
const SQUARE_SIZE: f32 = 10.0;//(10 cm)^2 = 1 square on map
const SCALE: f32 = 1.0;//Scale ticks to cm
const GOAL_X: usize = 50;
const GOAL_Y: usize = 10;
const GOAL_X_F: f32 = GOAL_X as f32;
const GOAL_Y_F: f32 = GOAL_Y as f32;

struct Mmap {
    map: Vec<Vec<u8>>,
    x: f32,
    y: f32,
    z: f32,
    a: Quaternion<f32>,
    points: Vec<(f32,f32)>,
}
impl Mmap {
    fn new(startangle: Quaternion<f32>) -> Self {
        Self {
            map: vec![vec![0;Y_SIZE];X_SIZE],
            x: 0.0,
            y: 0.0,
            z: 0.0,
            a: startangle,
            points: vec![(0.0, 0.0)]
        }
    }
    fn update(&mut self, point: &Point) {
        let length = (point.left + point.right) as f32 / 2.0;
        let r = rotate(&point.quart);
        let r = (
            r.0 * length,
            r.1 * length,
            r.2 * length
        );
        self.x += r.0*SCALE;
        self.y += r.1*SCALE;
        self.z += r.2*SCALE;
        let mx = (self.y / SQUARE_SIZE + GOAL_X_F) as usize;
        let my = (self.z / SQUARE_SIZE + GOAL_Y_F) as usize;
        if self.map[mx][my] <= 10 {
            self.map[mx][my] += 1;
        }
        self.points.push((r.1*SCALE,r.2*SCALE));
    }
    fn get_direction(&self, state: u32) -> Option<(f32,f32)> {
        match state {
            0 | 1 => {
                /* Go to X */
                let mut selected_x = GOAL_X;
                if state == 0 {
                    for x in GOAL_X..X_SIZE {
                        let mut sum = 0;
                        for y in GOAL_Y..Y_SIZE-1 {
                            let v = self.map[x][y];
                            sum += if v < 100 {v}else{0};
                        }
                        if sum < 2 {
                            selected_x = x;
                            break;
                        }
                    }
                } else {
                    for x in (0..GOAL_X).rev() {
                        let mut sum = 0;
                        for y in GOAL_Y..Y_SIZE-1 {
                            sum += self.map[x][y];
                        }
                        if sum < 2 {
                            selected_x = x;
                            break;
                        }
                    }
                }
                return Some(((selected_x as f32 - GOAL_X_F) *  SQUARE_SIZE, self.y));
            }
            2 => {
                /*Go up*/
                return Some((self.x, self.y + 10.0));
            }
            3 => {
                /*Go back to goal*/
                return Some((0.0, 0.0));
            }
            _=> {}
        }
        return None;
    }
}












pub fn test1() {
    let zero = Quaternion { v: Vector3 { x: 0.99884033, y: 0.009033203, z: 0.047302246 }, s: 0.0 };
    let mut chain = Vec::new();
    chain.push(Point { time: 5, angle: 0.0, quart: Quaternion { v: Vector3 { x: 0.99884033, y: 0.009033203, z: 0.047302246 }, s: 0.0 }, left: 0, right: 0, state: State::Rest });
    chain.push(Point { time: 12695, angle: 0.11222975, quart: Quaternion { v: Vector3 { x: 0.9972534, y: 0.0018310547, z: 0.04888916 }, s: 0.055786133 }, left: 105, right: 37, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13000, angle: 0.21652733, quart: Quaternion { v: Vector3 { x: 0.99298096, y: 0.0006713867, z: 0.048828125 }, s: 0.107543945 }, left: 111, right: 39, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13306, angle: 0.31486842, quart: Quaternion { v: Vector3 { x: 0.9864502, y: -0.00036621094, z: 0.04925537 }, s: 0.15655518 }, left: 110, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13612, angle: 0.4196526, quart: Quaternion { v: Vector3 { x: 0.9768677, y: -0.0029296875, z: 0.049865723 }, s: 0.2078247 }, left: 112, right: 39, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13920, angle: 0.5200295, quart: Quaternion { v: Vector3 { x: 0.96520996, y: -0.004760742, z: 0.049438477 }, s: 0.25665283 }, left: 111, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14226, angle: 0.6220732, quart: Quaternion { v: Vector3 { x: 0.9508667, y: -0.0059814453, z: 0.04876709 }, s: 0.3057251 }, left: 111, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14532, angle: 0.7241139, quart: Quaternion { v: Vector3 { x: 0.934021, y: -0.008239746, z: 0.04876709 }, s: 0.3538208 }, left: 111, right: 39, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14837, angle: 0.82311636, quart: Quaternion { v: Vector3 { x: 0.9154053, y: -0.012023926, z: 0.047851563 }, s: 0.3994751 }, left: 110, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15142, angle: 0.9236177, quart: Quaternion { v: Vector3 { x: 0.89416504, y: -0.013977051, z: 0.04748535 }, s: 0.44500732 }, left: 112, right: 39, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15447, angle: 1.0203137, quart: Quaternion { v: Vector3 { x: 0.87158203, y: -0.014953613, z: 0.047180176 }, s: 0.48773193 }, left: 111, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15752, angle: 1.1204147, quart: Quaternion { v: Vector3 { x: 0.84606934, y: -0.017456055, z: 0.046813965 }, s: 0.5307617 }, left: 111, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16058, angle: 1.2196262, quart: Quaternion { v: Vector3 { x: 0.8187256, y: -0.020629883, z: 0.045776367 }, s: 0.5720215 }, left: 111, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16363, angle: 1.3211473, quart: Quaternion { v: Vector3 { x: 0.78863525, y: -0.022399902, z: 0.04486084 }, s: 0.612854 }, left: 111, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16671, angle: 1.4219537, quart: Quaternion { v: Vector3 { x: 0.75671387, y: -0.023620605, z: 0.044433594 }, s: 0.65179443 }, left: 111, right: 40, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16671, angle: 1.4219537, quart: Quaternion { v: Vector3 { x: 0.75671387, y: -0.023620605, z: 0.044433594 }, s: 0.65179443 }, left: 0, right: 0, state: State::Rest });

    //let path = to_points(&chain, zero);
    //plot_2d(&path, "test2".to_string());

    to_points2(&chain);
}
pub fn test2() {
    let mut chain = Vec::new();
    chain.push(Point { time: 9, angle: 0.018554954, quart: Quaternion { v: Vector3 { x: 0.9989624, y: 0.0014648438, z: 0.045043945 }, s: 0.0 }, left: 0, right: 0, state: State::Rest });
    chain.push(Point { time: 632, angle: 0.13157135, quart: Quaternion { v: Vector3 { x: 0.9968262, y: -0.00030517578, z: 0.045410156 }, s: 0.06542969 }, left: 44, right: 126, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 938, angle: 0.22474802, quart: Quaternion { v: Vector3 { x: 0.9926758, y: -0.00091552734, z: 0.04547119 }, s: 0.11193848 }, left: 38, right: 105, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 1245, angle: 0.31870475, quart: Quaternion { v: Vector3 { x: 0.9863281, y: -0.002319336, z: 0.045043945 }, s: 0.15856934 }, left: 39, right: 105, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 1550, angle: 0.40962183, quart: Quaternion { v: Vector3 { x: 0.9780884, y: -0.00390625, z: 0.04510498 }, s: 0.2033081 }, left: 38, right: 105, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 1854, angle: 0.5034007, quart: Quaternion { v: Vector3 { x: 0.96746826, y: -0.00390625, z: 0.04510498 }, s: 0.24884033 }, left: 39, right: 105, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 2160, angle: 0.59599465, quart: Quaternion { v: Vector3 { x: 0.954895, y: -0.0045776367, z: 0.045043945 }, s: 0.293396 }, left: 39, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 2467, angle: 0.68755287, quart: Quaternion { v: Vector3 { x: 0.9404297, y: -0.005493164, z: 0.04534912 }, s: 0.33691406 }, left: 40, right: 105, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 2773, angle: 0.7839798, quart: Quaternion { v: Vector3 { x: 0.9230957, y: -0.0072631836, z: 0.044921875 }, s: 0.38189697 }, left: 39, right: 106, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 3080, angle: 0.8791771, quart: Quaternion { v: Vector3 { x: 0.9038696, y: -0.008117676, z: 0.044555664 }, s: 0.42541504 }, left: 39, right: 106, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 3387, angle: 0.97426844, quart: Quaternion { v: Vector3 { x: 0.8826294, y: -0.010314941, z: 0.043945313 }, s: 0.46795654 }, left: 39, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 3692, angle: 1.0699294, quart: Quaternion { v: Vector3 { x: 0.8592529, y: -0.012573242, z: 0.043151855 }, s: 0.5095825 }, left: 40, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 3997, angle: 1.165753, quart: Quaternion { v: Vector3 { x: 0.8338623, y: -0.015197754, z: 0.0423584 }, s: 0.55010986 }, left: 40, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 4302, angle: 1.2598351, quart: Quaternion { v: Vector3 { x: 0.8070679, y: -0.016296387, z: 0.041503906 }, s: 0.58880615 }, left: 39, right: 106, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 4607, angle: 1.3546094, quart: Quaternion { v: Vector3 { x: 0.7783203, y: -0.018310547, z: 0.039489746 }, s: 0.6263428 }, left: 38, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 5232, angle: 1.2753534, quart: Quaternion { v: Vector3 { x: 0.80255127, y: -0.018615723, z: 0.039733887 }, s: 0.59490967 }, left: 126, right: 82, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 5537, angle: 1.1896243, quart: Quaternion { v: Vector3 { x: 0.82733154, y: -0.019348145, z: 0.040161133 }, s: 0.5598755 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 5842, angle: 1.1021452, quart: Quaternion { v: Vector3 { x: 0.8510742, y: -0.019470215, z: 0.039978027 }, s: 0.52319336 }, left: 101, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 6147, angle: 1.0164739, quart: Quaternion { v: Vector3 { x: 0.8727417, y: -0.018859863, z: 0.039855957 }, s: 0.48620605 }, left: 103, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 6452, angle: 0.92790955, quart: Quaternion { v: Vector3 { x: 0.8934326, y: -0.018798828, z: 0.040222168 }, s: 0.44696045 }, left: 102, right: 39, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 6759, angle: 0.8410847, quart: Quaternion { v: Vector3 { x: 0.91204834, y: -0.018371582, z: 0.039794922 }, s: 0.40771484 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 7067, angle: 0.75572014, quart: Quaternion { v: Vector3 { x: 0.9286499, y: -0.017333984, z: 0.039855957 }, s: 0.3684082 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 7373, angle: 0.6692597, quart: Quaternion { v: Vector3 { x: 0.9437866, y: -0.016357422, z: 0.03881836 }, s: 0.32781982 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 7679, angle: 0.57981163, quart: Quaternion { v: Vector3 { x: 0.95751953, y: -0.0138549805, z: 0.03918457 }, s: 0.28527832 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 7984, angle: 0.49435833, quart: Quaternion { v: Vector3 { x: 0.9688721, y: -0.013549805, z: 0.0390625 }, s: 0.24401855 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 8289, angle: 0.4062621, quart: Quaternion { v: Vector3 { x: 0.97869873, y: -0.011474609, z: 0.039367676 }, s: 0.20123291 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 8594, angle: 0.3176408, quart: Quaternion { v: Vector3 { x: 0.98669434, y: -0.010620117, z: 0.0390625 }, s: 0.15753174 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 8902, angle: 0.23102927, quart: Quaternion { v: Vector3 { x: 0.99261475, y: -0.00970459, z: 0.03918457 }, s: 0.11462402 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 9208, angle: 0.1446962, quart: Quaternion { v: Vector3 { x: 0.99664307, y: -0.008117676, z: 0.03967285 }, s: 0.07092285 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 9400, angle: 0.1446962, quart: Quaternion { v: Vector3 { x: 0.99664307, y: -0.008117676, z: 0.03967285 }, s: 0.07092285 }, left: 0, right: 0, state: State::Rest });

    to_points2(&chain);
}
pub fn test3() {
    let mut chain = Vec::new();
    chain.push(Point { time: 3, angle: 0.0, quart: Quaternion { v: Vector3 { x: 0.9987793, y: 0.013305664, z: 0.048034668 }, s: 0.0 }, left: 0, right: 0, state: State::Rest });
    chain.push(Point { time: 12790, angle: 0.1375246, quart: Quaternion { v: Vector3 { x: 0.9963989, y: 0.0053100586, z: 0.049621582 }, s: 0.068237305 }, left: 46, right: 129, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13095, angle: 0.23674032, quart: Quaternion { v: Vector3 { x: 0.9918213, y: 0.0012817383, z: 0.049438477 }, s: 0.11767578 }, left: 38, right: 106, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13402, angle: 0.3317219, quart: Quaternion { v: Vector3 { x: 0.9851074, y: -0.0006713867, z: 0.049560547 }, s: 0.16479492 }, left: 38, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13709, angle: 0.43101242, quart: Quaternion { v: Vector3 { x: 0.975708, y: -0.0028686523, z: 0.049743652 }, s: 0.21343994 }, left: 38, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14015, angle: 0.5280952, quart: Quaternion { v: Vector3 { x: 0.9642334, y: -0.0040893555, z: 0.048706055 }, s: 0.26049805 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14322, angle: 0.62371254, quart: Quaternion { v: Vector3 { x: 0.9506836, y: -0.0064086914, z: 0.048461914 }, s: 0.30633545 }, left: 39, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14627, angle: 0.7171134, quart: Quaternion { v: Vector3 { x: 0.93530273, y: -0.008117676, z: 0.048950195 }, s: 0.35040283 }, left: 38, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14932, angle: 0.81392455, quart: Quaternion { v: Vector3 { x: 0.9172363, y: -0.010559082, z: 0.048950195 }, s: 0.3951416 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15237, angle: 0.9066733, quart: Quaternion { v: Vector3 { x: 0.8978882, y: -0.011474609, z: 0.048950195 }, s: 0.4373169 }, left: 40, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15542, angle: 1.0032438, quart: Quaternion { v: Vector3 { x: 0.8757324, y: -0.013427734, z: 0.04827881 }, s: 0.4802246 }, left: 39, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15849, angle: 1.1007828, quart: Quaternion { v: Vector3 { x: 0.8512573, y: -0.016113281, z: 0.048217773 }, s: 0.52227783 }, left: 39, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16156, angle: 1.1955373, quart: Quaternion { v: Vector3 { x: 0.8255615, y: -0.018127441, z: 0.04748535 }, s: 0.5620117 }, left: 39, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16462, angle: 1.2933064, quart: Quaternion { v: Vector3 { x: 0.79711914, y: -0.019592285, z: 0.046203613 }, s: 0.6017456 }, left: 39, right: 106, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16769, angle: 1.389838, quart: Quaternion { v: Vector3 { x: 0.7671509, y: -0.021362305, z: 0.045288086 }, s: 0.63946533 }, left: 39, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 17393, angle: 1.3079516, quart: Quaternion { v: Vector3 { x: 0.7929077, y: -0.0234375, z: 0.042541504 }, s: 0.6074219 }, left: 114, right: 76, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 17698, angle: 1.2188503, quart: Quaternion { v: Vector3 { x: 0.81921387, y: -0.023254395, z: 0.043151855 }, s: 0.57141113 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 18005, angle: 1.1320499, quart: Quaternion { v: Vector3 { x: 0.84332275, y: -0.023010254, z: 0.04272461 }, s: 0.5352173 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 18313, angle: 1.0450459, quart: Quaternion { v: Vector3 { x: 0.8658447, y: -0.022155762, z: 0.04309082 }, s: 0.4979248 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 18618, angle: 0.957061, quart: Quaternion { v: Vector3 { x: 0.8870239, y: -0.021911621, z: 0.042114258 }, s: 0.45922852 }, left: 101, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 18923, angle: 0.86912924, quart: Quaternion { v: Vector3 { x: 0.9063721, y: -0.020324707, z: 0.04284668 }, s: 0.4197998 }, left: 103, right: 39, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 19228, angle: 0.78413254, quart: Quaternion { v: Vector3 { x: 0.9234619, y: -0.01928711, z: 0.042541504 }, s: 0.38079834 }, left: 102, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 19533, angle: 0.69685245, quart: Quaternion { v: Vector3 { x: 0.93927, y: -0.017456055, z: 0.042053223 }, s: 0.34014893 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 19840, angle: 0.6075105, quart: Quaternion { v: Vector3 { x: 0.9536133, y: -0.016967773, z: 0.04156494 }, s: 0.2977295 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 20146, angle: 0.52193594, quart: Quaternion { v: Vector3 { x: 0.96551514, y: -0.015563965, z: 0.041870117 }, s: 0.2564087 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 20452, angle: 0.432927, quart: Quaternion { v: Vector3 { x: 0.9760132, y: -0.013427734, z: 0.042053223 }, s: 0.21325684 }, left: 103, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 20760, angle: 0.3443897, quart: Quaternion { v: Vector3 { x: 0.9845581, y: -0.012634277, z: 0.042114258 }, s: 0.16949463 }, left: 103, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 21065, angle: 0.25966346, quart: Quaternion { v: Vector3 { x: 0.9909668, y: -0.011230469, z: 0.041137695 }, s: 0.1272583 }, left: 102, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 21369, angle: 0.17144002, quart: Quaternion { v: Vector3 { x: 0.9956665, y: -0.008972168, z: 0.04156494 }, s: 0.082336426 }, left: 103, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 21893, angle: 0.09140436, quart: Quaternion { v: Vector3 { x: 0.998291, y: -0.008056641, z: 0.04144287 }, s: 0.041015625 }, left: 106, right: 39, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 22199, angle: 0.040062577, quart: Quaternion { v: Vector3 { x: 0.9991455, y: -0.006164551, z: 0.04071045 }, s: -0.0032958984 }, left: 104, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 22507, angle: 0.10383829, quart: Quaternion { v: Vector3 { x: 0.9980469, y: -0.00592041, z: 0.039611816 }, s: -0.048095703 }, left: 104, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 22812, angle: 0.18661314, quart: Quaternion { v: Vector3 { x: 0.9949951, y: -0.0029296875, z: 0.039733887 }, s: -0.091552734 }, left: 104, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 23116, angle: 0.27515787, quart: Quaternion { v: Vector3 { x: 0.98986816, y: -0.0010986328, z: 0.03967285 }, s: -0.13647461 }, left: 104, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 23421, angle: 0.36185446, quart: Quaternion { v: Vector3 { x: 0.9829712, y: 0.0013427734, z: 0.03930664 }, s: -0.17944336 }, left: 105, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 23726, angle: 0.4505182, quart: Quaternion { v: Vector3 { x: 0.973999, y: 0.0028686523, z: 0.03930664 }, s: -0.22320557 }, left: 103, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 24034, angle: 0.5417667, quart: Quaternion { v: Vector3 { x: 0.96276855, y: 0.005126953, z: 0.039001465 }, s: -0.2675171 }, left: 104, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 24341, angle: 0.6282774, quart: Quaternion { v: Vector3 { x: 0.95025635, y: 0.0067749023, z: 0.0390625 }, s: -0.30895996 }, left: 104, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 24647, angle: 0.71776503, quart: Quaternion { v: Vector3 { x: 0.93548584, y: 0.008544922, z: 0.038146973 }, s: -0.3511963 }, left: 105, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 24954, angle: 0.8081055, quart: Quaternion { v: Vector3 { x: 0.91864014, y: 0.009765625, z: 0.038024902 }, s: -0.3930664 }, left: 104, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 25259, angle: 0.8955831, quart: Quaternion { v: Vector3 { x: 0.90057373, y: 0.011169434, z: 0.037109375 }, s: -0.43292236 }, left: 103, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 25564, angle: 0.9854248, quart: Quaternion { v: Vector3 { x: 0.880188, y: 0.014221191, z: 0.03643799 }, s: -0.47296143 }, left: 105, right: 40, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 25869, angle: 1.0733856, quart: Quaternion { v: Vector3 { x: 0.8585205, y: 0.015625, z: 0.035888672 }, s: -0.5112915 }, left: 104, right: 41, state: State::ForwardRight(1000, 40.0) });
    chain.push(Point { time: 26495, angle: 1.1938473, quart: Quaternion { v: Vector3 { x: 0.8262329, y: 0.017211914, z: 0.03363037 }, s: -0.56207275 }, left: 126, right: 47, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 26801, angle: 1.2860558, quart: Quaternion { v: Vector3 { x: 0.7994385, y: 0.01965332, z: 0.032470703 }, s: -0.5994873 }, left: 104, right: 40, state: State::ForwardRight(1200, 40.0) });

    chain.push(Point { time: 27106, angle: 0.0, quart: Quaternion { v: Vector3 { x: -1.2278442, y: 0.022338867, z: 0.03125 }, s: -0.63427734 }, left: 104, right: 40, state: State::ForwardRight(1200, 40.0) });

    chain.push(Point { time: 27411, angle: 1.4661345, quart: Quaternion { v: Vector3 { x: 0.74224854, y: 0.024047852, z: 0.030456543 }, s: -0.66900635 }, left: 104, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 27716, angle: 1.5549878, quart: Quaternion { v: Vector3 { x: 0.711792, y: 0.025146484, z: 0.02947998 }, s: -0.701355 }, left: 105, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 28022, angle: 1.6468709, quart: Quaternion { v: Vector3 { x: 0.678833, y: 0.02758789, z: 0.027770996 }, s: -0.73327637 }, left: 105, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 28328, angle: 1.7391492, quart: Quaternion { v: Vector3 { x: 0.6442871, y: 0.02911377, z: 0.026306152 }, s: -0.76379395 }, left: 104, right: 41, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 28636, angle: 1.8299196, quart: Quaternion { v: Vector3 { x: 0.60894775, y: 0.030822754, z: 0.025146484 }, s: -0.7922363 }, left: 105, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 28943, angle: 1.918422, quart: Quaternion { v: Vector3 { x: 0.5733032, y: 0.03173828, z: 0.023742676 }, s: -0.8184204 }, left: 105, right: 39, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 29248, angle: 2.0106006, quart: Quaternion { v: Vector3 { x: 0.53497314, y: 0.03363037, z: 0.022216797 }, s: -0.8438721 }, left: 105, right: 41, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 29553, angle: 2.1024942, quart: Quaternion { v: Vector3 { x: 0.4956665, y: 0.03479004, z: 0.020080566 }, s: -0.8675537 }, left: 105, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 29858, angle: 2.1916413, quart: Quaternion { v: Vector3 { x: 0.45648193, y: 0.036315918, z: 0.018920898 }, s: -0.88879395 }, left: 104, right: 41, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 30163, angle: 2.2832208, quart: Quaternion { v: Vector3 { x: 0.4152832, y: 0.036132813, z: 0.018188477 }, s: -0.9088135 }, left: 105, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 30470, angle: 2.3723123, quart: Quaternion { v: Vector3 { x: 0.37438965, y: 0.037902832, z: 0.016418457 }, s: -0.92633057 }, left: 105, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 30776, angle: 2.464512, quart: Quaternion { v: Vector3 { x: 0.3312378, y: 0.03869629, z: 0.01586914 }, s: -0.94262695 }, left: 105, right: 41, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 31082, angle: 2.5557244, quart: Quaternion { v: Vector3 { x: 0.28790283, y: 0.039245605, z: 0.014343262 }, s: -0.9567261 }, left: 104, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 31390, angle: 2.6473691, quart: Quaternion { v: Vector3 { x: 0.24371338, y: 0.03930664, z: 0.0138549805 }, s: -0.9689331 }, left: 105, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 32015, angle: 2.6641748, quart: Quaternion { v: Vector3 { x: 0.2355957, y: 0.040527344, z: 0.012512207 }, s: -0.97094727 }, left: 115, right: 109, state: State::Forward(1300) });
    chain.push(Point { time: 32322, angle: 2.662867, quart: Quaternion { v: Vector3 { x: 0.23620605, y: 0.041137695, z: 0.012878418 }, s: -0.97076416 }, left: 105, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 32629, angle: 2.6600428, quart: Quaternion { v: Vector3 { x: 0.23754883, y: 0.04144287, z: 0.013427734 }, s: -0.97039795 }, left: 105, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 32936, angle: 2.658206, quart: Quaternion { v: Vector3 { x: 0.23846436, y: 0.041503906, z: 0.012939453 }, s: -0.9701538 }, left: 105, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 33241, angle: 2.6588285, quart: Quaternion { v: Vector3 { x: 0.23815918, y: 0.0423584, z: 0.012756348 }, s: -0.97021484 }, left: 105, right: 106, state: State::Forward(1300) });
    chain.push(Point { time: 33547, angle: 2.6566308, quart: Quaternion { v: Vector3 { x: 0.23925781, y: 0.04272461, z: 0.012023926 }, s: -0.96990967 }, left: 105, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 33854, angle: 2.6529403, quart: Quaternion { v: Vector3 { x: 0.24102783, y: 0.042907715, z: 0.012451172 }, s: -0.9694824 }, left: 104, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 34161, angle: 2.6502912, quart: Quaternion { v: Vector3 { x: 0.24230957, y: 0.04260254, z: 0.012634277 }, s: -0.96917725 }, left: 106, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 34467, angle: 2.64882, quart: Quaternion { v: Vector3 { x: 0.24310303, y: 0.043273926, z: 0.010803223 }, s: -0.96899414 }, left: 103, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 34774, angle: 2.646168, quart: Quaternion { v: Vector3 { x: 0.24438477, y: 0.043029785, z: 0.010986328 }, s: -0.96868896 }, left: 105, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 35082, angle: 2.6418047, quart: Quaternion { v: Vector3 { x: 0.24645996, y: 0.04309082, z: 0.01184082 }, s: -0.96813965 }, left: 105, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 35387, angle: 2.6382957, quart: Quaternion { v: Vector3 { x: 0.24810791, y: 0.04272461, z: 0.013061523 }, s: -0.9677124 }, left: 105, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 35693, angle: 2.637202, quart: Quaternion { v: Vector3 { x: 0.24865723, y: 0.043273926, z: 0.012512207 }, s: -0.9675293 }, left: 105, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 0, angle: 0.0, quart: Quaternion { v: Vector3 { x: 0.9987793, y: 0.013305664, z: 0.048034668 }, s: 0.0 }, left: 0, right: 0, state: State::Rest });


    to_points2(&chain);

}
pub fn test4() {
    let mut chain = Vec::new();
    chain.push(Point { time: 5, angle: 0.0, quart: Quaternion { v: Vector3 { x: 0.99890137, y: 0.0068969727, z: 0.046875 }, s: 0.0 }, left: 0, right: 0, state: State::Rest });
    chain.push(Point { time: 12793, angle: 0.13037285, quart: Quaternion { v: Vector3 { x: 0.9967041, y: 0.0014038086, z: 0.04815674 }, s: 0.06567383 }, left: 47, right: 133, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13098, angle: 0.2301692, quart: Quaternion { v: Vector3 { x: 0.9921875, y: 0.00024414063, z: 0.04876709 }, s: 0.11505127 }, left: 38, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13403, angle: 0.32982424, quart: Quaternion { v: Vector3 { x: 0.9852295, y: -0.0012817383, z: 0.048950195 }, s: 0.16394043 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 13708, angle: 0.42444074, quart: Quaternion { v: Vector3 { x: 0.9763794, y: -0.0022583008, z: 0.04852295 }, s: 0.21051025 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14013, angle: 0.52281564, quart: Quaternion { v: Vector3 { x: 0.96484375, y: -0.0045166016, z: 0.04852295 }, s: 0.25823975 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14320, angle: 0.61855525, quart: Quaternion { v: Vector3 { x: 0.951355, y: -0.0061035156, z: 0.04876709 }, s: 0.30413818 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14626, angle: 0.71513003, quart: Quaternion { v: Vector3 { x: 0.9356079, y: -0.008666992, z: 0.047607422 }, s: 0.34973145 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 14933, angle: 0.8105174, quart: Quaternion { v: Vector3 { x: 0.9178467, y: -0.010070801, z: 0.047607422 }, s: 0.39398193 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15241, angle: 0.904495, quart: Quaternion { v: Vector3 { x: 0.8983154, y: -0.012634277, z: 0.04748535 }, s: 0.43658447 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15546, angle: 1.0012777, quart: Quaternion { v: Vector3 { x: 0.87609863, y: -0.014526367, z: 0.047790527 }, s: 0.4794922 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 15851, angle: 1.0980713, quart: Quaternion { v: Vector3 { x: 0.8518677, y: -0.01586914, z: 0.047058105 }, s: 0.52142334 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16155, angle: 1.1918693, quart: Quaternion { v: Vector3 { x: 0.82647705, y: -0.01727295, z: 0.04638672 }, s: 0.560791 }, left: 39, right: 108, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16460, angle: 1.2895479, quart: Quaternion { v: Vector3 { x: 0.7980957, y: -0.019958496, z: 0.045959473 }, s: 0.60040283 }, left: 40, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 16767, angle: 1.3852601, quart: Quaternion { v: Vector3 { x: 0.7684326, y: -0.02142334, z: 0.045410156 }, s: 0.63793945 }, left: 38, right: 107, state: State::ForwardLeft(1000, 40.0) });
    chain.push(Point { time: 17397, angle: 1.30735, quart: Quaternion { v: Vector3 { x: 0.7929077, y: -0.023498535, z: 0.042236328 }, s: 0.60736084 }, left: 119, right: 73, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 17702, angle: 1.217677, quart: Quaternion { v: Vector3 { x: 0.819397, y: -0.023742676, z: 0.042236328 }, s: 0.571167 }, left: 103, right: 39, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 18007, angle: 1.1329219, quart: Quaternion { v: Vector3 { x: 0.8428955, y: -0.022094727, z: 0.042419434 }, s: 0.53601074 }, left: 102, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 18313, angle: 1.0419879, quart: Quaternion { v: Vector3 { x: 0.8664551, y: -0.022094727, z: 0.042114258 }, s: 0.49700928 }, left: 102, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 18620, angle: 0.95388246, quart: Quaternion { v: Vector3 { x: 0.88757324, y: -0.0211792, z: 0.041625977 }, s: 0.45825195 }, left: 102, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 18927, angle: 0.868345, quart: Quaternion { v: Vector3 { x: 0.9064331, y: -0.020507813, z: 0.041015625 }, s: 0.41986084 }, left: 102, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 19232, angle: 0.7786227, quart: Quaternion { v: Vector3 { x: 0.92437744, y: -0.01940918, z: 0.04144287 }, s: 0.3786621 }, left: 102, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 19539, angle: 0.69057894, quart: Quaternion { v: Vector3 { x: 0.94018555, y: -0.018310547, z: 0.041625977 }, s: 0.33764648 }, left: 103, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 19844, angle: 0.6053327, quart: Quaternion { v: Vector3 { x: 0.9537964, y: -0.017028809, z: 0.04083252 }, s: 0.2972412 }, left: 102, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 20149, angle: 0.5144068, quart: Quaternion { v: Vector3 { x: 0.9663696, y: -0.016113281, z: 0.04071045 }, s: 0.25335693 }, left: 102, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 20454, angle: 0.4285471, quart: Quaternion { v: Vector3 { x: 0.9763794, y: -0.0146484375, z: 0.04107666 }, s: 0.21173096 }, left: 104, right: 41, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 20758, angle: 0.33962673, quart: Quaternion { v: Vector3 { x: 0.9848633, y: -0.0126953125, z: 0.041015625 }, s: 0.16784668 }, left: 104, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 21066, angle: 0.25379258, quart: Quaternion { v: Vector3 { x: 0.99121094, y: -0.010681152, z: 0.040771484 }, s: 0.12554932 }, left: 103, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 21373, angle: 0.16499485, quart: Quaternion { v: Vector3 { x: 0.99591064, y: -0.009521484, z: 0.03942871 }, s: 0.08062744 }, left: 102, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 21678, angle: 0.07663224, quart: Quaternion { v: Vector3 { x: 0.99853516, y: -0.007507324, z: 0.040100098 }, s: 0.035949707 }, left: 103, right: 41, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 21986, angle: 0.032309245, quart: Quaternion { v: Vector3 { x: 0.9991455, y: -0.00592041, z: 0.039733887 }, s: -0.008728027 }, left: 104, right: 40, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 22291, angle: 0.10627446, quart: Quaternion { v: Vector3 { x: 0.99786377, y: -0.004760742, z: 0.03955078 }, s: -0.051879883 }, left: 103, right: 41, state: State::ForwardRight(1200, 40.0) });
    chain.push(Point { time: 22920, angle: 0.22129533, quart: Quaternion { v: Vector3 { x: 0.99316406, y: -0.0040283203, z: 0.039245605 }, s: -0.11004639 }, left: 118, right: 45, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 23227, angle: 0.31017503, quart: Quaternion { v: Vector3 { x: 0.98724365, y: -0.0013427734, z: 0.03942871 }, s: -0.1541748 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 23534, angle: 0.40049705, quart: Quaternion { v: Vector3 { x: 0.97924805, y: 0.00036621094, z: 0.03930664 }, s: -0.19891357 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 23839, angle: 0.48695514, quart: Quaternion { v: Vector3 { x: 0.96972656, y: 0.002746582, z: 0.03894043 }, s: -0.2409668 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 24144, angle: 0.57788706, quart: Quaternion { v: Vector3 { x: 0.9577637, y: 0.0045776367, z: 0.03845215 }, s: -0.28497314 }, left: 105, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 24449, angle: 0.66480666, quart: Quaternion { v: Vector3 { x: 0.944458, y: 0.0079956055, z: 0.038085938 }, s: -0.3262329 }, left: 104, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 24754, angle: 0.75553226, quart: Quaternion { v: Vector3 { x: 0.92871094, y: 0.009643555, z: 0.036987305 }, s: -0.36883545 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 25062, angle: 0.8432266, quart: Quaternion { v: Vector3 { x: 0.9116211, y: 0.011108398, z: 0.036987305 }, s: -0.4091797 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 25368, angle: 0.9341908, quart: Quaternion { v: Vector3 { x: 0.89208984, y: 0.012329102, z: 0.035949707 }, s: -0.4501953 }, left: 104, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 25674, angle: 1.0242347, quart: Quaternion { v: Vector3 { x: 0.87091064, y: 0.014709473, z: 0.03527832 }, s: -0.48999023 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 25981, angle: 1.1155728, quart: Quaternion { v: Vector3 { x: 0.8475952, y: 0.016113281, z: 0.03527832 }, s: -0.5292969 }, left: 104, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 26286, angle: 1.2026424, quart: Quaternion { v: Vector3 { x: 0.8237915, y: 0.017028809, z: 0.033813477 }, s: -0.5734863 }, left: 104, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 26591, angle: 1.2941831, quart: Quaternion { v: Vector3 { x: 0.7970581, y: 0.018066406, z: 0.032714844 }, s: -0.60272217 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 26896, angle: 1.3814294, quart: Quaternion { v: Vector3 { x: 0.77001953, y: 0.020446777, z: 0.03149414 }, s: -0.6369629 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 27201, angle: 1.4717737, quart: Quaternion { v: Vector3 { x: 0.7404785, y: 0.022399902, z: 0.030212402 }, s: -0.67108154 }, left: 105, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 27508, angle: 1.5594076, quart: Quaternion { v: Vector3 { x: 0.7103882, y: 0.023742676, z: 0.028808594 }, s: -0.7028198 }, left: 104, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 27815, angle: 1.6500661, quart: Quaternion { v: Vector3 { x: 0.67785645, y: 0.02557373, z: 0.026550293 }, s: -0.7342529 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 28121, angle: 1.7412475, quart: Quaternion { v: Vector3 { x: 0.64367676, y: 0.027526855, z: 0.025390625 }, s: -0.76434326 }, left: 105, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 28742, angle: 1.8564095, quart: Quaternion { v: Vector3 { x: 0.5986328, y: 0.029174805, z: 0.023376465 }, s: -0.8001709 }, left: 120, right: 45, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 29048, angle: 1.9471726, quart: Quaternion { v: Vector3 { x: 0.56170654, y: 0.029968262, z: 0.022216797 }, s: -0.8265381 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 29355, angle: 2.0373812, quart: Quaternion { v: Vector3 { x: 0.52386475, y: 0.031799316, z: 0.020751953 }, s: -0.85095215 }, left: 105, right: 41, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 29661, angle: 2.1284275, quart: Quaternion { v: Vector3 { x: 0.4845581, y: 0.03302002, z: 0.020019531 }, s: -0.87390137 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 29967, angle: 2.2204576, quart: Quaternion { v: Vector3 { x: 0.44384766, y: 0.0345459, z: 0.018310547 }, s: -0.8952637 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 30275, angle: 2.3121643, quart: Quaternion { v: Vector3 { x: 0.4022827, y: 0.03491211, z: 0.018066406 }, s: -0.91467285 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 30580, angle: 2.4011376, quart: Quaternion { v: Vector3 { x: 0.36120605, y: 0.03564453, z: 0.016479492 }, s: -0.9316406 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 30885, angle: 2.4929838, quart: Quaternion { v: Vector3 { x: 0.3180542, y: 0.03741455, z: 0.0146484375 }, s: -0.9472046 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 31190, angle: 2.5823178, quart: Quaternion { v: Vector3 { x: 0.27545166, y: 0.038146973, z: 0.0126953125 }, s: -0.9604492 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 31495, angle: 2.6738622, quart: Quaternion { v: Vector3 { x: 0.23120117, y: 0.038513184, z: 0.011230469 }, s: -0.97210693 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 31801, angle: 2.7640414, quart: Quaternion { v: Vector3 { x: 0.18707275, y: 0.038513184, z: 0.011169434 }, s: -0.98150635 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 32106, angle: 2.8553946, quart: Quaternion { v: Vector3 { x: 0.14208984, y: 0.039733887, z: 0.008605957 }, s: -0.9890137 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 32414, angle: 2.9479663, quart: Quaternion { v: Vector3 { x: 0.09613037, y: 0.040527344, z: 0.0076293945 }, s: -0.99450684 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 32721, angle: 3.0367699, quart: Quaternion { v: Vector3 { x: 0.051879883, y: 0.04119873, z: 0.0059814453 }, s: -0.99780273 }, left: 105, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 33026, angle: 3.1288316, quart: Quaternion { v: Vector3 { x: 0.005859375, y: 0.040771484, z: 0.0052490234 }, s: -0.9991455 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 33331, angle: 3.0650501, quart: Quaternion { v: Vector3 { x: -0.038757324, y: 0.04119873, z: 0.0036010742 }, s: -0.9984131 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 33636, angle: 2.9733756, quart: Quaternion { v: Vector3 { x: -0.084472656, y: 0.04168701, z: 0.0017700195 }, s: -0.99554443 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 33941, angle: 2.8827915, quart: Quaternion { v: Vector3 { x: -0.12945557, y: 0.0413208, z: -0.00024414063 }, s: -0.99072266 }, left: 104, right: 40, state: State::ForwardRight(1300, 40.0) });
    chain.push(Point { time: 34571, angle: 2.8658085, quart: Quaternion { v: Vector3 { x: -0.13787842, y: 0.041015625, z: -0.00024414063 }, s: -0.989624 }, left: 133, right: 119, state: State::Forward(1300) });
    chain.push(Point { time: 34875, angle: 2.8669236, quart: Quaternion { v: Vector3 { x: -0.13726807, y: 0.042236328, z: -0.0016479492 }, s: -0.989624 }, left: 105, right: 105, state: State::Forward(1300) });
    chain.push(Point { time: 35182, angle: 2.8701215, quart: Quaternion { v: Vector3 { x: -0.13568115, y: 0.04248047, z: -0.0017089844 }, s: -0.98986816 }, left: 104, right: 106, state: State::Forward(1300) });
    chain.push(Point { time: 35489, angle: 2.8722558, quart: Quaternion { v: Vector3 { x: -0.13458252, y: 0.04333496, z: -0.0026855469 }, s: -0.9899292 }, left: 104, right: 106, state: State::Forward(1300) });
    chain.push(Point { time: 35796, angle: 2.872, quart: Quaternion { v: Vector3 { x: -0.13464355, y: 0.042907715, z: -0.0040283203 }, s: -0.9899292 }, left: 104, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 36101, angle: 2.8741672, quart: Quaternion { v: Vector3 { x: -0.13354492, y: 0.043640137, z: -0.004638672 }, s: -0.99005127 }, left: 105, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 36409, angle: 2.877664, quart: Quaternion { v: Vector3 { x: -0.1317749, y: 0.043945313, z: -0.005432129 }, s: -0.9902954 }, left: 104, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 36717, angle: 2.8812363, quart: Quaternion { v: Vector3 { x: -0.13000488, y: 0.043518066, z: -0.0053100586 }, s: -0.99053955 }, left: 105, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 37022, angle: 2.8821852, quart: Quaternion { v: Vector3 { x: -0.1295166, y: 0.042663574, z: -0.005554199 }, s: -0.9906616 }, left: 105, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 37327, angle: 2.8848546, quart: Quaternion { v: Vector3 { x: -0.12786865, y: 0.042785645, z: -0.012451172 }, s: -0.9908447 }, left: 105, right: 108, state: State::Forward(1300) });
    chain.push(Point { time: 37634, angle: 2.8895986, quart: Quaternion { v: Vector3 { x: -0.12591553, y: 0.04296875, z: -0.00390625 }, s: -0.99108887 }, left: 106, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 37940, angle: 2.8930485, quart: Quaternion { v: Vector3 { x: -0.12420654, y: 0.042785645, z: -0.0037841797 }, s: -0.991333 }, left: 104, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 38247, angle: 2.893879, quart: Quaternion { v: Vector3 { x: -0.1237793, y: 0.042297363, z: -0.0040283203 }, s: -0.99139404 }, left: 103, right: 107, state: State::Forward(1300) });
    chain.push(Point { time: 39247, angle: 2.893879, quart: Quaternion { v: Vector3 { x: -0.1237793, y: 0.042297363, z: -0.0040283203 }, s: -0.99139404 }, left: 0, right: 0, state: State::Rest });


    to_points2(&chain);

}

 */


//##################################################################################################
//##################################################################################################
//##################################################################################################
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn to_angle_test() {
        assert_eq!(0.0, to_angle(0.0, 1.0));

        assert_eq!(1.0*std::f32::consts::FRAC_PI_4, to_angle(1.0, 1.0));
        assert_eq!(2.0*std::f32::consts::FRAC_PI_4, to_angle(1.0, 0.0));
        assert_eq!(3.0*std::f32::consts::FRAC_PI_4, to_angle(1.0, -1.0));
        assert_eq!(4.0*std::f32::consts::FRAC_PI_4, to_angle(0.0, -1.0));
        assert_eq!(5.0*std::f32::consts::FRAC_PI_4, to_angle(-1.0, -1.0));
        assert_eq!(6.0*std::f32::consts::FRAC_PI_4, to_angle(-1.0, 0.0));
        assert_eq!(7.0*std::f32::consts::FRAC_PI_4, to_angle(-1.0, 1.0));

        assert_eq!(1.0*std::f32::consts::FRAC_PI_4/2.0, to_angle(0.4142135624, 1.0));
        assert_eq!(15.0*std::f32::consts::FRAC_PI_4/2.0, to_angle(-0.4142135624, 1.0));
        assert_eq!(13.0*std::f32::consts::FRAC_PI_4/2.0, to_angle(-1.0,0.4142135624));

    }
    #[test]
    fn test_to_pointttt() {
        let mut chain = Vec::new();
        for i in 0..25 {
            chain.push((i as f32 * 2.0, 50.0));
        }
        for i in 0..25 {
            chain.push((50.0, i as f32 * -4.0+50.0));
        }
        for i in 0..26 {
            chain.push((i as f32 * -2.0+50.0, -50.0));
        }

        for i in 0..25 {
            chain.push((i as f32 * -2.0, 50.0));
        }
        for i in 0..25 {
            chain.push((-50.0, i as f32 * -4.0+50.0));
        }
        for i in 0..26 {
            chain.push((i as f32 * 2.0-50.0, -50.0));
        }

        for ch in &chain  {
            let a0 = to_angle(-1.0, 0.0);
            let a1 = to_angle(ch.0, ch.1);

            let mut da = a1-a0;
            if da > std::f32::consts::PI {
                da -= std::f32::consts::PI*2.0;
            }
            if da <= -std::f32::consts::PI {
                da += std::f32::consts::PI*2.0;
            }
            println!("{:?}: {}|{} -> {:?}", ch, a0, a1, da);
        }
    }
}



















