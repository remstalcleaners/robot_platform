use std::sync::mpsc;
use std::sync::mpsc::{Sender, TryRecvError};
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;
use rppal::gpio::{Gpio, OutputPin};
use sensorlib::KILL;


pub struct Servo {
    thread: JoinHandle<()>,
    sender: Sender<u64>
}
impl Servo {
    pub fn new() -> Self {
        let (thread, sender) = run_async_asyncly();
        Self {
            thread,
            sender
        }
    }
    pub fn change(&self, time_micro: u64) {
        self.sender.send(time_micro);
    }
}



fn run_async_asyncly() -> (JoinHandle<()>, Sender<u64>){
    let (tx, rx) = mpsc::channel();
    let mut control_pin = Gpio::new().expect("Control pin Err").get(1).expect("Control pin 2 Err").into_output();

    println!("Start servo thread");
    let handle = thread::spawn(move ||
        {
            let mut duration = 0;
            loop {
                match rx.try_recv() {
                    Ok(x) => {
                        if x == 0 {
                            control_pin.set_low();
                            break;/* If KILL signal is send, end this loop */
                        }else{
                            duration = x + 1000;
                            if duration > 2000 {
                                duration = 2000;
                            }else if duration < 1000 {
                                duration = 1000;
                            }
                        }
                    }
                    Err(TryRecvError::Disconnected) => {break;}
                    Err(TryRecvError::Empty) => {}
                };
                control_pin.set_high();
                thread::sleep(Duration::from_micros(duration));
                control_pin.set_low();
            }
        }
    );
    println!("Servo Thread should run now ...");
    (handle, tx)
}
