use rppal::gpio::{Gpio, OutputPin, InputPin, Level};
use rppal::gpio::Trigger;
use std::time::{Duration, Instant};
use std::thread;
use std::sync::{Arc, Mutex, mpsc, MutexGuard};
use rppal::{pwm, gpio};
use rppal::pwm::Pwm;
use std::thread::JoinHandle;
use std::sync::mpsc::{Sender, TryRecvError};
use std::f32::consts::PI;
use std::cmp::{min, max};

pub struct PwmMotor {
    pwn_left: Pwm,
    pwn_right: Pwm,
    pin_left: OutputPin,
    pin_right: OutputPin,
}
const FREQUENCY: f64 = 1000.0;
impl PwmMotor {
    pub(crate) fn new(left_direction_pin: u8, right_direction_pin: u8) -> Self {
        let pwm_left = Pwm::with_frequency(
            pwm::Channel::Pwm0,
            FREQUENCY,
            0.0,
            pwm::Polarity::Normal,
            true,
        ).unwrap();
        let pwm_right = Pwm::with_frequency(
            pwm::Channel::Pwm1,
            FREQUENCY,
            0.0,
            pwm::Polarity::Normal,
            true,
        ).unwrap();
        let mut direction_left = Gpio::new().unwrap().get(left_direction_pin).unwrap().into_output();
        let mut direction_right = Gpio::new().unwrap().get(right_direction_pin).unwrap().into_output();
        Self {
            pwn_left: pwm_left,
            pwn_right: pwm_right,
            pin_left: direction_left,
            pin_right: direction_right,
        }
    }
    pub(crate) fn change(&mut self, mut left_speed: f64, mut right_speed: f64) {
        if left_speed  > 1.0  { left_speed  =  1.0; }
        if right_speed > 1.0  { right_speed =  1.0; }
        if left_speed  < -1.0 { left_speed  = -1.0; }
        if right_speed < -1.0 { right_speed = -1.0; }

        if left_speed  > 0.0 {
            self.pin_left.set_high();
        } else {
            self.pin_left.set_low();
        }
        if right_speed > 0.0 {
            self.pin_right.set_high();
        } else {
            self.pin_right.set_low();
        }
        let duty_left  = left_speed.abs();
        let duty_right = right_speed.abs();
        self.pwn_left.set_duty_cycle( duty_left );
        self.pwn_right.set_duty_cycle(duty_right);
    }
}