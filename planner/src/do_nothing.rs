use motorlib::map::Map;
use crate::planner::Planner;

pub struct DoNothing {

}
impl DoNothing {
    pub fn new() -> Self {
        Self {}
    }
}
impl Planner for DoNothing {
    fn run(&mut self, sensors: &Vec<f32>, map: &Map) -> (f32, f32) {
        (0.0,0.0)
    }
    fn interrupted(&mut self, id: i32) -> (f32, f32) {
        (0.0,0.0)
    }
    fn terminate(&self) -> bool {
        false
    }
}

