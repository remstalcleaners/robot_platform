use motorlib::map::Map;
use crate::planner::Planner;

pub struct SimplePlanner {
    index: i32,
}
impl SimplePlanner {
    pub fn new() -> Self {
        Self{
            index: 0,
        }
    }
}
impl Planner for SimplePlanner {
    fn run(&mut self, sensors: &Vec<f32>, map: &Map) -> (f32, f32) {
        if self.index > 1000 {
            return (0.0,0.0);
        }
        self.index += 1;
        if sensors[1] < 40.0 {
            (0.0,0.0)
        }else {
            (0.0, 100.0)
        }
    }
    fn interrupted(&mut self, id: i32) -> (f32, f32) {
        (0.0,0.0)
    }
    fn terminate(&self) -> bool {
        false
    }
}

