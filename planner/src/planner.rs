use motorlib::map::Map;


pub trait Planner<T: Send = Self>: Send {
    fn run(&mut self, sensors: &Vec<f32>, map: &Map) -> (f32, f32);
    fn interrupted(&mut self, id: i32) -> (f32, f32);
    fn terminate(&self) -> bool;
}






