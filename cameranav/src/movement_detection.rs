use opencv::prelude::*;
use std::{thread, time};
use opencv::core::{Mat, Vec3b, Mat1b, Size, Point, absdiff, Point2f};
use opencv::videoio::VideoCapture;
use std::time::{Duration, Instant};
use opencv::imgproc::{find_contours, min_enclosing_circle, THRESH_BINARY, THRESH_BINARY_INV, threshold};
use opencv::types::VectorOfMat;



pub struct MovementFilter {
    diff: Mat,
    diff2: Mat,
}
impl MovementFilter {
    pub fn new() -> Self {
        Self{
            diff: Default::default(),
            diff2: Default::default()
        }
    }
    pub fn filter(&mut self, grey: &Mat, grey_old: &Mat, min_radius: f32) -> Vec<(f32, f32, f32)> {
        absdiff(
            grey,
            &grey_old,
            &mut self.diff
        ).expect("get difference not working");
        threshold(
            &self.diff,
            &mut self.diff2,
            25.0,
            255.0,
            THRESH_BINARY
        ).expect("get threshold not working");
        let mut contours = VectorOfMat::new();
        find_contours(
            &self.diff2,
            &mut contours,
            0,
            2,
            Point { x: 0, y: 0 }
        ).expect("conture search error");
        let mut liste = Vec::new();
        for cont in &contours {
            let mut center = Point2f { x: 0.0, y: 0.0 };
            let mut radius = 0.0;
            match min_enclosing_circle(&cont, &mut center, &mut radius) {
                Ok(_) => {}
                Err(_) => { continue }
            }
            if radius < min_radius { continue }
            liste.push((center.x, center.y, radius));
        }
        liste
    }
}









