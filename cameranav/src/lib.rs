use std::{fs, thread};
use std::time::{Duration, Instant};
use opencv::core::{Mat, MatTraitConst, MatTraitConstManual, Point, Scalar};
use opencv::features2d::*;
use opencv::imgproc::{FONT_HERSHEY_PLAIN, LINE_8};
use opencv::types::*;
use crate::camera::Camera;
use crate::color_detection::*;
use crate::goal_detector::detect;
use crate::gui::*;
use crate::movement_detection::MovementFilter;
use crate::observer::Observer;
use crate::relative_movement::test_match_images;
use crate::test_structure_from_motion::Odometry;
use crate::test_structure_from_motion_2::Odometry2;
use crate::test_structure_from_motion_3::Odometry3;


pub mod camera;
pub mod color_detection;
pub mod gui;
pub mod movement_detection;
pub mod visual_odometry;
pub mod cam_map;
mod relative_movement;
mod test_functions;
mod goal_detector;
mod obstacle_avoidance;
mod simple_odometry;
mod test_structure_from_motion;
mod dept_optic_flow;
mod test_structure_from_motion_2;
mod test_structure_from_motion_3;
mod observer;

pub fn libmain() {
    println!("Start camera ss");
    //test_match_images();
    //detect();
/*
    let mut first = true;
    let mut od = Odometry2::new();
    //let paths = fs::read_dir("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs").unwrap();
    for i in 1..493 {
        let name = format!("IM{}.png", i);
        let path = format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs/IM{}.png", i);
        let img = opencv::imgcodecs::imread(&path, opencv::imgcodecs::IMREAD_GRAYSCALE).unwrap();
        if first {
            od.first(&img);
            first = false;
        } else {
            od.step(&img, &name);
        }
    }


 */

/*

    let time = Instant::now();

    let mut first = true;
    let mut od = Odometry3::new(1.0, (0.0, 0.0), 1920, 1080, 10);
    let paths = fs::read_dir("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs").unwrap();
    for i in 31..493 {
        let name = format!("IM{}.png", i);
        let path = format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs/IM{}.png", i);
        //println!("{}", path);
        let img = opencv::imgcodecs::imread(&path, opencv::imgcodecs::IMREAD_GRAYSCALE).unwrap();
        if first {
            od.first(&img, &name);
            first = false;
        }else{
            od.step(&img, &name);
        }
    }
    println!("Time elapsed: {}", time.elapsed().as_secs());


 */


/*

 */
    /*


    let mut first = true;
    let mut od = Odometry::new(1.0,(0.0, 0.0));
    let paths = fs::read_dir("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs").unwrap();
    for path in paths {
        let path = path.unwrap();
        let path = path.path();
        let name = path.file_name().unwrap().to_str().unwrap();
        let path = path.as_os_str().to_str().unwrap();
        println!("{}", path);
        let path = println!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs/IM{}.png", );
        let img = opencv::imgcodecs::imread(path, opencv::imgcodecs::IMREAD_GRAYSCALE).unwrap();
        //println!("{} {:?}", img.dims(), img.size());
        if first {
            od.first(&img, name);
            first = false;
        }else{
            od.step(&img, name);
        }
    }

    let mut detector = <dyn ORB>::create(
        500,
        1.2,
        8,
        31,
        0,
        2,
        ORB_ScoreType::HARRIS_SCORE,
        31,
        20
    ).expect("ORB detector didn't create");
    let img = opencv::imgcodecs::imread("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs/IM35.png", opencv::imgcodecs::IMREAD_GRAYSCALE).unwrap();
    let mut res  = VectorOfKeyPoint::default();
    let mut res2  = Mat::default();
    //detector.detect(&img,&mut res,&Mat::default()).unwrap();
    detector.detect_and_compute(&img, &Mat::default(),&mut res, &mut res2, false).unwrap();
    println !("{:?}", res2);
    let rows = res2.size().unwrap().height;
    for i in 0..rows {
        println !("{:?}", res2.row(i));
    }

    println!("End sdfsdfsd ");
     */

    //Find the red blue blue red car
    let mut obv = Observer::new();
    let mut hsv = Mat::default();
    //let paths = fs::read_dir("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs").unwrap();
    let mut first = true;
    let mut map_img = Mat::default();
    let mut points = Vec::new();
    let mut x = 0.0;
    let mut y = 0.0;

    let mut vx = 0.0;
    let mut vy = 0.0;

    points.push((0.0,0.0));
    for i in 1..28 {
        let path = format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgobs/{}.jpeg", i);
        let mut img = opencv::imgcodecs::imread(&path, opencv::imgcodecs::IMREAD_COLOR).unwrap();
        if first {
            map_img = img.clone();
            first = false;
        }
        Camera::convert_hsv(&img, &mut hsv).expect("Conversion failed");
        //Camera::convert_hsv(&img, &mut hsv).expect("Conversion failed");
        println!("{}", path);
        let res = obv.run(&hsv, i);










        points.push(((res.0+res.2+res.4+res.6)/4.0, (res.1+res.3+res.5+res.7)/4.0));
        opencv::imgproc::put_text(&mut img, &format!("Score: {}", res.0), Point{ x: 20, y: 20 }, FONT_HERSHEY_PLAIN, 1.0, Default::default(), 2, LINE_8, false).unwrap();

        opencv::imgproc::arrowed_line(&mut img,
                                      Point{ x: res.0 as i32, y: res.1 as i32 },
                                      Point{ x: res.6 as i32, y: res.7 as i32},
                                      Scalar::new(140.0,235.0,235.0,0.0),
                                      2, LINE_8, 0, 0.2).unwrap();
        opencv::imgproc::arrowed_line(&mut img,
                                      Point{ x: res.2 as i32, y: res.3 as i32 },
                                      Point{ x: res.4 as i32, y: res.5 as i32},
                                      Scalar::new(0.0,235.0,0.0,0.0),
                                      2, LINE_8, 0, 0.2).unwrap();
        opencv::imgcodecs::imwrite(&format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgobs3/A{:02}.png", i), &img, &Default::default()).expect("Cannot save");


        opencv::imgproc::arrowed_line(&mut map_img,
                                      Point{ x: res.0 as i32, y: res.1 as i32 },
                                      Point{ x: res.6 as i32, y: res.7 as i32},
                                      Scalar::new(140.0,235.0,235.0,0.0),
                                      2, LINE_8, 0, 0.2).unwrap();
        opencv::imgproc::arrowed_line(&mut map_img,
                                      Point{ x: res.2 as i32, y: res.3 as i32 },
                                      Point{ x: res.4 as i32, y: res.5 as i32},
                                      Scalar::new(0.0,235.0,0.0,0.0),
                                      2, LINE_8, 0, 0.2).unwrap();

        opencv::imgproc::circle(&mut map_img, Point{ x: ((res.0+res.2+res.4+res.6)/4.0) as i32, y: ((res.1+res.3+res.5+res.7)/4.0) as i32 }, 10, Scalar::new(100.0,100.0,100.0,0.0), 2, LINE_8, 0).unwrap();

        opencv::imgcodecs::imwrite(&format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgobs3/B{:02}.png", i), &map_img, &Default::default()).expect("Cannot save");
    }

    for i in 3..26 {
        let path = format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgobs/{}.jpeg", i);
        let mut img = opencv::imgcodecs::imread(&path, opencv::imgcodecs::IMREAD_COLOR).unwrap();

        let x = points[i].0 + points[i-1].0 + points[i-2].0 + points[i+1].0 + points[i+2].0;
        let y = points[i].1 + points[i-1].1 + points[i-2].1 + points[i+1].1 + points[i+2].1;
        let x = (x/5.0) as i32;
        let y = (y/5.0) as i32;

        opencv::imgproc::circle(&mut img, Point{ x: points[i].0 as i32, y: points[i].1 as i32 }, 10, Scalar::new(200.0,200.0,200.0,0.0), 2, LINE_8, 0).unwrap();

        opencv::imgproc::circle(&mut img, Point{ x: points[i+1].0 as i32, y: points[i+1].1 as i32 }, 10, Scalar::new(100.0,100.0,100.0,0.0), 2, LINE_8, 0).unwrap();
        opencv::imgproc::circle(&mut img, Point{ x: points[i+2].0 as i32, y: points[i+2].1 as i32 }, 10, Scalar::new(100.0,100.0,100.0,0.0), 2, LINE_8, 0).unwrap();

        opencv::imgproc::circle(&mut img, Point{ x: points[i-1].0 as i32, y: points[i-1].1 as i32 }, 10, Scalar::new(50.0,50.0,50.0,0.0), 2, LINE_8, 0).unwrap();
        opencv::imgproc::circle(&mut img, Point{ x: points[i-2].0 as i32, y: points[i-2].1 as i32 }, 10, Scalar::new(50.0,50.0,50.0,0.0), 2, LINE_8, 0).unwrap();

        opencv::imgproc::circle(&mut img, Point{ x: x, y: y }, 10, Scalar::new(0.0,0.0,200.0,0.0), 2, LINE_8, 0).unwrap();

        opencv::imgcodecs::imwrite(&format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgobs3/Z{:02}.png", i), &img, &Default::default()).expect("Cannot save");
    }



    println!("End camera ");

    /*

    let mut cam = Camera::new(0).unwrap();
    let mut img = Mat::default();
    let mut index = 0;
    loop {
        cam.take_img(&mut img);
        opencv::imgcodecs::imwrite(&format!("IM{}.png",index), &img, &Default::default()).expect("Cannot save");
        index += 1;
    }
    let mut cam = Camera::new(0).unwrap();
    let mut img = Mat::default();
    let mut hsv = Mat::default();

    let mut time = Instant::now();
    loop {
        cam.take_img(&mut img);
        Camera::convert_hsv(&img, &mut hsv);
        show(&img, "colors".to_string());
        //thread::sleep(Duration::from_secs(1));
    }

     */


}
/*
Measures of test frame
23 cm width
12,5 cm height
Size:
5 cm width
5 cm height
*/





//##################################################################################################
//##################################################################################################
//##################################################################################################


/*
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_panel2() {
        println!("Start camera ");
        let mut cam = Camera::new(0).unwrap();
        let mut img = Mat::default();
        let mut hsv = Mat::default();
        let mut red = get_red();
        let mut blue = get_blue();
        let mut green = get_green();
        let mut yellow = get_yellow();
        let mut time = Instant::now();
        loop {
            cam.take_img(&mut img);
            Camera::convert_hsv(&img, &mut hsv);
            let red_list = red.filter_rectangle(&hsv,20.0);
            let blue_list = blue.filter_rectangle(&hsv,20.0);
            let green_list = green.filter_rectangle(&hsv,20.0);
            let yellow_list = yellow.filter_rectangle(&hsv,20.0);
            //println!("{} | {} | {} | {}", red_list.len(),blue_list.len(),green_list.len(),yellow_list.len());

            //show_with_rects(&mut hsv, "Red".to_string(), &red_list);
            //show_with_rects(&mut hsv, "Blue".to_string(), &blue_list);
            //show_with_rects(&mut hsv, "Green".to_string(), &green_list);
            //time = Instant::now();
            let (score, matched) = match_shape(&red_list,&blue_list,&green_list,&yellow_list,1.0);
            //println!("{}", time.elapsed().as_micros());
            paint_rects(&mut img, &red_list, Scalar::new(0.0,0.0,100.0,0.0));
            paint_rects(&mut img, &blue_list, Scalar::new(100.0,0.0,0.0,0.0));
            paint_rects(&mut img, &green_list, Scalar::new(0.0,100.0,0.0,0.0));
            paint_rects(&mut img, &yellow_list, Scalar::new(0.0,100.0,100.0,0.0));
            if score < 1000.0 {
                paint_match(&mut img, &matched, Scalar::new(0.0,0.0,0.0,0.0));
            }
            show(&img, "colors".to_string());
            //thread::sleep(Duration::from_secs(1))
        }
    }
    #[test]
    fn test_color() {
        println!("Start Color detection...");
        let mut cam = Camera::new(0).unwrap();
        let mut img = Mat::default();
        let mut hsv = Mat::default();
        let mut blue = get_red();
        loop {
            cam.take_img(&mut img);
            Camera::convert_hsv(&img, &mut hsv);
            paint_cycles(&mut hsv, &list, Scalar::new(0.0,0.0,0.0,0.0));
            let list = blue.filter_circle(&hsv,10.0);
            //show(&hsv, "HSV".to_string());
            //show_with_cycles(&mut hsv, "HSV Circle".to_string(), &list);
        }
    }
    #[test]
    fn test_panel1() {

        println!("Start camera ");
        let mut cam = Camera::new(0).unwrap();
        let mut img = Mat::default();
        let mut hsv = Mat::default();
        let mut red = get_red();
        let mut blue = get_blue();
        let mut green = get_green();
        let mut yellow = get_yellow();
        let mut time = Instant::now();
        loop {
            cam.take_img(&mut img);
            Camera::convert_hsv(&img, &mut hsv);
            let red_list = red.filter_rectangle(&hsv,20.0);
            let blue_list = blue.filter_rectangle(&hsv,20.0);
            let green_list = green.filter_rectangle(&hsv,20.0);
            let yellow_list = yellow.filter_rectangle(&hsv,20.0);
            //println!("{} | {} | {} | {}", red_list.len(),blue_list.len(),green_list.len(),yellow_list.len());

            //show_with_rects(&mut hsv, "Red".to_string(), &red_list);
            //show_with_rects(&mut hsv, "Blue".to_string(), &blue_list);
            //show_with_rects(&mut hsv, "Green".to_string(), &green_list);
            //time = Instant::now();
            let (score, matched) = match_shape(&red_list,&blue_list,&green_list,&yellow_list,1.0);
            //println!("{}", time.elapsed().as_micros());
            paint_rects(&mut img, &red_list, Scalar::new(0.0,0.0,100.0,0.0));
            paint_rects(&mut img, &blue_list, Scalar::new(100.0,0.0,0.0,0.0));
            paint_rects(&mut img, &green_list, Scalar::new(0.0,100.0,0.0,0.0));
            paint_rects(&mut img, &yellow_list, Scalar::new(0.0,100.0,100.0,0.0));
            if score < 1000.0 {
                paint_match(&mut img, &matched, Scalar::new(0.0,0.0,0.0,0.0));
            }
            show(&img, "colors".to_string());
            //thread::sleep(Duration::from_secs(1));
        }
    }
    #[test]
    fn test_movement() {
        println!("Start Movement detection...");
        let mut cam = Camera::new(0).unwrap();
        let mut img = Mat::default();
        let mut grey = Mat::default();
        let mut grey_old = Mat::default();
        let mut mov = MovementFilter::new();
        loop {
            cam.take_img(&mut img);
            Camera::convert_grey(&img, &mut grey);
            let list = mov.filter(&grey, &grey_old, 10.0);
            grey_old = grey.clone();
            //show_with_cycles(&mut grey, "Hov Circle".to_string(), &list);
        }
    }
}


 */

