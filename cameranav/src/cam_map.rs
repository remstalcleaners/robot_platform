use std::{thread, time};
use std::time::{Duration, Instant};

use opencv::prelude::*;
use opencv::core::{Mat, Vec3b, Mat1b, Size, Point, Ptr, NORM_HAMMING, DMatch};
use opencv::videoio::{CAP_PROP_FRAME_HEIGHT, CAP_PROP_FRAME_WIDTH, VideoCapture};
use opencv::imgproc::{THRESH_BINARY, THRESH_BINARY_INV};
use opencv::imgproc::{cvt_color, COLOR_BGR2HSV, COLOR_BGR2GRAY};
use opencv::features2d::ORB;
use opencv::core::Vector;
use opencv::core::KeyPoint;
use opencv::types::VectorOfMat;
use crate::Camera;
//use motorlib::motor::MotorInternal;
use sensorlib::imu::IMU;
/*
#[derive(Clone)]
pub struct Keys {
    key_points: Vector<KeyPoint>,
    descriptors: VectorOfMat,
}
pub struct Matches {
    matches: Vector<DMatch>,
}
impl Keys {
    pub fn new() -> Self {
        let mut key_points = Vector::<KeyPoint>::new();
        let mut descriptors = VectorOfMat::new();
        Self { key_points, descriptors }
    }
    pub fn img_to_features(&mut self, img: &Mat) {
        let mut orb = <dyn ORB>::default().unwrap();
        orb.detect_and_compute(img, &Mat::default(), &mut self.key_points, &mut self.descriptors, false);
    }
}
impl Matches {
    pub fn get_matches(keys_1: &Keys, keys_2: &Keys) -> Self {
        let mut bf = opencv::features2d::BFMatcher::create(NORM_HAMMING,true).unwrap();
        let mut matches = Vector::<DMatch>::new();
        bf.train_match(&keys_1.descriptors, &keys_2.descriptors, &mut matches, &Mat::default());
        return Matches{ matches };
    }
}


struct Map {
    grid: Vec<Vec<Vec<Keys>>>,
}
impl Map {
    fn scan_node(cam: &mut Camera, motor: &mut MotorInternal, steps: usize) -> Vec<Keys> {
        let mut origin_img = Mat::default();
        cam.take_img(&mut origin_img);
        let mut keys = Vec::new();
        let mut imu = IMU::new();
        for i in 0..steps {
            let mut img = Mat::default();
            cam.take_img(&mut img);
            let mut key = Keys::new();
            key.img_to_features(&img);
            keys.push(key);
            motor.turn(&mut imu,90.0,false);
        }
        keys
    }
}







 */
