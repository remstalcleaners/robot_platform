use std::{thread, time};
use std::fs::read_dir;
use std::path::PathBuf;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};
use opencv::calib3d::{CALIB_CB_ADAPTIVE_THRESH, CALIB_CB_NORMALIZE_IMAGE, calibrate_camera, find_chessboard_corners, project_points};

use opencv::prelude::*;
use opencv::core::{Mat, Vec3b, Mat1b, Size, Point, TermCriteria_Type, TermCriteria, Point3i, norm, NORM_L2};
use opencv::imgcodecs::{imread, IMREAD_COLOR, imwrite};
use opencv::videoio::{CAP_PROP_BUFFERSIZE, CAP_PROP_FRAME_HEIGHT, CAP_PROP_FRAME_WIDTH, VideoCapture};
use opencv::imgproc::{corner_sub_pix, THRESH_BINARY, THRESH_BINARY_INV};
use opencv::imgproc::{cvt_color, COLOR_BGR2HSV, COLOR_BGR2GRAY};
use opencv::types::{VectorOfMat, VectorOfPoint3d, VectorOfPoint3f, VectorOfPoint3i, VectorOfVectorOfPoint3f, VectorOfVectorOfPoint3i};

/*
  Important: I was able to remove smearing with: v4l2-ctl --help
  1. See what is possible for the camera:  v4l2-ctl --all
  2. Disable auto exposure, because this sucks: v4l2-ctl -c exposure_auto=1
  3. Set fixed exposure low (or high?): v4l2-ctl -c exposure_absolute=50
  This way I got pictures at daylight without blurring
*/

pub struct Camera {
    source: VideoCapture,
    pub camera_prop: Option<(Mat, Mat)>,
}
impl Camera {
    pub fn new(index: i32) -> Result<Self,()> {
        let mut camera = match VideoCapture::new(index,0) {
            Ok(x) => x,
            Err(_) => return Err(()),
        };
        camera.set(CAP_PROP_FRAME_WIDTH, 1280.0);
        camera.set(CAP_PROP_FRAME_HEIGHT, 720.0);
        camera.set(CAP_PROP_BUFFERSIZE, 3.0);
        Ok(
            Self {
                source: camera,
                camera_prop: None
            }
        )
    }
    pub fn take_img(&mut self, img: &mut Mat) -> Result<bool,()> {
        match self.source.read(img){
            Ok(x) => Ok(x),
            Err(_) => Err(()),
        }
    }
    pub fn convert_hsv(img: &Mat, hsv: &mut Mat) -> Result<(),()> {
        match cvt_color(img, hsv, COLOR_BGR2HSV, 0){
            Ok(_) => Ok(()),
            Err(_) =>  Err(()),
        }
    }
    pub fn convert_grey(img: &Mat, grey: &mut Mat) -> Result<(),()> {
        match cvt_color(img, grey, COLOR_BGR2GRAY, 0){
            Ok(_) => Ok(()),
            Err(_) =>  Err(()),
        }
    }
}
pub fn take_calibration_imgs(path: PathBuf, width: i32, height: i32) {
    println!("Start taking calibration images");
    let mut cam = Camera::new(0).unwrap();
    let time = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap().as_secs();

    for i in 0..100 {
        loop {
            let mut img = Mat::default();
            cam.take_img(&mut img);
            let mut grey = Mat::default();
            Camera::convert_grey(&img, &mut grey);
            let mut corners = VectorOfPoint3f::new();
            let ret = find_chessboard_corners(&grey, Size{ width: width, height: height }, &mut corners,CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE).unwrap();
            if ret {
                let name = format!("CAL{}.{}.png", time, i);
                imwrite(&name, &img, &Default::default());
                println!("Image {} saved.", i);
                break;
            }
        }
    }
    println!("All 100 Calibration images saved. FINISH");
}
pub fn calibrate_cam(path: PathBuf, width: i32, height: i32) -> (Mat, Mat) {
    //Load images
    let paths = read_dir(&path).unwrap();
    let mut obj_points = VectorOfVectorOfPoint3i::new();
    let mut objp = VectorOfPoint3i::new();
    for i in 0..width {
        for j in 0..height {
            objp.push(Point3i{
                x: i,
                y: j,
                z: 0
            })
        }
    }
    let mut img_points = VectorOfVectorOfPoint3f::new();
    let mut shape = Size{ width: 0, height: 0 };
    for path in paths {
        let img = imread(path.unwrap().path().to_str().unwrap(), IMREAD_COLOR).unwrap();
        let mut gray = Mat::default();
        cvt_color(&img,&mut gray, COLOR_BGR2GRAY,0);
        let mut corners = VectorOfPoint3f::new();
        let ret = find_chessboard_corners(&gray, Size{ width: width, height: height }, &mut corners,CALIB_CB_ADAPTIVE_THRESH+CALIB_CB_NORMALIZE_IMAGE).unwrap();
        if ret {
            obj_points.push(objp.clone());
            corner_sub_pix(&gray, &mut corners, Size{ width: 11, height: 11 }, Size{ width: -1, height: -1 }, TermCriteria {
                typ: 1,
                max_count: 1000,
                epsilon: 0.0
            }).unwrap();
            img_points.push(corners);
        }
    }

    let mut camera_matrix = Mat::default();
    let mut distr_coeffs = Mat::default();
    let mut rvecs = VectorOfMat::new();
    let mut tvecs = VectorOfMat::new();
    calibrate_camera(
        &obj_points,
        &img_points,
        shape,
        &mut camera_matrix,
        &mut distr_coeffs,
        &mut rvecs,
        &mut tvecs,
        0,
        TermCriteria{
            typ:  1,
            max_count: 300000,
            epsilon: 0.0
        }
    ).unwrap();
    /*
    let mut jacobinan  = Mat::default();
    let mut img_proj = Mat::default();
    for i in 0..obj_points.len() {
        project_points(&obj_points.get(i), &rvecs.get(i), &tvecs.get(i), &camera_matrix, &distr_coeffs, &mut img_proj, &mut jacobinan, 0.0);
        norm(&img_points.get(i),NORM_L2,&img_proj)/img_points.get(i).len();
    }
     */

    (camera_matrix, distr_coeffs)
}





















