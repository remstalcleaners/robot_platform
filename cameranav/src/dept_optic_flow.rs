use nalgebra::DMatrix;
use opencv::core::{Mat, Point2f};
use opencv::types::{VectorOfPoint2f, VectorOfu8};

/*
 Based on the paper: Optical Flow based robot obstacle avoidance
 Kahlouche Souhila and Achour Karim
 https://journals.sagepub.com/doi/pdf/10.5772/5715

 Minimal Rust reimplementation of https://github.com/zainmehdi/Optical-Flow-based-Obstacle-Avoidance
 with ROS and stuff removed

*/




//Calculate the Focus of Expansion
pub fn calculate_foe(prev_pts: &VectorOfPoint2f, next_pts: &VectorOfPoint2f, status: &VectorOfu8) -> Result<Point2f, opencv::Error> {
    let mut a = DMatrix::<f32>::zeros(next_pts.len(),2);
    let mut b = DMatrix::<f32>::zeros(next_pts.len(),1);

    for i in 0..next_pts.len() {
        if status.get(i)? == 1 {
            let tmp = next_pts.get(i)? - prev_pts.get(i)?;
            a[(i,0)] = next_pts.get(i)?.x-prev_pts.get(i)?.y;
            a[(i,1)] = next_pts.get(i)?.y-prev_pts.get(i)?.y;

            b[(i,0)] = prev_pts.get(i)?.x * tmp.x - prev_pts.get(i)?.y * tmp.y;
        }
    }
    let a2 = a.clone();
    let foe = (a.transpose()*a).try_inverse().unwrap();
    let foe = foe*a2.transpose()*b;

    return Ok(Point2f{x: foe[(0,0)], y: foe[(1,0)]});
}




























