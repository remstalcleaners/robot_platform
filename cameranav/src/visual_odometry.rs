use std::{thread, time};
use std::time::{Duration, Instant};
use opencv::calib3d::{decompose_essential_mat, find_essential_mat, RANSAC};

use opencv::prelude::*;
use opencv::core::{Mat, Vec3b, Mat1b, Size, Point, Ptr, NORM_HAMMING, DMatch, no_array};
use opencv::videoio::{CAP_PROP_FRAME_HEIGHT, CAP_PROP_FRAME_WIDTH, VideoCapture};
use opencv::imgproc::{THRESH_BINARY, THRESH_BINARY_INV};
use opencv::imgproc::{cvt_color, COLOR_BGR2HSV, COLOR_BGR2GRAY};
use opencv::features2d::ORB;
use opencv::core::Vector;
use opencv::core::KeyPoint;
use opencv::platform_types::size_t;
use opencv::types::VectorOfMat;
use crate::Camera;
use sensorlib::imu::IMU;
use std::thread::JoinHandle;
use std::sync::{Arc, mpsc, Mutex};
use std::sync::mpsc::{TryRecvError, Sender};
use crate::relative_movement::compare_imgs;

struct CamNav {
    handle: JoinHandle<()>,
    commands: Sender<(i32, i32)>,
    data: Arc<Mutex<i64>>,
}
impl CamNav {
    pub fn new() -> Self {
        let (handle, commands, data) = cam_nav_async();
        Self {
            handle: handle,
            commands: commands,
            data: data,
        }
    }
}
fn cam_nav_async() -> (
    JoinHandle<()>,
    Sender<(i32, i32)>,
    Arc<Mutex<i64>>
) {
    println!("Init cam nav ...");
    let mut data = Arc::new(Mutex::new(0));
    let data_handler = data.clone();
    let (tx, rx) = mpsc::channel();
    /**/
    println!("Create cam nav thread ...");
    let handle= thread::spawn(move || {
        let mut imgs: Vec<Box<Mat>> = Vec::with_capacity(100_000);
        let mut times: Vec<i32> = Vec::with_capacity(100_000);
        let mut distances: Vec<Vec<(f32,f32)>> = Vec::with_capacity(100_000);
        let mut angle: i32 = 0;
        let mut old_angle: i32 = 0;
        let mut cam = CamSource::new();
        let mut time = Instant::now();
        println!("Start cam nav loop");
        loop {
            match rx.try_recv() {
                Ok((x, y)) => {
                    match x {
                        1 => {
                            /* Got current 2D angle (IMU)*/
                            angle = y;
                        }
                        42 => {
                            println!("KILL signal for cam nav");
                            cam.commands.send(42);//Also kill the img col, cam source
                            break;
                        }
                        _ => {}
                    }
                }
                Err(TryRecvError::Disconnected) => {
                    println!("ERROR");
                    break;
                }
                Err(TryRecvError::Empty) => {}
            };
            if old_angle-angle > 10 || old_angle-angle < 10 || time.elapsed().as_secs() > 4 {
                time = Instant::now();
                old_angle = angle;
                let img_col = cam.data.lock().unwrap();
                let time = img_col.0;
                let img = img_col.1.clone();
                let mut this_distances = Vec::with_capacity(10);
                let start = imgs.len() - 1;
                for i in 0..(1+std::cmp::min(imgs.len(), 10)) {
                    let distance = compare_imgs(&img,&imgs[start - i]);
                    this_distances.push(distance);
                }
                imgs.push(Box::new(img));
                times.push(time);
                distances.push(this_distances);
            }
        }
    });
    println!("Cam nav Thread should run now ...");
    (handle, tx, data)
}

























// Thread that only collects images and nothing more
struct CamSource {
    handle: JoinHandle<()>,
    commands: Sender<i32>,
    data: Arc<Mutex<(i32,Mat)>>,
}
impl CamSource {
    fn new() -> Self {
        let (handle, commands, data) = img_collector_async();
        Self {
            handle: handle,
            commands: commands,
            data: data,
        }
    }
}
fn img_collector_async() -> (
    JoinHandle<()>,
    Sender<i32>,
    Arc<Mutex<(i32,Mat)>>,
    //Arc<Mutex<(Vec<u8>, i32)>>
) {
    println!("Init img collector ...");
    let mut data = Arc::new(Mutex::new((0, Mat::default())));
    let data_handler = data.clone();
    let (tx, rx) = mpsc::channel();
    /**/
    println!("Create img coll thread ...");
    let handle= thread::spawn(move || {
        let mut cam = Camera::new(0).expect("Error: Camera cannot be accessed!");
        let mut start_time = Instant::now();
        println!("Start img coll loop");
        loop {
            match rx.try_recv() {
                Ok(x) => {
                    match x {
                        42 => {
                            println!("KILL signal for img col, cam source");
                            break;
                        }
                        _ => {}
                    }
                }
                Err(TryRecvError::Disconnected) => {
                    println!("ERROR");
                    break;
                }
                Err(TryRecvError::Empty) => {}
            };
            let mut img = Mat::default();
            cam.take_img(&mut img);
            let time = start_time.elapsed().as_millis() as i32;
            if let Ok(mut data_locked) = data_handler.try_lock() {
                data_locked.0 = time;
                data_locked.1 = img;
            }
        }
    });
    println!("Img Coll Thread should run now ...");
    (handle, tx, data)
}

















