use opencv::core::Mat;
use opencv::video::calc_optical_flow_farneback;

/*


fn to_3d(grey_old: &Mat, grey_new: &Mat) {
    //let flow = calc_optical_flow_farneback(prev_gray, curr_gray, None, 0.5, 3, 15, 3, 5, 1.2, 0);
    let mut flow_img = Mat::default();
    calc_optical_flow_farneback(
        grey_old,
        grey_new,
        &mut flow_img,
        0.5,
        3,
        15,
        3,
        5,
        1.2,
        0
    ).expect("Cannot calc opt flow");

    let depth = depthFromFlow(&flow_img, 1.0);

}


fn depth_from_flow(flow: &Mat, focal_length: f64) -> Mat {
    let mut flow_cartesian = Mat::default().unwrap();
    cv::cartToPolar(flow, &Mat::default().unwrap(), &mut flow_cartesian, None, true);

    let mut flow_norm = Mat::default().unwrap();
    cv::norm(flow_cartesian, &mut flow_norm, cv::NormTypes::L2, None, None);

    let mut flow_norm_repeated = Mat::default().unwrap();
    cv::repeat(flow_norm, 3, 1, &mut flow_norm_repeated);

    let mut flow_cartesian_norm = Mat::default().unwrap();
    cv::divide(flow_cartesian, flow_norm_repeated, &mut flow_cartesian_norm, 1.0, -1);

    let mut depth = Mat::default().unwrap();
    cv::divide(
        Mat::new_rows_cols_with_data(
            flow_cartesian_norm.size().height,
            flow_cartesian_norm.size().width,
            opencv::core::CV_32FC1,
            &[focal_length],
        )
            .unwrap(),
        flow_cartesian_norm.channel(2).unwrap(),
        &mut depth,
        1.0,
        -1,
    );

    depth
}

 */