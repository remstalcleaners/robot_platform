use opencv::highgui::{imshow,wait_key};
use opencv::core::{Mat, Point, Rect, Size, Scalar};
use opencv::imgproc::{circle, line};
use opencv::imgproc::rectangle;


pub fn show(img: &Mat, name: String) -> i32 {
    let mut img_2 = Mat::default();
    //opencv::imgproc::resize(&img, &mut img_2, Size{ width: 800, height:500 }, 0.5, 0.5, 0);
    imshow(&*name, &img);
    let key = wait_key(20);
    match key {
        Ok(x) => x,
        Err(_) => {0}
    }
}
pub fn paint_rects(img: &mut Mat, list: &Vec<(f32,f32,f32,f32)>, color: Scalar) {
    for ent in list {
        rectangle(img, Rect{
            x: (ent.0 - ent.2/2.0) as i32,
            y: (ent.1 - ent.3/2.0) as i32,
            width: ent.2 as i32,
            height: ent.3 as i32
        }, color, 5, 0, 0);
    }
}
pub fn paint_cycles(img: &mut Mat, list: &Vec<(f32,f32,f32,f32)>, color: Scalar) {
    for ent in list {
        circle(img,
               Point{ x: ent.0 as i32, y: ent.1 as i32 },
               ent.2 as i32, color, 5, 0, 0);
    }
}
pub fn paint_match(img: &mut Mat, list: &Vec<(f32, f32, f32, f32)> , color: Scalar) {
    line(img,
         Point{ x: list[2].0 as i32, y: list[2].1  as i32},
        Point{ x: list[0].0 as i32, y: list[0].1  as i32},
        color, 5, 0, 0);
    line(img,
         Point{ x: list[0].0 as i32, y: list[0].1  as i32},
         Point{ x: list[3].0 as i32, y: list[3].1  as i32},
         color, 5, 0, 0);
    line(img,
         Point{ x: list[2].0 as i32, y: list[2].1  as i32},
         Point{ x: list[1].0 as i32, y: list[1].1  as i32},
         color, 5, 0, 0);
    line(img,
         Point{ x: list[1].0 as i32, y: list[1].1  as i32},
         Point{ x: list[3].0 as i32, y: list[3].1  as i32},
         color, 5, 0, 0);
}





//Only for the tests ################################################################################
pub (crate) fn show_with_cycles(img: &mut Mat, name: String, list: &Vec<(f32,f32,f32)>) -> i32 {
    let mut img_2 = Mat::default();
    for ent in list {
        circle(img, Point{ x: ent.0 as i32, y: ent.1 as i32 }, ent.2 as i32, Scalar::new(1.0,1.0,1.0,1.0), -1, 0, 0);
    }
    opencv::imgproc::resize(img, &mut img_2, Size{ width: 800, height:500 }, 0.5, 0.5, 0);
    imshow(&*name, &img_2);
    let key = wait_key(20);
    match key {
        Ok(x) => x,
        Err(_) => {0}
    }
}
pub (crate) fn show_with_rects(img: &Mat, name: String, list: &Vec<(f32,f32,f32,f32)>) -> i32 {
    let mut img = img.clone();
    let mut img_2 = Mat::default();
    for ent in list {
        rectangle(&mut img, Rect{
            x: (ent.0 - ent.2/2.0) as i32,
            y: (ent.1 - ent.3/2.0) as i32,
            width: ent.2 as i32,
            height: ent.3 as i32
        }, Scalar::new(1.0,1.0,1.0,1.0), -1, 0, 0);
    }
    opencv::imgproc::resize(&mut img, &mut img_2, Size{ width: 800, height:500 }, 0.5, 0.5, 0);
    imshow(&*name, &img_2);
    let key = wait_key(20);
    match key {
        Ok(x) => x,
        Err(_) => {0}
    }
}




