use std::fs::File;
use std::io::Write;
use std::ops::Add;
//use image::png::CompressionType::Default;
use opencv::calib3d::RANSAC;
use opencv::core::{KeyPointTraitConst, Mat, MatTraitConst, Point, Point2d, Point2f, Ptr, Scalar, Size, TermCriteria, TermCriteria_Type, ToInputArray};
use opencv::types::{VectorOfKeyPoint, VectorOfMat, VectorOfPoint2f, VectorOfu8};
use opencv::video::calc_optical_flow_pyr_lk;
use opencv::features2d::{FastFeatureDetector, FastFeatureDetector_DetectorType, Feature2DTrait, SIFT, ORB, ORB_ScoreType, DrawMatchesFlags};
use opencv::imgproc::{FONT_HERSHEY_PLAIN, LINE_8};
use opencv::viz::Color;
use crate::dept_optic_flow::calculate_foe;


pub struct Odometry3 {
    detector: Ptr<SIFT>,
    //detector: Ptr<dyn ORB>,
    img: Mat,
    mask: Mat,
    mask_2: Mat,
    mask_3: Mat,
    status: VectorOfu8,
    error: Mat,
    index: i32,
    x_res: i32,
    y_res: i32,
    n: i32,
}

impl Odometry3 {
    pub fn new(focal: f64, pp: (f64, f64), x_res: i32, y_res: i32, n: i32) -> Self {
        println!("3");
        //Set camera properties
        let pp: Point2d = Point2d { x: pp.0, y: pp.1 };

        //Create the feature detectors
        /**/
        //SIFT

        let mut detector = SIFT::create(
            500,
            3,
            0.04,
            10.0,
            1.6
        ).expect("SIFT detector didn't crate");

        //FAST
        /*
        let mut detector = <dyn FastFeatureDetector>::create(
            25,
            true,
            FastFeatureDetector_DetectorType::TYPE_9_16
        ).expect("Detector couldn't be created.");
         */
        //ORB
        /*
        let mut detector = <dyn ORB>::create(
            500,
            1.2,
            8,
            31,
            0,
            2,
            ORB_ScoreType::HARRIS_SCORE,
            31,
            20
        ).expect("ORB detector didn't create");
         */

        ;
        ;


        //Make the masks
        /*
        let mut mask = Mat::default();
        let mut res_mask = Mat::default();
        let mut res_mask_2 = Mat::default();

        let mut first_pts = VectorOfPoint2f::default();
        let mut second_pts = VectorOfPoint2f::default();
        let mut first_keypoints = VectorOfKeyPoint::default();
        let mut state = VectorOfu8::default();
        let mut error = Mat::default();

        let mut rot = Mat::default();
        let mut t = Mat::default();
        */

        Self {
            detector: detector,
            img: Mat::default(),
            mask: Mat::default(),
            mask_2: Mat::default(),
            mask_3: Mat::default(),
            status:  VectorOfu8::new(),
            error:  Mat::default(),
            index: 0,
            x_res,
            y_res,
            n,
        }
    }
    pub fn first(&mut self, img: &Mat, name: &str) {
        self.img = img.clone();
    }
    pub fn step(&mut self, n_img: &Mat, name: &str) {
        let mut img_p = n_img.clone();
        let mut n_points = VectorOfPoint2f::new();
        for i in 0..191 {
            for j in 0..107 {
                n_points.push(Point2f{ x: (10+i*10) as f32, y: (10+j*10) as f32 });
            }
        }
        let mut n_opts = VectorOfPoint2f::new();
        calc_optical_flow_pyr_lk(
            &self.img,
            n_img,
            &n_points,
            &mut n_opts,
            &mut self.status,
            &mut self.error,
            Size { width: 31, height: 31 },
            5/* 0 or 3?*/,
            TermCriteria::new(3, 30, 0.01).unwrap(),
            0,
            0.0004,
        ).expect("calc_optical_flow_pyr_lk failed");

        println!("ccc{}", self.index);
        self.index += 1;
        let mut filtered_old = VectorOfPoint2f::new();
        let mut filtered_pts = VectorOfPoint2f::new();
        for i in 0..n_opts.len() {
            match self.status.get(i) {
                Ok(x) => {
                    if x == 1 {
                        filtered_old.push(n_points.get(i).expect("Status and old_pts have different lengths!"));
                        filtered_pts.push(n_opts.get(i).expect("Status and new_pts have different lengths!"));
                    }
                }
                Err(_) => {}
            }
        }



        let mut total = 0.0;
        let mut dx = 0.0;
        let mut dy = 0.0;
        let mut blocks = vec![vec![(0.0,0.0,0.0); 20]; 20];

        for i in 0..filtered_old.len() {
            let old_p = filtered_old.get(i).unwrap();
            let new_p = filtered_pts.get(i).unwrap();
            let tdx = new_p.x - old_p.x;
            let tdy = new_p.y - old_p.y;

            dx += tdx;
            dy += tdy;
            total += 1.0;

            let ix = (old_p.x / 1921.0 * blocks.len() as f32) as usize;
            let iy = (old_p.y / 1081.0 * blocks.len() as f32) as usize;
            blocks[ix][iy].0 += tdx;
            blocks[ix][iy].1 += tdy;
            blocks[ix][iy].2 += 1.0;


            opencv::imgproc::arrowed_line(
                &mut img_p,
                Point{ x: (old_p.x*2.0) as i32, y: (old_p.y*2.0) as i32 },
                Point{ x: (new_p.x*2.0) as i32, y: (new_p.y*2.0) as i32 },
                Scalar::new(20.0,0.0,255.0,255.0),
                1, LINE_8, 0, 0.2).unwrap();


        }
        let dx = dx / total;
        let dy = dy / total;
        opencv::imgproc::arrowed_line(
            &mut img_p,
            Point{ x: 1920/2, y: 1080/2 },
            Point{ x: 1920/2-(dx*10.0) as i32, y: 1080/2-(dy*10.0) as i32 },
            Scalar::new(255.0,0.0,0.0,0.0), 3, LINE_8, 0, 0.2).unwrap();

        for i in 0..blocks.len() {
            for j in 0..blocks[i].len() {
                /*
                opencv::imgproc::arrowed_line(
                    &mut img_p,
                    Point{
                        x: (960/blocks.len()+1920/blocks.len()*i) as i32,
                        y: (540/blocks[i].len()+1080/blocks[i].len()*j) as i32
                    },
                    Point{
                        x: (960/blocks.len()+1920/blocks.len()*i) as i32 + ((blocks[i][j].0/blocks[i][j].2)*10.0) as i32,
                        y: (540/blocks[i].len()+1080/blocks.len()*j) as i32 + ((blocks[i][j].1/blocks[i][j].2)*10.0) as i32
                    },
                    Scalar::new(255.0,255.0,255.0,0.0), 2, LINE_8, 0, 0.2).unwrap();
                 */
            }
        }

        for i in 0..filtered_old.len() {
            let old_p = filtered_old.get(i).unwrap();
            let new_p = filtered_pts.get(i).unwrap();
            opencv::imgproc::arrowed_line(
                &mut img_p,
                Point{ x: (old_p.x) as i32, y: (old_p.y) as i32 },
                Point{ x: (new_p.x - dx) as i32, y: (new_p.y - dy) as i32 },
                Scalar::new(20.0,255.0,255.0,255.0),
                1, LINE_8, 0, 0.2).unwrap();
        }



        let mut blocks = vec![vec![(0.0,0.0,0.0); 20]; 20];
        for i in 0..filtered_old.len() {
            let old_p = filtered_old.get(i).unwrap();
            let new_p = filtered_pts.get(i).unwrap();
            let tdx = new_p.x - old_p.x - dx;
            let tdy = new_p.y - old_p.y - dy;

            let ix = (old_p.x / 1921.0 * blocks.len() as f32) as usize;
            let iy = (old_p.y / 1081.0 * blocks.len() as f32) as usize;
            blocks[ix][iy].0 += tdx;
            blocks[ix][iy].1 += tdy;
            blocks[ix][iy].2 += 1.0;

            /*
            opencv::imgproc::arrowed_line(
                &mut img_p,
                Point{ x: (old_p.x*2.0) as i32, y: (old_p.y*2.0) as i32 },
                Point{ x: (new_p.x*2.0) as i32, y: (new_p.y*2.0) as i32 },
                Scalar::new(20.0,0.0,255.0,255.0),
                1, LINE_8, 0, 0.2).unwrap();

             */
        }
        for i in 0..blocks.len() {
            for j in 0..blocks[i].len() {
                /*
                opencv::imgproc::arrowed_line(
                    &mut img_p,
                    Point{
                        x: (960/blocks.len()+1920/blocks.len()*i) as i32,
                        y: (540/blocks[i].len()+1080/blocks[i].len()*j) as i32
                    },
                    Point{
                        x: (960/blocks.len()+1920/blocks.len()*i) as i32 + ((blocks[i][j].0/blocks[i][j].2)*10.0) as i32,
                        y: (540/blocks[i].len()+1080/blocks.len()*j) as i32 + ((blocks[i][j].1/blocks[i][j].2)*10.0) as i32
                    },
                    Scalar::new(255.0,255.0,255.0,0.0), 2, LINE_8, 0, 0.2).unwrap();


                 */

                let rrr = (
                    (blocks[i][j].0/blocks[i][j].2) * (blocks[i][j].0/blocks[i][j].2)+(blocks[i][j].1/blocks[i][j].2) * (blocks[i][j].1/blocks[i][j].2)
                ).sqrt();

                opencv::imgproc::circle(
                    &mut img_p,
                    Point{
                        x: (960/blocks.len()+1920/blocks.len()*i) as i32,
                        y: (540/blocks[i].len()+1080/blocks[i].len()*j) as i32
                    },
                    rrr as i32,
                    Scalar::new(255.0,255.0,255.0,0.0),
                    2,
                    LINE_8,
                    0).unwrap();
            }
        }


        //######################### Files ##########################################################
        {
            const DZ: f32 = 1.0;
            const ZC: f32 = 100.0;
            let name_i = name.clone().replace(".png","");
            let mut file = File::create(format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs2/P{}", name_i)).expect("Cannot create file.");
            let mut text = String::new();
            for i in 0..blocks.len() {
                for j in 0..blocks[i].len() {
                    text += &format!("{};{};{};{}\n", i, j, blocks[i][j].0/blocks[i][j].2, blocks[i][j].1/blocks[i][j].2);
                }
            }
            file.write_all(text.as_bytes()).expect("Cannot write to new file.");
        }
        //##########################################################################################

        opencv::imgcodecs::imwrite(&format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs2/M{}", name), &img_p, &Default::default()).expect("Cannot save");
        self.img = n_img.clone();
    }
}



