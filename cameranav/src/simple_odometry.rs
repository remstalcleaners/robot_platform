use opencv::core::{Mat, Vector};
use opencv::features2d::{BFMatcher, DescriptorMatcherConst, Feature2DTrait, ORB_ScoreType};
use opencv::types::{VectorOfDMatch, VectorOfMat};
use crate::camera::Camera;

fn run() {
    let mut orb = <dyn opencv::features2d::ORB>::create(
        500,
        1.2,
        8, 31,
        0,
        2,
        ORB_ScoreType::HARRIS_SCORE,
        31,
        20
    ).unwrap();
    let mut matcher = BFMatcher::create(2, true).unwrap();

    let mut camera = Camera::new(0).unwrap();
    let mask = Mat::default();

    let mut img_1 = Mat::default();
    let mut img_2 = Mat::default();

    let mut aps_1 = Vector::default();
    let mut ades_1 = VectorOfMat::default();
    let mut aps_2 = Vector::default();
    let mut ades_2 = VectorOfMat::default();

    let mut matched = VectorOfDMatch::new();
    let mut even = true;
    camera.take_img(&mut img_1).expect("init Img not taken error...");
    camera.take_img(&mut img_2).expect("init Img not taken error...");
    for i in 0..100 {
        if even {
            camera.take_img(&mut img_1).expect("Img not taken error...");
        }else{
            camera.take_img(&mut img_2).expect("Img not taken error...");
        }
        even = !even;

        orb.detect_and_compute(&img_1, &mask,&mut aps_1,&mut ades_1, false).unwrap();
        orb.detect_and_compute(&img_2, &mask,&mut aps_2,&mut ades_2, false).unwrap();
        matcher.train_match(
            &ades_1,
            &ades_2,
            &mut matched,
            &mask
        ).unwrap();


       // opencv::calib3d::find_essential_mat(&(), &(), &(), 0, 0.0, 0.0, 0, &mut ());

        //# Decompose the Essential matrix into R and t
        //R, t = self.decomp_essential_mat(E, q1, q2)

        //# Get transformation matrix
        //transformation_matrix = self._form_transf(R, np.squeeze(t))

    }























}
















