use opencv::prelude::*;
use opencv::core::in_range;
use opencv::types::VectorOfMat;
use opencv::imgproc::{find_contours, min_enclosing_circle, circle};
use opencv::imgproc::min_area_rect;
use opencv::core::{Point, Point2f, Scalar, Scalar_};
use opencv::core::{Vec3b, Mat1b, Size};
use opencv::core::Mat;


pub struct ColorFilter {
    b1: Scalar_<f64>,
    b2: Scalar_<f64>,
    b3: Option<Scalar_<f64>>,
    b4: Option<Scalar_<f64>>,
    mask_bin: Mat,
    mask_bin_extend: Mat,
    total_mask: Mat,
}
pub fn get_blue() -> ColorFilter {
    ColorFilter {
        b1: Scalar::new(100.0,90.0,50.0,0.0),
        b2: Scalar::new(160.0,255.0,255.0,0.0),
        b3: None,
        b4: None,
        mask_bin: Mat::default(),
        mask_bin_extend: Default::default(),
        total_mask: Default::default()
    }
}
pub fn get_red() -> ColorFilter {
    ColorFilter {
        b1: Scalar::new(0.0,90.0,50.0,0.0),
        b2: Scalar::new(5.0,255.0,255.0,0.0),
        b3: Some(Scalar::new(170.0,90.0,50.0,0.0)),
        b4: Some(Scalar::new(180.0,255.0,255.0,0.0)),
        mask_bin: Mat::default(),
        mask_bin_extend: Default::default(),
        total_mask: Default::default()
    }
}
pub fn get_green() -> ColorFilter {
    ColorFilter {
        b1: Scalar::new(35.0,20.0,50.0,0.0),
        b2: Scalar::new(80.0,255.0,255.0,0.0),
        b3: None,
        b4: None,
        mask_bin: Mat::default(),
        mask_bin_extend: Default::default(),
        total_mask: Default::default()
    }
}
pub fn get_yellow() -> ColorFilter {
    ColorFilter {
        b1: Scalar::new(20.0,90.0,50.0,0.0),
        b2: Scalar::new(30.0,255.0,255.0,0.0),
        b3: None,
        b4: None,
        mask_bin: Mat::default(),
        mask_bin_extend: Default::default(),
        total_mask: Default::default()
    }
}




impl ColorFilter {
    /*
      return Vec<(center x, center y, radius, radius)>
    */
    pub fn filter_circle(&mut self, hsvbild: &Mat, min_radius: f32) -> Vec<(f32, f32, f32, f32)> {
        if let (Some(b3), Some(b4)) = (&self.b3, &self.b4) {
            in_range(hsvbild, &self.b1, &self.b2, &mut self.mask_bin);
            in_range(hsvbild, b3, b4, &mut self.mask_bin_extend);
            opencv::core::add(
                &self.mask_bin,
                &self.mask_bin_extend,
                &mut self.total_mask,
                &Mat::default(),
                0
            );
        } else {
            in_range(hsvbild, &self.b1, &self.b2, &mut self.total_mask);
        }
        let mut contours = VectorOfMat::new();
        find_contours(
            &self.total_mask,
            &mut contours,
            0,
            2,
            Point { x: 0, y: 0 }
        ).expect("conture search error");
        let mut liste = Vec::new();
        for cont in &contours {
            let mut center = Point2f { x: 0.0, y: 0.0 };
            let mut radius = 0.0;
            match min_enclosing_circle(&cont, &mut center, &mut radius) {
                Ok(_) => {}
                Err(_) => { continue }
            }
            if radius < min_radius { continue }
            liste.push((center.x, center.y, radius, radius));
        }
        liste
    }
    pub fn filter_rectangle(&mut self, hsvbild: &Mat, min_size: f32) -> Vec<(f32, f32, f32, f32)> {
        if let (Some(b3), Some(b4)) = (&self.b3, &self.b4) {
            in_range(hsvbild, &self.b1, &self.b2, &mut self.mask_bin);
            in_range(hsvbild, b3, b4, &mut self.mask_bin_extend);
            opencv::core::add(
                &self.mask_bin,
                &self.mask_bin_extend,
                &mut self.total_mask,
                &Mat::default(),
                0
            );
        } else {
            in_range(hsvbild, &self.b1, &self.b2, &mut self.total_mask);
        }
        let mut contours = VectorOfMat::new();
        find_contours(
            &self.total_mask,
            &mut contours,
            0,
            2,
            Point { x: 0, y: 0 }
        ).expect("conture search error");
        let mut liste = Vec::new();
        for cont in &contours {
            match min_area_rect(&cont) {
                Ok(rect) => {
                    let angle = rect.angle();
                    let center = rect.center();
                    let (x,y) = (center.x,center.y);
                    let size = rect.size();
                    let (width,height) = (size.width, size.height);
                    if (width > min_size || height > min_size) && (width < height*3.0 && width*3.0 > height ) {
                        liste.push((x,y, width,height));
                    }
                }
                Err(_) => {continue}
            };
        }
        liste
    }
}


pub fn match_shape(
    red: &Vec<(f32,f32,f32,f32)>,
    blue: &Vec<(f32,f32,f32,f32)>,
    green: &Vec<(f32,f32,f32,f32)>,
    yellow: &Vec<(f32,f32,f32,f32)>,
    minimum: f32
) -> (f32, Vec<(f32, f32, f32, f32)>)
{
    let mut best = f32::MAX;
    let mut res = (0.0,0.0, 0.0,0.0, 0.0,0.0, 0.0,0.0);
    for re in red {
        for ye in yellow {
            let y_2 = re.1 - ye.1;
            if y_2 > 0.0 {continue}
            for gr in green {
                let x_1 =  gr.0 - re.0;
                if x_1 > 0.0  {continue}
                for bl in blue {
                    let y_1 = gr.1-bl.1;
                    let x_2 = bl.0 - ye.0;
                    if y_1 > 0.0 || x_2 > 0.0 {continue}
                    let score = (x_1-x_2)*(x_1-x_2) * (y_1-y_2)*(y_1-y_2);
                    if score < best {
                        best = score;
                        res = (re.0,re.1, bl.0,bl.1, gr.0,gr.1, ye.0,ye.1);/*Red, blue, green, yellow*/
                    }
                }
            }
        }
    }
    if best < 100.0 {
        println!(">>{}", best as i32);
        let x_d = ((res.0-res.4).abs() + (res.6-res.2).abs())/2.0;
        let y_d = ((res.7-res.1).abs() + (res.3-res.5).abs())/2.0;
        //println!("Distance: {}", (x_d/1000.0) as i32);
        //println!("Distance: {:.2}", y_d/10.0);
    }
    //red, blue, green, yellow
    (best, vec![(res.0,res.1,50.0,50.0),(res.2,res.3,50.0,50.0),(res.4,res.5,50.0,50.0),(res.6,res.7,50.0,50.0)])
}





