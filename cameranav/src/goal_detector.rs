use std::thread;
use std::time::Duration;
use opencv::calib3d::SOLVEPNP_ITERATIVE;
use opencv::core::{Point, Point2f, Point3f, Scalar};
use opencv::imgproc::{FONT_HERSHEY_SIMPLEX, LINE_8};
use opencv::prelude::Mat;
use opencv::types::{VectorOfMat, VectorOfPoint2f, VectorOfPoint3f};
use crate::{Camera, get_blue, get_green, get_red, get_yellow, match_shape, paint_match, paint_rects, show_with_rects};

const WIDTH: f32 = 10.0; //Width of the goal
const HEIGHT: f32 = 10.0; //Height of the goal

pub(crate) fn detect() {
    let mut cam = Camera::new(0).expect("Camera not found...");
    let mut img = Mat::default();
    let mut hsv = Mat::default();
    let mut red = get_red();
    let mut blue = get_blue();
    let mut green = get_green();
    let mut yellow = get_yellow();
    //let camera_matrix = cam.camera_prop.clone().unwrap().0;
    //let distortion = cam.camera_prop.clone().unwrap().1;
    ;
    let mut final_distance_left = 0.0;
    let mut final_distance_right = 0.0;
    let mut final_angle_top = 0.0;
    let mut final_angle_botton = 0.0;
    let mut K = 0.0051;
    loop {
        cam.take_img(&mut img);
        Camera::convert_hsv(&img, &mut hsv);
        let red_list = red.filter_rectangle(&hsv,20.0);
        let blue_list = blue.filter_rectangle(&hsv,20.0);
        let green_list = green.filter_rectangle(&hsv,20.0);
        let yellow_list = yellow.filter_rectangle(&hsv,20.0);
        //println!("{} | {} | {} | {}", red_list.len(),blue_list.len(),green_list.len(),yellow_list.len());

        //show_with_rects(&mut hsv, "Red".to_string(), &red_list);
        //show_with_rects(&mut hsv, "Blue".to_string(), &blue_list);
        //show_with_rects(&mut hsv, "Green".to_string(), &green_list);
        //show_with_rects(&mut hsv, "Yellow".to_string(), &yellow_list);
        //time = Instant::now();
        let (score, matched) = match_shape(&red_list,&blue_list,&green_list,&yellow_list,1.0);
        //println!("{}", time.elapsed().as_micros());
        paint_rects(&mut img, &red_list, Scalar::new(0.0,0.0,100.0,0.0));
        paint_rects(&mut img, &blue_list, Scalar::new(100.0,0.0,0.0,0.0));
        paint_rects(&mut img, &green_list, Scalar::new(0.0,100.0,0.0,0.0));
        paint_rects(&mut img, &yellow_list, Scalar::new(0.0,100.0,100.0,0.0));
        if score < 1000.0 {
            paint_match(&mut img, &matched, Scalar::new(0.0,0.0,0.0,0.0));


            /*
            let mut rvec = VectorOfMat::new();
            let mut tvec = VectorOfPoint3f::new();
            //The coordinate system inside this function is:
            //x is left to right (green to red)
            //y is down to up (blue to green)
            //z is the distance to the goal
            let mut obj_points = VectorOfPoint3f::new();//3d points of the color points on the goal
            let mut img_points = VectorOfPoint2f::new();//2d point they actually have on the image
            //Red point
            let mut red3f = Point3f::default();
            let mut red2f = Point2f::default();
            red3f.x = WIDTH/2.0;
            red3f.y = HEIGHT/2.0;
            red3f.z = 0.0;
            obj_points.push(red3f);
            red2f.x = matched[0].0;
            red2f.y = matched[0].1;
            img_points.push(red2f);
            //Blue point
            let mut blue3f = Point3f::default();
            let mut blue2f = Point2f::default();
            blue3f.x = WIDTH/2.0;
            blue3f.y = -HEIGHT/2.0;
            blue3f.z = 0.0;
            obj_points.push(blue3f);
            blue2f.x = matched[1].0;
            blue2f.y = matched[1].1;
            img_points.push(blue2f);
            //Green point
            let mut green3f = Point3f::default();
            let mut green2f = Point2f::default();
            green3f.x = -WIDTH/2.0;
            green3f.y = HEIGHT/2.0;
            green3f.z = 0.0;
            obj_points.push(green3f);
            green2f.x = matched[2].0;
            green2f.y = matched[2].1;
            img_points.push(green2f);

            //Yellow point
            let mut yellow3f = Point3f::default();
            let mut yellow2f = Point2f::default();
            yellow3f.x = -WIDTH/2.0;
            yellow3f.y = -HEIGHT/2.0;
            yellow3f.z = 0.0;
            obj_points.push(yellow3f);
            yellow2f.x = matched[3].0;
            yellow2f.y = matched[3].1;
            img_points.push(yellow2f);
            //Solve and get the rotation and translation vectors
            opencv::calib3d::solve_pnp(&obj_points, &img_points, &camera_matrix, &distortion, &mut rvec, &mut tvec, false, SOLVEPNP_ITERATIVE);
            println!("{:?} | {:?}", rvec, tvec);
            */

            //Simple and dump methode to get the distance

            let x_red = matched[0].0;
            let y_red = matched[0].1;
            let x_blue = matched[1].0;
            let y_blue = matched[1].1;
            let x_green = matched[2].0;
            let y_green  = matched[2].1;
            let x_yellow = matched[3].0;
            let y_yellow = matched[3].1;

            let height_left = y_blue - y_green;
            let height_right = y_yellow - y_red;
            //let height = (height_left + height_right)/2.0;
            final_distance_left = 1.0/height_left * 100.0;
            final_distance_right = 1.0/height_right * 100.0;
            let final_distance = (final_distance_left+final_distance_right)/2.0;
            let length_top = x_red - x_green;
            let length_bottom = x_yellow - x_blue;
            /*
            if (length_top*final_distance*K).abs() > 1.0 {
                K = 1.0/(length_top*final_distance).abs();
                println!("Corrected angle constant K to = {}", K);
            }
            if (length_bottom*final_distance*K).abs() > 1.0 {
                K = 1.0/(length_bottom*final_distance).abs();
                println!("Corrected angle constant K to = {}", K);
            }
             */
            final_angle_top = length_top*final_distance*K;
            final_angle_botton = length_bottom*final_distance*K;
        }
        let text = format!("D: {:.2} | {:.2} || A: {:.2} | {:.2} ;;", final_distance_left, final_distance_right, final_angle_top, final_angle_botton);
        opencv::imgproc::put_text(&mut img, &text, Point{ x: 10, y: 50 }, FONT_HERSHEY_SIMPLEX, 1.0, Scalar{ 0: [0.0,0.0,0.0,0.0] }, 1, LINE_8, false);
        crate::show(&img, "colors".to_string());
    }
}






pub fn detect_goal(img: &mut Mat) -> Option<(f32, f32, f32)> {
    let mut hsv = Mat::default();
    let mut red = get_red();
    let mut blue = get_blue();
    let mut green = get_green();
    let mut yellow = get_yellow();
    let mut K = 0.0051;

    Camera::convert_hsv(&img, &mut hsv);
    let red_list = red.filter_rectangle(&hsv,20.0);
    let blue_list = blue.filter_rectangle(&hsv,20.0);
    let green_list = green.filter_rectangle(&hsv,20.0);
    let yellow_list = yellow.filter_rectangle(&hsv,20.0);

    let (score, matched) = match_shape(&red_list,&blue_list,&green_list,&yellow_list,1.0);
    if score < 1000.0 {
        let x_red = matched[0].0;
        let y_red = matched[0].1;
        let x_blue = matched[1].0;
        let y_blue = matched[1].1;
        let x_green = matched[2].0;
        let y_green  = matched[2].1;
        let x_yellow = matched[3].0;
        let y_yellow = matched[3].1;

        let height_left = y_blue - y_green;
        let height_right = y_yellow - y_red;

        let distance_left = 1.0/height_left * 100.0;
        let distance_right = 1.0/height_right * 100.0;
        let sign = if distance_left >= distance_right {1.0} else {-1.0};
        let distance = (distance_left+distance_right)/2.0;
        let length_top = x_red - x_green;
        let length_bottom = x_yellow - x_blue;

        let angle_top = length_top*distance*K;
        let angle_botton = length_bottom*distance*K;

        let mut angle = (angle_top+angle_botton)/2.0;
        if angle > 1.0 {angle = 1.0;}

        return Some((score, distance, angle.cos()*sign));
    }
    None
}





