use std::time::Instant;
use opencv::core::{Mat, Vector};
use opencv::imgcodecs::imwrite;
use crate::Camera;


pub fn read_dir_imgs(path: &str) -> Vec<Mat> {
    let mut imgs = Vec::new();

    let mut files: Vec<_> = std::fs::read_dir(path).unwrap()
        .map(|r| r.unwrap())
        .collect();
    files.sort_by_key(|dir| dir.path());

    for file in files {
        let file = match file.path().to_str() {
            Some(x) => x.to_string(),
            _=> continue
        };
        println!("Read: {}", &file);
        let img = opencv::imgcodecs::imread(&file, 0);
        if let Ok(img) = img {
            imgs.push(img);
        }
    }
    imgs
}


pub fn collect_test_imgs() {
    let mut cam = Camera::new(0).unwrap();
    let mut time = Instant::now();
    for i in 0..300 {
        let mut img = Mat::default();
        cam.take_img(&mut img);
        if time.elapsed().as_secs() > 1 {
            imwrite(&format!("img{}.png", i),&img,&Vector::new());
        }
    }
}
