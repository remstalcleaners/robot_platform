use std::fs::File;
use std::io::Write;
use std::ops::Add;
//use image::png::CompressionType::Default;
use opencv::calib3d::RANSAC;
use opencv::core::{KeyPointTraitConst, Mat, MatTraitConst, Point, Point2d, Point2f, Ptr, Scalar, Size, TermCriteria, TermCriteria_Type, ToInputArray};
use opencv::types::{VectorOfKeyPoint, VectorOfMat, VectorOfPoint2f, VectorOfu8};
use opencv::video::calc_optical_flow_pyr_lk;
use opencv::features2d::{FastFeatureDetector, FastFeatureDetector_DetectorType, Feature2DTrait, SIFT, ORB, ORB_ScoreType, DrawMatchesFlags};
use opencv::imgproc::{FONT_HERSHEY_PLAIN, LINE_8};
use opencv::viz::Color;
use crate::dept_optic_flow::calculate_foe;


pub struct Odometry {
    detector: Ptr<SIFT>,
    //detector: Ptr<dyn ORB>,
    focal: f64,
    pp: Point2d,
    mask: Mat,
    mask_2: Mat,
    mask_3: Mat,

    status: VectorOfu8,
    error: Mat,

    rot: Mat,
    t: Mat,
    loc: Vec<f64>,
    dir: Vec<Vec<f64>>,

    imgs: Vec<Mat>,
    kpts: Vec<VectorOfKeyPoint>,
    descps: Vec<Mat>,
    pts: Vec<VectorOfPoint2f>,
    //Optical flow matches
    opts: Vec<VectorOfPoint2f>,

    rots: Vec<Mat>,
    ts: Vec<Mat>,
    index: usize,
}

impl Odometry {
    pub fn new(focal: f64, pp: (f64,f64)) -> Self {
        //Set camera properties
        let pp: Point2d = Point2d {x: pp.0, y: pp.1};

        //Create the feature detectors
        /**/
        //SIFT

        let mut detector = SIFT::create(
            500,
            3,
            0.04,
            10.0,
            1.6
        ).expect("SIFT detector didn't crate");

        //FAST
        /*
        let mut detector = <dyn FastFeatureDetector>::create(
            25,
            true,
            FastFeatureDetector_DetectorType::TYPE_9_16
        ).expect("Detector couldn't be created.");
         */
        //ORB
        /*
        let mut detector = <dyn ORB>::create(
            500,
            1.2,
            8,
            31,
            0,
            2,
            ORB_ScoreType::HARRIS_SCORE,
            31,
            20
        ).expect("ORB detector didn't create");
         */

;;


        //Make the masks
        /*
        let mut mask = Mat::default();
        let mut res_mask = Mat::default();
        let mut res_mask_2 = Mat::default();

        let mut first_pts = VectorOfPoint2f::default();
        let mut second_pts = VectorOfPoint2f::default();
        let mut first_keypoints = VectorOfKeyPoint::default();
        let mut state = VectorOfu8::default();
        let mut error = Mat::default();

        let mut rot = Mat::default();
        let mut t = Mat::default();
        */

        Self {
            focal: focal,
            pp: pp,
            detector: detector,
            mask: Mat::default(),
            mask_2: Mat::default(),
            mask_3: Mat::default(),
            status:  VectorOfu8::new(),
            error:  Mat::default(),
            rot: Mat::default(),
            t: Mat::default(),
            loc: vec![0.0,0.0,0.0],
            dir: vec![vec![0.0,0.0,0.0],vec![0.0,0.0,0.0],vec![0.0,0.0,0.0]],
            imgs: vec![],
            kpts: vec![],
            descps: vec![],
            pts: vec![],
            opts: vec![],
            rots: vec![],
            ts: vec![],
            index: 0,
        }
    }
    pub fn first(&mut self, img: &Mat, name: &str) {
        let mut n_keypoints = VectorOfKeyPoint::default();
        let mut n_descriptors = Mat::default();
        let mut n_points = VectorOfPoint2f::new();
        self.detector.detect_and_compute(
            img, &self.mask, &mut n_keypoints, &mut n_descriptors,false
        ).expect("detect and compute not working");
        for pt in &n_keypoints {
            n_points.push(pt.pt());
        }
        let test = VectorOfKeyPoint::default();

        //At the end push the newly allocated vectors into memory for later use
        self.kpts.push(n_keypoints);
        self.descps.push(n_descriptors);
        self.pts.push(n_points.clone());
        self.opts.push(n_points);
        self.imgs.push(img.clone());
        self.rots.push(self.rot.clone());
        self.ts.push(self.t.clone());
    }

    pub fn step(&mut self, n_img: &Mat, name: &str) {
        let mut n_keypoints = VectorOfKeyPoint::default();
        let mut n_descriptors = Mat::default();
        let mut n_points = VectorOfPoint2f::new();

        self.detector.detect_and_compute(
            n_img,
            &self.mask,
            &mut n_keypoints,
            &mut n_descriptors,
            false
        ).expect("detect and compute not working");
        for pt in &n_keypoints {
            //n_points.push(pt.pt());
        }
        for i in 0..38 {
            for j in 0..21 {
                n_points.push(Point2f{ x: (50+i*50) as f32, y: (50+j*50) as f32 });
            }
        }

        let mut n_opts = VectorOfPoint2f::new();
        calc_optical_flow_pyr_lk(
            &self.imgs[self.imgs.len()-1],
            n_img,
            &self.pts[self.pts.len()-1],
            &mut n_opts,
            &mut self.status,
            &mut self.error,
            Size { width: 21, height: 21 },
            5/* 0 or 3?*/,
            TermCriteria::new(3, 30, 0.01).unwrap(),
            0,
            0.0004,
        ).expect("calc_optical_flow_pyr_lk failed");
        let foe = calculate_foe(&self.pts[self.pts.len()-1], &n_opts, &self.status).unwrap();

        let mut filtered_old = VectorOfPoint2f::new();
        let mut filtered_pts = VectorOfPoint2f::new();
        //println!("Filtered {:?}", self.status);
        for i in 0..n_opts.len() {
            match self.status.get(i) {
                Ok(x) => {
                    if x == 1 {
                        filtered_old.push(self.pts[self.pts.len()-1].get(i).expect("Status and old pts have different lengths!"));
                        filtered_pts.push(n_opts.get(i).expect("Status and new_pts have different lengths!"));
                    }
                }
                Err(_) => {}
            }
        }
        //println!("Filtered {}", filtered_pts.len());
        //Calculate the rotation and translation direction for this image and the image before it
        let e = match opencv::calib3d::find_essential_mat_1(&filtered_old, &filtered_pts, self.focal, self.pp, RANSAC, 0.999, 1.0, 10000, &mut self.mask_2){
            Ok(x)=>x,
            Err(_)=>{return;}
        };

        match opencv::calib3d::recover_pose(&e, &filtered_old, &filtered_pts, &mut self.rot, &mut self.t, self.focal, self.pp, &mut self.mask_2){
            Ok(x)=>x,
            Err(_)=>{return;}
        };

        let rot =
        vec![
            vec![*self.rot.at_2d::<f64>(0,0).unwrap(), *self.rot.at_2d::<f64>(0,1).unwrap(), *self.rot.at_2d::<f64>(0,2).unwrap()],
            vec![*self.rot.at_2d::<f64>(1,0).unwrap(), *self.rot.at_2d::<f64>(1,1).unwrap(), *self.rot.at_2d::<f64>(1,2).unwrap()],
            vec![*self.rot.at_2d::<f64>(2,0).unwrap(), *self.rot.at_2d::<f64>(2,1).unwrap(), *self.rot.at_2d::<f64>(2,2).unwrap()]
        ];
        let t = vec![
            *self.t.at_2d::<f64>(0,0).unwrap(),
            *self.t.at_2d::<f64>(1,0).unwrap(),
            *self.t.at_2d::<f64>(2,0).unwrap()
        ];

        let x = rot[0][0]*t[0] + rot[0][1]*t[1] + rot[0][2]*t[2];
        let y = rot[1][0]*t[0] + rot[1][1]*t[1] + rot[1][2]*t[2];
        let z = rot[2][0]*t[0] + rot[2][1]*t[1] + rot[2][2]*t[2];
        self.loc[0] += x;
        self.loc[1] += y;
        self.loc[2] += z;

        self.dir[0][0] = rot[0][0]*self.dir[0][0]+ rot[0][1]*self.dir[1][0]+ rot[0][2]*self.dir[2][0];
        self.dir[0][1] = rot[0][0]*self.dir[0][1]+ rot[0][1]*self.dir[1][1]+ rot[0][2]*self.dir[2][1];
        self.dir[0][2] = rot[0][0]*self.dir[0][2]+ rot[0][1]*self.dir[1][2]+ rot[0][2]*self.dir[2][2];

        self.dir[1][0] = rot[1][0]*self.dir[0][0]+ rot[1][1]*self.dir[1][0]+ rot[1][2]*self.dir[2][0];
        self.dir[1][1] = rot[1][0]*self.dir[0][1]+ rot[1][1]*self.dir[1][1]+ rot[1][2]*self.dir[2][1];
        self.dir[1][2] = rot[1][0]*self.dir[0][2]+ rot[1][1]*self.dir[1][2]+ rot[1][2]*self.dir[2][2];

        self.dir[2][0] = rot[2][0]*self.dir[0][0]+ rot[2][1]*self.dir[1][0]+ rot[2][2]*self.dir[2][0];
        self.dir[2][1] = rot[2][0]*self.dir[0][1]+ rot[2][1]*self.dir[1][1]+ rot[2][2]*self.dir[2][1];
        self.dir[2][2] = rot[2][0]*self.dir[0][2]+ rot[2][1]*self.dir[1][2]+ rot[2][2]*self.dir[2][2];

        //println!("{:?}", self.loc);




        //self.cur_t = self.cur_t + self.cur_R.dot(t)
        //self.cur_R = R.dot(self.cur_R)


        //println!("{:?} -> {:?}", &self.rot, &self.t);
        //println!("------------------------");
        //println!("{:.2} {:.2} {:.2}", self.rot.at_2d::<f64>(0,0).unwrap(), self.rot.at_2d::<f64>(0,1).unwrap(), self.rot.at_2d::<f64>(0,2).unwrap());
        //println!("{:.2} {:.2} {:.2}", self.rot.at_2d::<f64>(1,0).unwrap(), self.rot.at_2d::<f64>(1,1).unwrap(), self.rot.at_2d::<f64>(1,2).unwrap());
        //println!("{:.2} {:.2} {:.2}", self.rot.at_2d::<f64>(2,0).unwrap(), self.rot.at_2d::<f64>(2,1).unwrap(), self.rot.at_2d::<f64>(2,2).unwrap());
        //println!("------------------------");
        //println!("{:.2} {:.2} {:.2}", self.t.at_2d::<f64>(0,0).unwrap(), self.t.at_2d::<f64>(1,0).unwrap(), self.t.at_2d::<f64>(2,0).unwrap());
        //println!("------------------------");

        let rx = format!("{:+.2} {:+.2} {:+.2}", self.rot.at_2d::<f64>(0,0).unwrap(), self.rot.at_2d::<f64>(0,1).unwrap(), self.rot.at_2d::<f64>(0,2).unwrap(), );
        let ry = format!("{:+.2} {:+.2} {:+.2}", self.rot.at_2d::<f64>(1,0).unwrap(), self.rot.at_2d::<f64>(1,1).unwrap(), self.rot.at_2d::<f64>(1,2).unwrap(), );
        let rz = format!("{:+.2} {:+.2} {:+.2}", self.rot.at_2d::<f64>(2,0).unwrap(), self.rot.at_2d::<f64>(2,1).unwrap(), self.rot.at_2d::<f64>(2,2).unwrap());




        let mut img_p = Mat::default();
        opencv::features2d::draw_keypoints(&n_img, &self.kpts[self.kpts.len()-1], &mut img_p, core::default::Default::default(), DrawMatchesFlags::DEFAULT).unwrap();
        //opencv::imgproc::put_text(&mut img_p, &rx, Point{ x: 20, y: 20 }, FONT_HERSHEY_PLAIN, 1.0, Default::default(), 2, LINE_8, false).unwrap();
        //opencv::imgproc::put_text(&mut img_p, &ry, Point{ x: 20, y: 40 }, FONT_HERSHEY_PLAIN, 1.0, Default::default(), 2, LINE_8, false).unwrap();
        //opencv::imgproc::put_text(&mut img_p, &rz, Point{ x: 20, y: 60 }, FONT_HERSHEY_PLAIN, 1.0, Default::default(), 2, LINE_8, false).unwrap();
        let mut dx = 0.0;
        let mut dy = 0.0;
        let mut total = 0.0;
        for i in 0..filtered_old.len() {
            let old_p = filtered_old.get(i).unwrap();
            let new_p = filtered_pts.get(i).unwrap();
            dx += new_p.x - old_p.x;
            dy += new_p.y - old_p.y;
            total += 1.0;
            let new_p = Point{ x: new_p.x as i32, y: new_p.y as i32 };
            let old_p = Point{ x: old_p.x as i32, y: old_p.y as i32 };
            //opencv::imgproc::arrowed_line(&mut img_p, old_p, new_p, Scalar::new(160.0,255.0,255.0,0.0), 1, LINE_8, 0, 0.2).unwrap();
        }
        dx = dx / total;
        dy = dy / total;
        opencv::imgproc::arrowed_line(&mut img_p, Point{ x: 640, y: 360 }, Point{ x: 640-(dx*10.0) as i32, y: 360-(dy*10.0) as i32 }, Scalar::new(140.0,235.0,235.0,0.0), 2, LINE_8, 0, 0.2).unwrap();


        let mut dx2 = 0.0;
        let mut dy2 = 0.0;
        let mut total2 = 0.0;
        for i in 0..filtered_old.len() {
            let old_p = filtered_old.get(i).unwrap();
            let new_p = filtered_pts.get(i).unwrap();
            let cc = ((old_p.x - new_p.x)* (old_p.x - new_p.x) + (old_p.y - new_p.y)*(old_p.y - new_p.y)).sqrt()*20000000.0;
            dx2 += new_p.x - old_p.x - dx;
            dy2 += new_p.y - old_p.y - dy;
            total2 += 1.0;

            let new_p = Point{ x: (old_p.x + (new_p.x - old_p.x - dx)*3.0) as i32, y: (old_p.y + (new_p.y - old_p.y - dy)*3.0) as i32 };
            let old_p = Point{ x: old_p.x as i32, y: old_p.y as i32 };

            if new_p.x.abs() > 1000 || new_p.y.abs() > 1000  {
                continue
            }


            opencv::imgproc::arrowed_line(&mut img_p, old_p, new_p, Scalar::new(5.0, 5.0, 235.0, 0.0), 1, LINE_8, 0, 0.2).unwrap();
        }
        dx2 = dx2 / total2;
        dy2 = dy2 / total2;

        //######################### Files ##########################################################
        {
            const DZ: f32 = 1.0;
            const ZC: f32 = 100.0;
            let name_i = name.clone().replace(".png","");
            let mut file = File::create(format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs2/P{}", name_i)).expect("Cannot create file.");
            let mut text = String::new();
            for i in 0..filtered_old.len() {
                let old_p = filtered_old.get(i).unwrap();
                let new_p = filtered_pts.get(i).unwrap();
                //Triangulation calculations
                //X
                let g = (old_p.x/new_p.x + old_p.y/new_p.y)/2.0;
                let s = DZ/ZC/(1.0-g);
                let x = s*new_p.x;
                let y = s*new_p.y;
                let z = s*ZC;
                let z = ((old_p.x - new_p.x)* (old_p.x - new_p.x) + (old_p.y - new_p.y)*(old_p.y - new_p.y)).sqrt()*1000.0;
                //
                text += &format!("{};{};{}\n", new_p.x, new_p.y, z);
            }
            file.write_all(text.as_bytes()).expect("Cannot write to new file.");
        }
        //##########################################################################################


        opencv::imgproc::arrowed_line(&mut img_p, Point{ x: 640, y: 360 }, Point{ x: 640-(dx2*10.0) as i32, y: 360-(dy2*10.0) as i32 }, Scalar::new(255.0, 0.0, 255.0, 0.0), 2, LINE_8, 0, 0.2).unwrap();
        println!("ZZ: {} | {}", dx2, dy2);

        opencv::imgproc::rectangle_points(&mut img_p, Point{ x: foe.x as i32, y: foe.y as i32 }, Point{ x: foe.x as i32+20, y: foe.y as i32+20 }, Scalar::new(0.0, 255.0, 0.0, 0.0), 3, LINE_8, 0).unwrap();
        //println!("{} || {}", foe.x, foe.y);
        opencv::imgcodecs::imwrite(&format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs2/M{}", name), &img_p, &Default::default()).expect("Cannot save");
        self.index += 1;

        //At the end push the newly allocated vectors into memory for later use
        self.kpts.push(n_keypoints);
        self.descps.push(n_descriptors);
        self.pts.push(n_points);
        self.opts.push(filtered_pts);
        self.imgs.push(n_img.clone());
        self.rots.push(self.rot.clone());
        self.ts.push(self.t.clone());
    }
}



/*
fn structure_from_motion() {





    //For each image LOOP




    loop {




    }
}


 */







