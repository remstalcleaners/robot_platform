use std::thread;
use std::time::{Duration, Instant};
use opencv::calib3d::{decompose_essential_mat, find_essential_mat, RANSAC};

use opencv::prelude::*;
use opencv::core::{Mat, Vec3b, Mat1b, Size, Point, Ptr, NORM_HAMMING, DMatch, no_array};
use opencv::videoio::{CAP_PROP_FRAME_HEIGHT, CAP_PROP_FRAME_WIDTH, VideoCapture};
use opencv::imgproc::{THRESH_BINARY, THRESH_BINARY_INV};
use opencv::imgproc::{cvt_color, COLOR_BGR2HSV, COLOR_BGR2GRAY};
use opencv::features2d::{BFMatcher, DrawMatchesFlags, ORB};
use opencv::core::Vector;
use opencv::core::KeyPoint;
use opencv::features2d;
use opencv::imgcodecs::imwrite;
use opencv::platform_types::size_t;
use opencv::types::{VectorOfDMatch, VectorOfKeyPoint, VectorOfMat, VectorOfPoint2d, VectorOfVectorOfDMatch};
use crate::Camera;
use crate::test_functions::read_dir_imgs;

pub fn compare_imgs(a: &Mat, b: &Mat) -> (f32, f32) {

    unimplemented!()

}


fn match_images(a: &Mat, b: &Mat) -> (VectorOfKeyPoint, VectorOfKeyPoint, Mat, Mat, VectorOfDMatch) {
    println!("{} zu {}", a.dims(), b.dims());
    let mut orb = <dyn features2d::ORB>::default().unwrap();
    let mask = Mat::default();
    let mut aps = Vector::default();
    let mut ades = Mat::default();
    let mut bps = Vector::default();
    let mut bdes = Mat::default();
    orb.detect_and_compute(&a, &mask,&mut aps,&mut ades, false).unwrap();
    orb.detect_and_compute(&b, &mask,&mut bps,&mut bdes, false).unwrap();
    let mut matcher = BFMatcher::create(2, true).unwrap();
    println!("DES v{} {}", ades.dims(), bdes.dims());
    let mut res = VectorOfDMatch::new();
    matcher.train_match(
        &ades,
        &bdes,
        &mut res,
        &mask
    ).unwrap();
    (aps, bps, ades, bdes, res)
}


//Test ###################################################################################################################

pub fn test_match_images() {
    let path1 = "/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/testimg2/a200/".to_string();
    let path2 = "/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/testimg2/m200/".to_string();
    let images = read_dir_imgs(&path1);
    let mut total = 0;
    println!(">> {}", images.len());
    for i in 0..images.len()-1 {
        let mut img = Mat::default();
        let (aps,bps,ades,bdes,res) = match_images(&images[i], &images[i+1]);
        println!("M: {}", res.len());
        total += res.len();
        opencv::features2d::draw_matches(
            &images[i],
            &aps,
            &images[i+1],
            &bps,
            &res,
            &mut img,
            Default::default(),
            Default::default(),
            &Default::default(),
            DrawMatchesFlags::DEFAULT
        );
        println!("> {}", i);
        imwrite(&format!("{}match{}to{}.png",path2, i, i+1),&img, &Vector::new());
    }
    println!("F: {}", total);
}





