use std::ops::{Add, Mul};
//use image::png::CompressionType::Default;
use opencv::calib3d::RANSAC;
use opencv::core::{CV_32FC1, Mat, MatExpr, MatExprResult, MatExprTraitConst, MatTraitConst, MatTraitConstManual, NORM_MINMAX, Point, Point2d, Point2f, Ptr, Scalar, Size, TermCriteria, TermCriteria_Type, ToInputArray};
use opencv::types::{VectorOff32, VectorOfKeyPoint, VectorOfMat, VectorOfPoint2f, VectorOfu8};
use opencv::video::calc_optical_flow_pyr_lk;
use opencv::features2d::{FastFeatureDetector, FastFeatureDetector_DetectorType, Feature2DTrait, SIFT, ORB, ORB_ScoreType, DrawMatchesFlags};
use opencv::imgproc::{FONT_HERSHEY_PLAIN, LINE_8};
use opencv::viz::Color;
use crate::dept_optic_flow::calculate_foe;


pub struct Odometry2 {
    imgs: Vec<Mat>,
    mask: Mat,
}


impl Odometry2 {
    pub fn new() -> Self {
        Self {
            imgs: vec![],
            mask: Mat::default(),
        }
    }
    pub fn first(&mut self, img: &Mat) {
        self.imgs.push(img.clone());
    }

    pub fn step(&mut self, n_img: &Mat, name: &str) {
        let mut flow = Mat::default();

        opencv::video::calc_optical_flow_farneback(
            &self.imgs[self.imgs.len() - 1],
            &n_img,
            &mut flow,
            0.5,
            3,
            15,
            3,
            5,
            1.2,
            0
        ).expect("Flow failed!");

        let mut flowxy = VectorOfMat::new();
        opencv::core::split(&flow, &mut flowxy).expect("Splitting not working");
        let mut m = Mat::default();
        let mut a = Mat::default();
        //opencv::imgcodecs::imwrite(&format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs2/M{}", name), &flowxy.get(1).expect("0 not got"), &Default::default()).expect("Cannot save");

        opencv::core::cart_to_polar(
            &flowxy.get(0).expect("0 not got"),
            &flowxy.get(1).expect("0 not got"),
            &mut m,
            &mut a,
            false
        ).expect("Transformation cart to pol failed!");
        let a = match a.mul(180.0/3.14/2.0){
            MatExprResult::Ok(x) => {x}
            MatExprResult::Err(_) => {panic!("mul not working")}
        };
        let a: Mat = a.to_mat().expect("matexprresult not working");
        let mut m2 = Mat::default();
        opencv::core::normalize(
            &m,
            &mut m2,
            1.0,
            0.0,
            NORM_MINMAX,
            -1,
            &self.mask).expect("norm failed");
        let mut mixer = VectorOfMat::new();
        let filler = Mat::zeros_size(a.size().unwrap(), CV_32FC1).expect("Size failed").to_mat().unwrap();
        //println!("{:?} : {:?} : {:?}", a.size().unwrap(), filler.size().unwrap(), m2.size().unwrap());
        //println!("{:?} : {:?} : {:?}", a.typ(), filler.typ(), m2.typ());
        mixer.push(a);
        mixer.push(filler);
        mixer.push(m2);

        let mut res = Mat::default();

        opencv::core::merge(&mixer, &mut res).expect("lol");
        opencv::imgcodecs::imwrite(&format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgs2/M{}", name), &res, &Default::default()).expect("Cannot save");

        self.imgs.push(n_img.clone());
        println!("{}", &name);
    }
}