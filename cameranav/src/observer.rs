use std::fs::File;
use std::io::Write;
use std::ops::Add;
//use image::png::CompressionType::Default;
use opencv::calib3d::RANSAC;
use opencv::core::{KeyPointTraitConst, Mat, MatTraitConst, Point, Point2d, Point2f, Ptr, Scalar, Size, TermCriteria, TermCriteria_Type, ToInputArray};
use opencv::types::{VectorOfKeyPoint, VectorOfMat, VectorOfPoint2f, VectorOfu8};
use opencv::video::calc_optical_flow_pyr_lk;
use opencv::features2d::{FastFeatureDetector, FastFeatureDetector_DetectorType, Feature2DTrait, SIFT, ORB, ORB_ScoreType, DrawMatchesFlags};
use opencv::imgproc::{FONT_HERSHEY_PLAIN, LINE_8};
use opencv::viz::Color;
use crate::color_detection::{ColorFilter, get_blue, get_red};


pub struct Observer {
    filter_red: ColorFilter,
    filter_blue: ColorFilter,
}


impl Observer {
    pub fn new() -> Self {
        Self {
            filter_red: get_red(),
            filter_blue: get_blue(),
        }
    }
    pub fn run(&mut self, cam: &Mat, index: i32) -> (f32, f32, f32, f32, f32, f32, f32, f32) {
        let reds = self.filter_red.filter_circle(cam, 7.0);
        let blues = self.filter_blue.filter_circle(cam, 7.0);
        let mut img = cam.clone();
        for red in &reds {
            opencv::imgproc::circle(&mut img,
                                    Point{ x: red.0 as i32, y: red.1 as i32 },
                                    red.3 as i32,
                                    Scalar::new(0.0,0.0,235.0,0.0),
                                    2, LINE_8, 0).unwrap();
        }
        for bl in &blues {
            opencv::imgproc::circle(&mut img,
                                    Point{ x: bl.0 as i32, y: bl.1 as i32 },
                                    bl.3 as i32,
                                    Scalar::new(235.0,0.0,0.0,0.0),
                                    2, LINE_8, 0).unwrap();
        }




        let res = match_car(&reds, &blues);
        let z = res.1;

        opencv::imgproc::circle(&mut img,
                                Point{ x: z.0 as i32, y: z.1 as i32 },
                                10,
                                Scalar::new(0.0,235.0,0.0,0.0),
                                2, LINE_8, 0).unwrap();
        opencv::imgproc::circle(&mut img,
                                Point{ x: z.2 as i32, y: z.3 as i32 },
                                10,
                                Scalar::new(0.0,235.0,0.0,0.0),
                                2, LINE_8, 0).unwrap();
        opencv::imgproc::circle(&mut img,
                                Point{ x: z.4 as i32, y: z.5 as i32 },
                                10,
                                Scalar::new(0.0,235.0,0.0,0.0),
                                2, LINE_8, 0).unwrap();
        opencv::imgproc::circle(&mut img,
                                Point{ x: z.6 as i32, y: z.7 as i32 },
                                10,
                                Scalar::new(0.0,235.0,0.0,0.0),
                                2, LINE_8, 0).unwrap();
        opencv::imgcodecs::imwrite(&format!("/media/lippok/nixos/home/lippok/IntelliJIDEAProjects/imgobs2/{}.jpeg", index), &img, &Default::default()).expect("Cannot save");


        res.1
    }
}



fn match_car(
    red: &Vec<(f32,f32,f32,f32)>,
    blue: &Vec<(f32,f32,f32,f32)>
) -> (f32, (f32, f32, f32, f32, f32, f32, f32, f32))
{
    let mut best = (0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0);
    let mut total_score = f32::MAX;
    println!("match car");
    println!("{} {}", red.len(), blue.len());
    for re1 in red {
        for re2 in red {
            for bl1 in blue {
                for bl2 in blue {
                    //Don't check against itself
                    if re1.0 > re2.0 {continue;}
                    if bl1.0 > bl2.0 {continue;}
                    if re1.0 == re2.0 && re1.1 == re2.1 {continue;}
                    if bl1.0 == bl2.0 && bl1.1 == bl2.1 {continue;}
                    if re1.0 > bl1.0 || bl2.0 > re2.0 {continue;}
                    let ar_r = (re2.0-re1.0, re2.1-re1.1);

                    //if (ar_r.0*ar_r.0 + ar_r.1*ar_r.1).sqrt() > 500.0 {continue}

                    let bl1t = (re1.0 + ar_r.0*(6.0/29.0), re1.1 + ar_r.1*(6.0/29.0),);
                    let bl2t = (re2.0 - ar_r.0*(6.0/29.0), re2.1 - ar_r.1*(6.0/29.0),);

                    let bl1_score = (bl1.0 - bl1t.0)*(bl1.0 - bl1t.0) + (bl1.1 - bl1t.1)*(bl1.1 - bl1t.1);
                    let bl2_score = (bl2.0 - bl2t.0)*(bl2.0 - bl2t.0) + (bl2.1 - bl2t.1)*(bl2.1 - bl2t.1);

                    let score = bl1_score*bl2_score;

                    if score < total_score {
                        best = (re1.0, re1.1, bl1.0, bl1.1, bl2.0, bl2.1 , re2.0, re2.1 );
                        total_score = score;
                    }
                }
            }
        }
    }
    //Return: total score, (left red to left blue arrow, x,y, to x,y, right red to blue arrow, x,y to x,y)
    (total_score, best)
}
